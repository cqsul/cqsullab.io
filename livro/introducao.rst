.. role:: raw-latex(raw)
   :format: latex
..

.. _cap3_MQ:

******************************
Introdução à Mecânica Quântica
******************************

A partir do final do século XIX e início do século XX, diversos
fenômenos desafiavam a capacidade de explicação das teorias físicas
vigentes. Dentre esses fenômenos, pode-se citar a radiação de corpo
negro, o efeito fotoelétrico, as características ondulatórias do elétron
em experimento de dupla fenda e o experimento de Stern-Gerlach. A
Mecânica Quântica é produto das tentativas de se explicar esses
fenômenos de modo compatível com o que se observa experimentalmente.

A apresentação da Mecânica Quântica, neste capítulo, é feita de maneira
axiomática. Para manter o texto mais conciso e restrito ao objetivo de
se estudar Computação Quântica, não são apresentadas motivações para os
axiomas ou discussões a respeito dos experimentos. Uma discussão dos
experimentos de fenda dupla e de Stern-Gerlach encontra-se em
:raw-latex:`\cite{book:pqci_benenti}`, seções 2.1 e 2.2, e em
:raw-latex:`\cite{book:qcqi_nc}`, seção 1.5.1.

As principais referências para este capítulo são os livros
:raw-latex:`\cite{book:qcqi_nc}`, seção 2.2 e
:raw-latex:`\cite{book:pqci_benenti}`, seção 2.4.

.. container:: section

Postulados da Mecânica Quântica
===============================

Descrição de um Sistema Físico
------------------------------

O primeiro postulado prescreve como um sistema físico isolado pode
ser descrito pela Mecânica Quântica. Um sistema *isolado* é um
sistema no qual não ocorrem trocas de energia ou matéria com o
ambiente.

.. admonition:: Postulado 1

    Um **sistema físico isolado** é descrito por um espaço de
    Hilbert :math:`\mathcal{H}` com escalares complexos. Esse
    espaço vetorial é chamado *espaço de estados*. Um estado do
    sistema é descrito pelo chamado *vetor de estado*, um vetor
    unitário no espaço de estados do sistema.


A Mecânica Quântica não descreve qual espaço de Hilbert deve ser
usado para descrever o sistema. Descobrir qual espaço de estados
descreve adequadamente um dado sistema físico isolado é tarefa dos
cientistas, que recorrem a teorias específicas ou propõem modelos
para o sistema em questão. A Mecânica Quântica traz apenas a
estrutura matemática e conceitual na qual espera-se que os
sistemas físicos se encaixem.

No presente trabalho, apenas os espaços vetoriais de dimensão
finita serão considerados. Nesse caso, um espaço de Hilbert é
equivale a um espaço vetorial com produto interno.

.. admonition:: Exemplo 2.1
    :class: tip

    Um *qubit* é um sistema quântico descrito matematicamente por
    um espaço de Hilbert :math:`\mathcal{H} = \mathbb{C}^2` com
    :math:`\dim \mathcal{H} = 2`. Qualquer base do espaço do
    sistema tem exatamente dois vetores. Uma base para o sistema é
    a base computacional :math:`\big\{ \ket{0} , \ket{1} \big\}`. O
    vetor nulo é costumeiramente denotado apenas por :math:`0` para
    não ser confundido com o vetor da base :math:`\ket{0}`.

    Esse sistema é um análogo quântico ao bit clássico, um sistema
    que pode assumir os valores 0 ou 1 e que assume apenas um dos
    valores em cada instante. O sistema pode encontrar-se em
    qualquer estado :math:`\ket{\psi} \in \mathcal{H}` com
    :math:`||\ket{\psi||} = 1`. Isto é, todo vetor
    :math:`\ket{\psi} = a \ket{0} + b \ket{1}` com
    :math:`|a|^2 + |b|^2 = 1` é capaz de descrever um
    estado do sistema.

    Por exemplo, o qubit pode encontrar-se no estado
    :math:`\ket{0}` ou :math:`\ket{1}`, de forma parecida com o bit
    clássico. A novidade agora é que ele pode se encontrar numa
    superposição desses estados, como
    :math:`\frac{1}{\sqrt{2}}\ket{0} + \frac{1}{\sqrt{2}}\ket{1}`,
    pois este é um vetor unitário em :math:`\mathcal{H}`.


Evolução Temporal de um Sistema Físico
--------------------------------------

O estado de um sistema físico está sujeito a alterações com o
tempo. A estrutura geral para essa evolução temporal do estado é
dada pelo postulado a seguir.


.. admonition:: Postulado 2

    A evolução temporal de um **sistema físico fechado** é dada por
    um operador linear unitário no seu espaço de estados
    :math:`\mathcal{H}`. Em símbolos:

    .. math:: \ket{\psi_f} = U \ket{\psi_i} \ .

Por ser um operador unitário, :math:`U` preserva o tamanho dos
vetores, de modo que o vetor :math:`\ket{\phi_f}` tem norma 1 e
corresponde a um estado do sistema.

O enunciado do postulado 2 pode ser reescrito em termos de
operadores Hermitianos.


.. admonition:: Postulado 2'

    A evolução temporal de um **sistema físico fechado** é dada
    pela *equação de Schroedinger*

    .. math:: i \hbar \frac{d}{dt} \ket{\psi(t)} = H \ket{\psi(t)} \ ,

    em que :math:`H` é um operador Hermitiano no espaço de estados
    do sistema.


Neste postulado tem-se :math:`\hbar = \frac{h}{2\pi}`, em que
:math:`h = 6,\!626 \cdot 10^{-34} \, J \cdot s` é a constante de
Plank, determinada experimentalmente.

A equivalência entre esses dois postulados é dada por um teorema
da Análise Funcional chamado Teorema de Stone [1]_. Para se ter
uma ideia dessa equivalência entre os postulados, é mostrada uma
argumentação não formal de que (2’) implica (2) a seguir.


.. admonition:: Postulado 2'

    O enunciado (2’) implica (2).

.. container:: proofclaim

 Supõe-se que valha o enunciado em (2’). Seja :math:`H` um
 operador hermitiano. Então :math:`H = H^{\dagger}`. Considere
 inicialmente um intervalo de tempo :math:`dt` infinitesimal.
 Pode-se escrever

 .. math::

    \begin{aligned}
     \ket{\psi(t+dt)} &\cong &
     \ket{\psi(t)} + \frac{d}{dt} \ket{\psi(t)} dt + \frac{1}{2!} \frac{d^2}{dt^2} \ket{\psi(t)} dt^2 + \ldots \nonumber \\
     &\cong & \ket{\psi(t)} + \frac{d}{dt} \ket{\psi(t)} dt \ ,\nonumber % \label{eq:post2_1}
    \end{aligned}

 realizando a expansão em série de Taylor e retendo os termos
 até primeira ordem apenas. Pelo enunciado de (2’), tem-se que

 .. math:: \frac{d}{dt} \ket{\psi(t)} = - \frac{i H}{\hbar} \ket{\psi(t)}

 Substituindo na equação anterior, tem-se que

 .. math:: \ket{\psi(t+dt)} \cong \left( I - \frac{i H dt}{\hbar}   \right) \ket{\psi(t)} \ .

 Definindo :math:`U_{dt} = I - \frac{i H dt}{\hbar}`, tem-se que
 :math:`U_{dt}` é unitária se desprezarmos os termos de ordem
 maior ou igual a 2 em :math:`dt`. Efetivamente, tem-se:

 .. math::

    \begin{aligned}
     U_{dt} U_{dt}^{\dagger}
     &=&
     \left( I - \frac{i H dt }{\hbar}   \right) \left( I + \frac{i H^{\dagger} dt}{\hbar}   \right) \nonumber \\
      &=&
      I - \frac{i H dt}{\hbar} +\frac{i H dt}{\hbar} -  \frac{i^2 H^2 dt^2}{\hbar^2} \nonumber \\
      &\cong& I  \nonumber \\
       U_{dt}^{\dagger}  U_{dt}
     &=&
     \left( I + \frac{i H^{\dagger} dt }{\hbar}   \right) \left( I - \frac{i H dt}{\hbar}   \right) \nonumber \\
      &=&
      I + \frac{i H dt}{\hbar} - \frac{i H dt}{\hbar} -  \frac{i^2 H^2 dt^2}{\hbar^2} \nonumber \\
      &\cong& I \ . \nonumber
    \end{aligned}

 Para um intervalo de tempo finito de :math:`t_i` a :math:`t_f`,
 divida o intervalo de tempo em pedaços iguais
 :math:`dt = \frac{t_f - t_i}{n}` , com
 :math:`n \rightarrow \infty`. A evolução temporal entre
 :math:`t` e :math:`t+dt` nesse intervalo de tempo é dada pelo
 operador :math:`U_{dt}`. Dessa forma, tem-se que

 .. math:: t_f = t_i + n \cdot dt

 e que

 .. math::

    \begin{aligned}
      \ket{\psi(t_f)}
      &=& U_{dt} \ket{\psi(t_f - dt)} \\
      &=&  U_{dt}  U_{dt}  \ket{\psi(t_f - 2dt)} \\
      &=& \ldots \\
      &=& \underbrace{U_{dt} \ldots U_{dt}}_{\text{$n$ vezes}} \ket{\psi(t_i)}
    \end{aligned}

 Seja :math:`U = (U_{dt})^{n}`. Com isso [2]_,

 .. math::

    \begin{aligned}
      U \! \! \! \! \! \!  \!
      & &= \  (U_{dt})^{n}   =  \left( I - \frac{i H dt }{\hbar}   \right)^n  \\
      & &= \  \left( I - \frac{\frac{i H (t_f-t_i)}{\hbar}}{n}   \right)^n \\
      & &\xrightarrow[n \to \infty]{} \exp\left(\frac{i H (t_f-t_i)}{\hbar} \right)
    \end{aligned}

 Portanto tem-se

 .. math:: \ket{ \psi(t_f)} = U \ket{\psi(t_i)} \ .

 Além disso, :math:`U` é um operador unitário pois é produto de
 operadores unitários, conforme seção
 `[cap2:operadores_unitarios] <#cap2:operadores_unitarios>`__.
 Assim, chegou-se ao enunciado (2).

.. admonition:: Exemplo 2.2
    :class: tip


    As matrizes de Pauli :math:`X`, :math:`Y` e :math:`Z`
    apresentadas no exemplo
    `[cap2:ex_matrizes_de_pauli] <#cap2:ex_matrizes_de_pauli>`__
    representam operadores lineares unitários em 1 qubit na base
    computacional e podem, portanto, representar a evolução
    temporal de um qubit.

.. admonition:: Exemplo 2.3
    :class: tip

    A matriz de Hadamard :math:`H` representa um operador linear
    unitário na base computacional, e pode, portanto, ser uma
    operação de evolução temporal de um sistema físico de 1 qubit.

    Se o qubit se encontra no estado

    .. math:: \ket{\psi_i} = \frac{1}{\sqrt{2}} \big( \ket{0} + \ket{1} \big) \ ,

    após a aplicação de :math:`H`, o estado final passa a ser

    .. math::
        \begin{aligned}
            \ket{\psi_f}
            &=& H\frac{1}{\sqrt{2}} \big( \ket{0} + \ket{1} \big) \\
            &=& \frac{1}{\sqrt{2}}\big( H \ket{0} + H\ket{1} \big) \\
            &=& \frac{1}{\sqrt{2}}\left(\frac{1}{\sqrt{2}}\ket{0} + \frac{1}{\sqrt{2}}\ket{1}  + \frac{1}{\sqrt{2}} \ket{0} - \frac{1}{\sqrt{2}} \ket{1} \right) \\
            &=& \ket{0}
        \end{aligned}

Medidas em Sistemas Físicos
---------------------------

Um fenômeno muito particular da Mecânica Quântica é a medida de um
observável em um sistema físico. Em geral, não é possível obter
com certeza o resultado de uma medida. O que a teoria fornece são
probabilidades de se obter cada possível resultado da medida.

Esse caráter probabilístico está atrelado aos fundamentos da
Mecânica Quântica [3]_. Isso significa que a Mecânica Quântica é
uma teoria fundamentalmente probabilística.

Matematicamente, um observável é descrito por um operador
Hermitiano e seus autovalores correspondem aos possíveis
resultados da medida. Imediatamente após a medida, o estado é
projetado no autoespaço associado ao valor obtido.

.. admonition:: Postulado 3


    A cada observável físico associa-se um operador Hermitiano
    :math:`A` no espaço de Hilbert do sistema. O operador :math:`A`
    admite decomposição espectral e seus autovalores são reais
    (conforme seção
    `[cap2:op_hermitianos] <#cap2:op_hermitianos>`__). Seja

    .. math:: A = \sum_k a_k P_k

    a sua decomposição espectral, com :math:`a_k` autovalores
    distintos e :math:`P_k` as projeções ortogonais nos autoespaços
    correspondentes.

    Os valores :math:`a_k` correspondem aos possíveis resultados da
    medida do observável :math:`A`. Se o sistema está no estado
    :math:`\ket{\psi}` antes da medida, a probabilidade de o
    resultado ser :math:`a_k` é dada por

    .. math:: p(a_k) = ||P_k \ket{\psi||}^2 \ .

    O estado do sistema após a medida, dado que se obteve o
    resultado :math:`a_k`, é dado por

    .. math:: \frac{P_k \ket{\psi}}{||P_k \ket{\psi||}} \ .

    Caso o observável seja descrito por um operador :math:`A` com
    todos os autovalores distintos, é possível escrever

    .. math:: A = \sum_k a_k \ketbra{k}{k} \ ,

    com os :math:`a_k`\ s todos distintos e :math:`P_k = \ketbra{k}{k}`. A
    probabilidade de se obter o valor :math:`a_k` é

    .. math:: p(a_k) = |\braket{k | \psi}|^2 \ ,

    e, dado que o resultado da medida tenha sido :math:`a_k`, o estado
    do sistema passa a ser

    .. math:: \frac{\braket{k | \psi}}{|\braket{k | \psi}|} \ket{k} \sim \ket{k} \ \text{(a menos de uma fase global)} .

.. admonition:: Exemplo 2.4
    :class: tip


    Seja :math:`\ket{\psi} = a\ket{0} + b\ket{1}` o estado de um
    qubit, com :math:`a, b \neq 0`. A medição desse qubit na base
    computacional, isto é, em relação ao observável

    .. math:: Z = 1\cdot \ketbra{0}{0} -1 \cdot \ketbra{1}{1} \ ,

    \ tem dois possíveis resultados mutuamente excludentes:
    :math:`1` ou :math:`-1`. As probabilidades de se obter esses
    resultados são dadas a seguir.

    O resultado :math:`1` ocorre com probabilidade

    .. math:: p(1) =  ||\ketbra{0}{0}\ket{\psi}||^2= ||\ket{0|| a}^2 = |a|^2 \ ,

    e o estado do sistema após a medida é

    .. math:: \ket{\psi'} = \frac{\ketbra{0}{0} \ket{\psi}}{||\ketbra{0}{0}\ket{\psi}||} = \frac{a}{|a|} \ket{0} \ .

    O resultado :math:`-1` ocorre com probabilidade

    .. math:: p(-1) =  ||\ketbra{1}{1}\ket{\psi}||^2 = ||\ket{1|| b}^2 = |b|^2 \ ,

    e o estado do sistema após a medida é

    .. math:: \ket{\psi'} = \frac{\ketbra{1}{1} \ket{\psi}}{||\ketbra{1}{1}\ket{\psi}||} = \frac{b}{|b|} \ket{1} \ .

    Se o qubit estava no estado :math:`\ket{\psi} = \ket{0}`, a
    probabilidade de se obter o resultado :math:`1` na medição é
    :math:`p(1) = 1 = 100\%`, e o estado após a medição é
    :math:`\ket{\psi'} = \ket{0}`. Subsequentes medições
    continuarão fornecendo o resultado :math:`1` e mantendo o
    estado final em :math:`\ket{0}`.

    Da mesma forma, se o qubit estava no estado
    :math:`\ket{\psi} = \ket{1}`, a probabilidade de se obter o
    resultado :math:`-1` na medição é :math:`p(-1) = 1 = 100\%`; o
    estado após a medição é :math:`\ket{\psi'} = \ket{1}` e
    subsequentes medições continuarão fornecendo o resultado
    :math:`-1` e mantendo o estado final em :math:`\ket{1}`.


Valor esperado de um Observável
-------------------------------

Um *ensemble* é uma coleção suficientemente grande de sistemas
preparados no mesmo estado. Se realizarmos a medida de um
observável em um ensemble, cada sistema tem um resultado
possivelmente diferente, com probabilidades de acordo com o
postulado 3. Pode-se extrair o valor esperado desse resultado, ou
seja, a média dos resultados das medidas desses sistemas
idênticos.

.. admonition:: Definição 2.5
    :class: tip

    Seja :math:`A` um observável, isto é, um operador hermitiano no
    espaço de estados do sistema. O *valor esperado* de :math:`A`
    para esse enum ensemble preparado no estado :math:`\ket{\psi}`,
    é dado pelo número real

    .. math:: \braket{A}  = \braket{\psi | A | \psi} \ .

.. admonition:: Observação 2.6
    :class: hint

    O valor esperado do observável depende do estado em que é
    preparado o ensemble. Por isso, pode-se usar algum rótulo para
    lembrar qual é o estado do ensemble:

    .. math:: \braket{A} = \braket{A}_\psi \ .

    É comum também explicitar o estado do ensemble denotando o
    valor esperado simplesmente por :math:`\braket{\psi | A | \psi}`.

.. admonition:: Observação 2.7
    :class: hint

    O valor esperado dado na definição
    `[cap3:def_valor_esperado] <#cap3:def_valor_esperado>`__
    corresponde à média dos resultados das medidas dos sistemas,
    como dito anteriormente. Sejam :math:`A = \sum_k a_k P_k` o
    referido observável e sua decomposição espectral. Sejam
    :math:`\ket{\psi}` o estado de todos os sistemas do ensemble e
    :math:`N` é o número de sistemas do ensemble. O valor esperado
    do observável :math:`A`, denotado por :math:`\braket{A}`,
    depende do estado :math:`\ket{\psi}` em que os sistemas se
    encontram e é dado pela média dos resultados das medidas. Como
    a probabilidade de se obter :math:`a_k` na medição é
    :math:`p(a_k) = ||P_k \ket{\psi||}^2`, como informado no
    postulado 3, o número de sistemas com resultado :math:`a_k` é
    :math:`N p(a_k)`, e a média dos resultados é

    .. math::
        \begin{split}
              \braket{A}
              &= \frac{1}{N}\sum_k a_k \cdot N p(a_k) \\
              &= \sum_k a_k p(a_k) \\
              &= \sum_k a_k ||P_k \ket{\psi||}^2 \\
              &= \sum_k a_k \bra{\psi}P_k^\dagger P_k \ket{\psi}  \\
              &= \sum_k a_k  \bra{\psi} P_k \ket{\psi} \\
              &= \braket{\psi | \sum_k a_k P_k | \psi} \\
              &= \braket{\psi | A | \psi} \ ,
        \end{split}

    lembrando que :math:`P_k`, o operador projeção, é hermitiano
    (:math:`P_k^\dagger = P_k`) e vale que :math:`P_k^2 = P_k`.


Sistemas Compostos
------------------


O último postulado refere-se à união de dois ou
mais sistemas em um único sistema composto. A ferramenta
matemática que descreve adequadamente essa composição é o produto
tensorial.

.. admonition:: Postulado 4

    A composição de sistemas dados por :math:`\mathcal{H}_1`,
    :math:`\mathcal{H}_2`, :math:`\ldots`, :math:`\mathcal{H}_N` é
    descrita pelo produto tensorial

    .. math:: \mathcal{H}_1 \otimes \mathcal{H}_2 \otimes \ldots \otimes \mathcal{H}_N \ .

    Se o estado de cada sistema é
    :math:`\ket{\psi_1} \in \mathcal{H}_1` ,
    :math:`\ket{\psi_2} \in \mathcal{H}_2`, :math:`\ldots` ,
    :math:`\ket{\psi_N} \in \mathcal{H}_N`, então o estado do
    sistema composto é dado pelo produto tensorial

    .. math:: \ket{\psi_1} \otimes \ldots \otimes \ket{\psi_N} \in \mathcal{H}_1 \otimes \ldots \otimes \mathcal{H}_N

    dos estados dos sistemas componentes.

.. admonition:: Exemplo 2.8
    :class: tip


    Suponha que se tenha dois qubits
    :math:`\mathcal{H}_1 = \mathbb{C}^2` e
    :math:`\mathcal{H}_2 = \mathbb{C}^2`, preparados nos estados

    .. math::

    \begin{aligned}
       \ket{\psi_1} &=& \frac{1}{\sqrt{2}} \ket{0} + \frac{1}{\sqrt{2}} \ket{1} \\
       \ket{\psi_2} &=& \ket{0} \ .
    \end{aligned}

    A composição dos dois qubits é modelada pelo espaço de Hilbert
    :math:`\mathcal{H} = \mathcal{H}_1 \otimes \mathcal{H}_2 = \mathbb{C}^2 \otimes \mathbb{C}^2`,
    e o estado do sistema composto é descrito por

    .. math::
        \begin{aligned}
           \ket{\psi}_{1,2}
           &=&
           \ket{\psi_1}_1 \otimes \ket{\psi_2}_2 \\
           &=&
           \left( \frac{1}{\sqrt{2}} \ket{0} + \frac{1}{\sqrt{2}} \ket{1}  \right)_1 \otimes \ket{0}_2 \\
           &=&
           \frac{1}{\sqrt{2}} \ket{0}_1 \otimes \ket{0}_2 + \frac{1}{\sqrt{2}} \ket{1}_1 \otimes \ket{0}_2 \\
           &=&
           \frac{1}{\sqrt{2}} \ket{0}_1 \ket{0}_2 + \frac{1}{\sqrt{2}} \ket{1}_1 \ket{0}_2 \\
           &=&
           \frac{1}{\sqrt{2}} \ket{00}_{1,2} + \frac{1}{\sqrt{2}} \ket{10}_{1,2} \\
           &=&
           \frac{1}{\sqrt{2}} \ket{00} + \frac{1}{\sqrt{2}} \ket{10} \ .
        \end{aligned}

    As últimas 4 igualdades trazem diferentes notações para o mesmo
    vetor de estado. É comum omitir o símbolo :math:`\otimes` e
    escrever as entradas tensoriais justapostas, ou ainda, dentro
    do mesmo ket. Para não haver confusão, pode-se usar índices
    para rotular os qubits (como os índices 1 e 2 usados acima).
    Caso não sejam utilizados os índices, a ordem em que os vetores
    são escritos na justaposição nos mostra a qual entrada
    tensorial cada vetor pertence, e um cuidado extra deve ser
    observado para não trocar a ordem das entradas. Por exemplo,
    :math:`\ket{10} \neq \ket{01}`. Mais detalhes sobre a notação
    podem ser vistos na seção
    `[cap2:produto_tensorial_notacao] <#cap2:produto_tensorial_notacao>`__.

O produto tensorial permite que o sistema composto tenha uma
propriedade muito importante: o *emaranhamento*.

.. admonition:: Definição 2.9
    :class: tip

    Um sistema composto está em um *estado emaranhado* quando não é
    possível escrever o estado composto como um produto tensorial
    de estados individuais dos qubits:

    .. math:: \ket{\psi} \neq \ket{\psi_1} \otimes \ldots \otimes \ket{\psi_n} \ .

    Se o estado do sistema puder ser escrito dessa forma fatorada,
    diz-se que é um *estado separável*, ou *fatorável*, ou ainda,
    *estado produto*.

    Para um sistema composto por 2 qubits, um estado
    :math:`\ket{\psi}` é separável se e somente se puder ser escrito
    na forma

    .. math::
        \begin{aligned}
         \ket{\psi}
         &=& \big( a \ket{0} + b \ket{1} \big) \big( c\ket{0} + d \ket{1} \big) \\
         &=& ac \ket{00} + ad \ket{01} + bc \ket{10} + bd \ket{11}
        \end{aligned}

.. admonition:: Example 2.10
    :class: hint

        Num sistema composto por 2 qubits, o estado

        .. math:: \ket{\beta_{00}} =  \frac{\ket{00} + \ket{11}}{\sqrt{2}}

        é um estado emaranhado. De fato, se fosse um estado separável,
        poderia ser escrito como

        .. math:: \frac{\ket{00} + \ket{11}}{\sqrt{2}} = ac \ket{00} + ad \ket{01} + bc \ket{10} + bd \ket{11} \ .

        Se isso ocorresse, então

        .. math::
            \begin{cases}
               ac = \tfrac{1}{\sqrt{2}} & (1) \\
               ad = 0  & (2) \\
               bc = 0 & (3) \\
               bd =  \tfrac{1}{\sqrt{2}}  \ .  & (4)
              \end{cases}

        O sistema não tem solução, pois as equações (1) e (4) implicam
        :math:`a,b,c,d \neq 0` e a equação (2) implica :math:`a=0` ou
        :math:`d=0`, levando a uma contradição.

        Se tentarmos medir apenas o primeiro qubit de
        :math:`\ket{\beta_{00}}_{1,2}` (na base computacional), teremos
        o resultado :math:`\ket{0}_{1}` com probabilidade
        :math:`\tfrac{1}{2}` e :math:`\ket{1}_{1}` com probabilidade
        também :math:`\tfrac{1}{2}`. Se o resultado da medição for
        :math:`\ket{0}_1`, então o estado do sistema composto será
        :math:`\ket{00}_{1,2}`, e portanto o segundo qubit também
        colapsará para o estado :math:`\ket{0}_2`. Da mesma forma, se o
        resultado da medição for :math:`\ket{1}_1`, o estado do sistema
        composto ficará :math:`\ket{11}_{1,2}`, e o segundo qubit
        colapsará para o estado :math:`\ket{1}_2`. Medir o estado do
        qubit 1 afeta o qubit 2, se ambos estiverem emaranhados, mesmo
        que estejam muito distantes!


Matrizes de Pauli
=================

As matrizes de Pauli são algumas das ferramentas
mais corriqueiras em Computação Quântica. Essas matrizes já foram
introduzidas no capítulo de Álgebra Linear, no exemplo
`[cap2:ex_matrizes_de_pauli] <#cap2:ex_matrizes_de_pauli>`__, e
algumas propriedades já foram abordadas nos exemplos
`[cap2:ex_autovalores_z] <#cap2:ex_autovalores_z>`__,
`[cap2:ex_autovetores_X] <#cap2:ex_autovetores_X>`__ e
`[cap2:ex_diagonalizacao_X] <#cap2:ex_diagonalizacao_X>`__. As
principais informações a respeito dessas matrizes são reunidas nesta
seção por conveniência.



Definição
---------

As matrizes de Pauli são reproduzidas na tabela a seguir.

.. csv-table::
    :header-rows: 1
    :delim: &

    Notação & Ação na base :math:`\ket{0} , \, \ket{1}` & Matriz na base :math:`\ket{0} , \, \ket{1}`
    :math:`X = \sigma_x = \sigma_1` &:math:`\begin{array}{l} X\ket{0} = \ket{1} \\ X \ket{1} = \ket{0} \end{array}` &:math:`\begin{bmatrix}  0 \phantom{-}1  \\  1 \phantom{-}0 \end{bmatrix}`
    :math:`Y  = \sigma_y = \sigma_2` &:math:`\begin{array}{l} Y\ket{0} = \phantom{-}i\ket{1} \\ Y \ket{1} = -i\ket{0} \end{array}` &:math:`\begin{bmatrix} 0 -i \\ i \phantom{-}0 \end{bmatrix}`
    :math:`Z  = \sigma_z = \sigma_3` &:math:`\begin{array}{l} Z\ket{0} = \phantom{-}\ket{0} \\ Z \ket{1} = -\ket{1} \end{array}` &:math:`\begin{bmatrix} 1 \phantom{-}0 \\ 0 -1 \end{bmatrix}`

Propriedades
------------

As matrizes de Pauli são *hermitianas* e *unitárias*. Para
verificar isso, basta perceber que cada matriz é igual à sua
adjunta (conjugada transposta) e que suas colunas são ortonormais.
Portanto vale que

.. math::
    \begin{split}
       X^\dagger &= X \\
       Y^\dagger &= Y \\
       Z^\dagger &= Z
      \end{split}
      \quad \quad \quad
        \begin{split}
         X^\dagger &= X^{-1} \\
         Y^\dagger &= Y^{-1} \\
         Z^\dagger &= Z^{-1} \ .
    \end{split}

Dessas igualdades segue que cada matriz de Pauli é igual à sua
inversa, e que portanto, o seu quadrado é a matriz identidade.

.. math::
    \begin{split}
         X^2 &= I \\
         Y^2 &= I \\
         Z^2 &= I \ .
    \end{split}

Há outras identidades envolvendo o produto de duas matrizes de
Pauli diferentes, e que podem ser obtidas por cálculo direto.

.. math::

 \begin{split}
         XY &= iZ \\
         YZ &= iX \\
         ZX &= iY
        \end{split}
        \quad \quad \quad
        \begin{split}
         YX &= -iZ \\
         ZY &= -iX \\
         XZ &= -iY \ .
        \end{split}

As identidades da coluna à direita podem ser obtidas fazendo-se o
hermitiano das igualdades da coluna à esquerda.

.. container:: subsection

Autovalores, Autovetores e Diagonalização
-----------------------------------------

Por serem matrizes hermitianas e unitárias, são matrizes normais
(isto é, comutam com sua adjunta). São, pois, diagonalizáveis pelo
teorema espectral
`[cap2:teorema_espectral_op_normal] <#cap2:teorema_espectral_op_normal>`__,
têm autovalores reais por serem hermitianas e seus autovalores
devem ter módulo 1 por serem matrizes unitárias. Um cálculo
explícito fornece os autovalores :math:`1` e :math:`-1`. Cada
matriz possui uma base de autovetores diferente, denotada por
:math:`\mathcal{X}`, :math:`\mathcal{Y}` e :math:`\mathcal{Z}` e
apresentadas no exemplo
`[cap2:bases_1qubit] <#cap2:bases_1qubit>`__. O cálculo dos
autovalores e autovetores segue as diretrizes do exemplo
`[cap2:ex_autovetores_X] <#cap2:ex_autovetores_X>`__ e
`[cap2:ex_diagonalizacao_X] <#cap2:ex_diagonalizacao_X>`__, em que
são encontrados os autovalores, autovetores e a forma diagonal da
matriz :math:`X`.

.. csv-table:: Autovalores, base de autovetores e forma diagonal dos operadores :math:`X`, :math:`Y` e :math:`Z` de Pauli.
    :header-rows: 1
    :delim: &


    Matriz & Autovalores & Base de Autovetores & Forma diagonal
    :math:`X` & 1 , -1 &:math:`\mathcal{X} = \big\{ \ket{+} , \ket{-} \big\}`&:math:`X =  \ketbra{+}{+} -  \ketbra{-}{-}`
    :math:`Y` & 1 , -1 &:math:`\mathcal{Y} = \big\{ \ket{+i} , \ket{-i} \big\}`&:math:`Y =  \ketbra{+i}{+i} -  \ketbra{-i}{-i}`
    :math:`Z` & 1 , -1 &:math:`\mathcal{Z} = \big\{ \ket{0} , \ket{1} \big\} = \mathcal{I}`&:math:`Z =  \ketbra{0}{0} -  \ketbra{1}{1}`


Os vetores :math:`\ket{+}`, :math:`\ket{-}`, :math:`\ket{+i}` e :math:`\ket{-i}` são dados por:

.. math::
    \begin{array}{lll}
        \ket{+} = \frac{1}{\sqrt{2}} \ket{0} + \frac{1}{\sqrt{2}} \ket{1}
        & &
        \ket{+i} = \frac{1}{\sqrt{2}} \ket{0} + i \frac{1}{\sqrt{2}} \ket{1}
        \\
        \ket{-} = \frac{1}{\sqrt{2}} \ket{0} - \frac{1}{\sqrt{2}} \ket{1}
        & &
        \ket{-i} = \frac{1}{\sqrt{2}} \ket{0} -i \frac{1}{\sqrt{2}} \ket{1} \ .
    \end{array}

.. container:: section

Estados de Bell
===============

Os *estados de Bell* são estados emaranhados que formam uma base
ortonormal :math:`\mathcal{B}` para o espaço de estados de 2 qubits.
Um deles já foi visto no exemplo
`[cap3:ex_estado_emaranhado] <#cap3:ex_estado_emaranhado>`__. Os 4
estados de Bell estão dispostos a seguir.

.. math::
  \begin{split}
     \ket{\beta_{00}} &=  \ket{\Phi^+} = \frac{1}{\sqrt{2}}\ket{00} + \frac{1}{\sqrt{2}}\ket{11} \\
     \ket{\beta_{01}} &=  \ket{\Phi^-} = \frac{1}{\sqrt{2}}\ket{00} - \frac{1}{\sqrt{2}}\ket{11} \\
     \ket{\beta_{10}} &=  \ket{\Psi^+} =\frac{1}{\sqrt{2}}\ket{01} + \frac{1}{\sqrt{2}}\ket{10} \\
     \ket{\beta_{11}} &=  \ket{\Psi^-} =\frac{1}{\sqrt{2}}\ket{01} - \frac{1}{\sqrt{2}}\ket{10}
    \end{split}

Na literatura encontram-se os dois tipos de notação.

.. admonition:: Proposição 2.11
    :class: note

    Os estados de Bell formam uma base ortonormal para o espaço de
    estados de 2 qubits :math:`\mathbb{C}^2 \otimes \mathbb{C}^2`.

    Demonstração

    | :math:`\mathcal{B}` é base de
    :math:`\mathbb{C}^2 \otimes \mathbb{C}^2`:
    | A dimensão de :math:`\mathbb{C}^2 \otimes \mathbb{C}^2` é 4,
    portanto basta mostrar que os 4 estados de Bell são LI. Seja

    .. math:: a_0 \ket{\beta_{00}} + a_1 \ket{\beta_{01}} + a_2 \ket{\beta_{10}} + a_3 \ket{\beta_{11}} = 0

    uma combinação linear nula dos elementos de :math:`\mathcal{B}`.
    Tem-se que:

    .. math::

       \begin{split}
              a_0 \ket{\beta_{00}} + a_1 \ket{\beta_{01}} + a_2 \ket{\beta_{10}} + a_3 \ket{\beta_{11}}
              &= 0 \\
              a_0 \frac{\ket{00} + \ket{11}}{\sqrt{2}}  + a_1  \frac{\ket{00} - \ket{11}}{\sqrt{2}} + a_2 \frac{\ket{01} + \ket{10}}{\sqrt{2}}  + a_3 \frac{\ket{01} - \ket{10}}{\sqrt{2}}
              &= 0  \\
              \frac{a_0 + a_1}{\sqrt{2}} \ket{00} + \frac{a_2 + a_3}{\sqrt{2}} \ket{01} + \frac{a_2 - a_3}{\sqrt{2}} \ket{10} + \frac{a_0 1 a_1}{\sqrt{2}} \ket{11}
              &= 0
             \end{split}

    Portanto

    .. math::

       \begin{cases}
              a_0 + a_1 = 0 \\
              a_0 - a_1 = 0 \\
              a_2 + a_3 = 0 \\
              a_2 - a_3 = 0
             \end{cases}
             \implies
             \begin{cases}
              a_0  = 0 \\
              a_1 = 0 \\
              a_2 = 0 \\
              a_3 = 0  \ ,
             \end{cases}

    e como a única solução para os coeficientes é a solução nula, os
    vetores são LI. Formam, em consequência, uma base do espaço de 2
    qubits.
    | :math:`\mathcal{B}` é ortonormal:
    | Os vetores têm norma 1 pois

    .. math::

       \begin{split}
                 \braket{\beta_{00}}{\beta_{00}}
                 &= \frac{\bra{00} + \bra{11}}{\sqrt{2}} \frac{\ket{00} + \ket{11}}{\sqrt{2}} = \frac{1+0+0+1}{2} = 1 \\
                 \braket{\beta_{01}}{\beta_{01}}
                 &= \frac{\bra{00} - \bra{11}}{\sqrt{2}} \frac{\ket{00} - \ket{11}}{\sqrt{2}} = \frac{1-0-0+1}{2} = 1 \\
                 \braket{\beta_{10}}{\beta_{10}}
                 &= \frac{\bra{01} + \bra{10}}{\sqrt{2}} \frac{\ket{01} + \ket{10}}{\sqrt{2}} = \frac{1+0+0+1}{2} = 1 \\
                 \braket{\beta_{11}}{\beta_{11}}
                 &= \frac{\bra{01} - \bra{10}}{\sqrt{2}} \frac{\ket{01} - \ket{10}}{\sqrt{2}} = \frac{1-0-0+1}{2} = 1 \ ,
                \end{split}

    e são ortogonais porque

    .. math::

       \begin{split}
                 \braket{\beta_{00}}{\beta_{01}}
                 &=\frac{\bra{00} + \bra{11}}{\sqrt{2}} \frac{\ket{00} - \ket{11}}{\sqrt{2}} = \frac{1-0+0-1}{2} = 0
                 \\
                 \braket{\beta_{00}}{\beta_{10}}
                 &=\frac{\bra{00} + \bra{11}}{\sqrt{2}} \frac{\ket{01} + \ket{10}}{\sqrt{2}} = \frac{0+0+0+0}{2} = 0
                 \\
                 \braket{\beta_{00}}{\beta_{11}}
                 &=\frac{\bra{00} + \bra{11}}{\sqrt{2}} \frac{\ket{01} - \ket{10}}{\sqrt{2}} = \frac{0-0+0-0}{2} = 0
                 \\
                 \braket{\beta_{01}}{\beta_{10}}
                 &=\frac{\bra{00} - \bra{11}}{\sqrt{2}} \frac{\ket{01} + \ket{10}}{\sqrt{2}} = \frac{0+0-0-0}{2} = 0
                 \\
                 \braket{\beta_{01}}{\beta_{11}}
                 &=\frac{\bra{00} - \bra{11}}{\sqrt{2}} \frac{\ket{01} - \ket{10}}{\sqrt{2}} = \frac{0-0-0+0}{2} = 0
                 \\
                 \braket{\beta_{10}}{\beta_{11}}
                 &=\frac{\bra{01} + \bra{10}}{\sqrt{2}} \frac{\ket{01} - \ket{10}}{\sqrt{2}} = \frac{1-0+0-1}{2} = 0 \ .
                 \end{split}

    Logo a base é ortonormal. ◻

.. admonition:: Proposição 2.11
    :class: note


    Os estados de Bell são emaranhados (isto é, não podem ser escritos
    como o produto tensorial de estados de 1 qubit).

    .. container:: proof

      *Proof.* As contas são análogas às do exemplo
      `[cap3:ex_estado_emaranhado] <#cap3:ex_estado_emaranhado>`__, que
      mostra que o estado :math:`\ket{\beta_{00}}` é emaranhado. ◻

Os estados de Bell são muito utilizados pelo fato de formarem uma
base de estados emaranhados. Algumas aplicações mais elementares
ocorrem no circuito de teletransporte e na codificação superdensa,
que serão vistos no capítulo `[cap5:Alg_Q] <#cap5:Alg_Q>`__, seções
`[cap5:sd_coding] <#cap5:sd_coding>`__ e
`[cap5:telep_circ] <#cap5:telep_circ>`__, respectivamente.

.. [1]
   Hall, B.C. (2013), Quantum Theory for Mathematicians, Graduate Texts
   in Mathematics, 267, Springer.

.. [2]
   Perceba que o limite :math:`n\to \infty` que surge é análogo ao
   limite fundamental

   .. math:: e^{a} = \lim_{n\to \infty} \left( 1 - \frac{a}{n} \right)^n \ .

.. [3]
   Uma discussão interessante sobre o colapso da função de onda do
   sistema devido à medida é feita em
   :raw-latex:`\cite{book:iqm_griffiths}`, p. 2-5.
