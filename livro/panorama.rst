.. role:: raw-latex(raw)
   :format: latex
..

.. _`cap6:panorama_CQ`:

Panorama Atual da Computação Quântica
=====================================

A Computação Quântica é uma tecnologia ainda em estágio inicial, e tem
se mostrado uma área de pesquisa estratégica no cenário internacional.
Neste capítulo, faz-se um breve estudo das expectativas de mercado em
relação à Computação Quântica, em grande parte baseado na Gartner, uma
consultoria em Tecnologia da Informação
:raw-latex:`\cite{webinar:gartner}`. Apresenta-se também algumas das
principais empresas com pesquisas em Computação Quântica, e mostra-se um
resumo dos desenvolvimentos realizados nessas empresas tanto em hardware
como em software.

Expectativas de Mercado
-----------------------
.. container:: section

   A Gartner :raw-latex:`\cite{webinar:gartner}` estima que a Computação
   Quântica será uma realidade no mercado dentro de 5 a 10 anos.
   Espera-se que essa tecnologia se torne mais rápida e escalável, de
   forma a tratar problemas reais que a computação atual não conseguiria
   abordar satisfatoriamente. A curva da figura
   `1.1 <#cap6:fig_hype_cicle>`__, chamada *hype-cicle*, mostra as
   tecnologias em alta no mercado e as que já atingiram maturidade. A
   posição da Computação Quântica é de subida na curva de expectativas.

   .. figure:: cap6/figuras/hype_cicle_gartner.png
      :alt: 

   Há uma percepção no mercado que a Computação Quântica gere uma
   disrupção algo similar às ocorridas na Revoluções Industrial,
   Tecnológica e Digital. O panorama atual da Computação Quântica é
   comparável, nessa percepção, ao computador ENIAC da decada de 1950
   (para comparar, ver figura `1.2 <#cap6:fig_eniac>`__). À época, não
   havia como prever o desenvolvimento tecnológico subsequente, e a
   capacidade de processamento que se conseguiria obter nas décadas
   seguintes.

   .. figure:: cap6/figuras/eniac.jpg
      :alt: 

   A expectativa é que um modelo híbrido entre Computação Clássica e
   Quântica seja a tecnologia emergente no cenário atual. A computação
   seria realizada em um processador quântico para problemas nos quais
   há vantagens no uso da Computação Quântica, como espera-se que ocorra
   com os problemas chamados NP-difíceis.

   Dentre as principais aplicações previstas para a Computação Quântica,
   destaca-se resolução de problemas de otimização, machine learning,
   desenvolvimento de novos materiais, fármacos e processos químicos.
   Outra aplicação de destaque é em criptografia. A figura
   `1.3 <#cap6:fig_aplic_cq_1>`__ mostra aplicações em que se espera
   utilizar processamento quântico.

   .. figure:: cap6/figuras/qc_applications.png
      :alt: 

   O impacto da Computação Quântica em sistemas de segurança é
   considerado certo dentro de poucos anos. Algumas tecnologias atuais
   como a *criptografia RSA* e o *blockchain* (do qual o *bitcoin* faz
   uso, em particular) seriam vulneráveis à Computação Quântica. Há
   demanda por protocolos de criptografia que levem em conta a
   existência da Computação Quântica, isto é, por criptografia
   *pós-quântica* (*post-quantum*/*quantum-safe*/*quantum-proof
   cryptography*).

Empresas e Desenvolvimentos Atuais
-----------------------
.. container:: section

   A Computação Quântica encontra-se em um estágio de desenvolvimento
   acelerado. Empresas como IBM, Google, Intel e Microsoft, e Startups
   como Rigetti, têm pesquisas para desenvolvimento de processadores
   quânticos. Algumas dessas empresas, como a IBM e Rigetti,
   disponibilizam protótipos de computadores quânticos para acesso pela
   nuvem ao público. Também há desenvolvimento de simuladores,
   linguagens de programação para Computação Quântica e kits de
   softwares por essas empresas. Alguns comentários são desenvolvidos no
   que segue.

   .. rubric:: IBM
      :name: ibm
      :class: unnumbered

   A IBM utiliza tecnologia de qubits supercondutores, compostos por
   junções de Josephson. Em 10 de novembro de 2017, foram anunciados
   protótipos de 50 e de 20 qubits, em uma iniciativa de tornar a
   computação quântica comercialmente disponível em um futuro próximo.

   No projeto IBM Quantum Experience, um computador quântico de 5 qubits
   está disponível para uso através da nuvem. A programação é feita por
   uma interface gráfica, que permite realizar simulação e/ou enviar o
   algoritmo para execução no computador quântico. A programação pode
   ser realizada em modo texto, com a linguagem OpenQASM (Open Quantum
   Assembly Language). Em relação à simulação, a IBM também colabora no
   projeto open source QISKit (Quantum Information Software Kit), uma
   biblioteca python para Computação e Informação Quântica que funciona
   em conjunto com OpenQASM.

   |  
   | Fonte:
   | ``http://newsroom.ibm.com/IBM-research?item=30270``
   | ``http://newsroom.ibm.com/``
   | ``https://www.research.ibm.com/ibm-q/``
   | ``https://quantumexperience.ng.bluemix.net/qx/experience``
   | ``https://qiskit.org/``
   | ``https://github.com/QISKit``
   | ``https://github.com/QISKit/openqasm``

   .. rubric:: Google
      :name: google
      :class: unnumbered

   O processador quântico Bristlecone, de 72 qubits, foi apresentado em
   5 de março de 2018. A tecnologia de qubits da Google é baseada em
   filmes supercondutores de alumínio em substrato de safira. Para
   simulação de computadores quânticos, a empresa tem um projeto open
   source chamado Quantum Playground, em que é possível simular um
   computador quântico de até 22 qubtis.

   |  
   | Fonte:
   | ``https://ai.googleblog.com/2018/03/a-preview-of-bristlecone-googles-new.html``
   | ``https://ai.googleblog.com/2015/03/a-step-closer-to-quantum-computation.html``
   | ``https://www.nature.com/articles/nature14270``
   | ``http://www.quantumplayground.net``
   | ``https://opensource.google.com/projects/quantum-computing-playground``
   | ``https://github.com/gwroblew/Quantum-Computing-Playground``

   .. rubric:: Intel
      :name: intel
      :class: unnumbered

   Em 8 de janeiro de 2018 foi anunciado pela Intel a fabricação de um
   processador quântico, em fase de teste, de 49 qubits supercondutores.
   A tecnologia utilizada atualmente para realizar os qubits são as
   junções de Josephson, consistindo em uma camada fina de óxido entre
   dois fios de alumínio. A Intel também pesquisa qubits de spin em
   silício.

   |  
   | Fonte:
   | ``https://newsroom.intel.com/news/intel-advances-quantum-neuromorphic-``
   | ``computing-research/``
   | ``https://newsroom.intel.com/press-kits/quantum-computing/``

   .. rubric:: Microsoft
      :name: microsoft
      :class: unnumbered

   A Microsoft vem empregando esforços na elaboração de qubits
   topológicos por meio de férmions de Majorana. Além disso, foi lançado
   em 11 de dezembro de 2017 o Microsoft Quantum Development Kit,
   contendo a linguagem de programação Q# dedicada para Computação
   Quântica, e acompanhada de simuladores.

   |  
   | Fonte:
   | ``https://www.microsoft.com/en-us/quantum/technology``
   | ``https://cloudblogs.microsoft.com/quantum/2017/12/11/announcing-microsoft-``
   | ``quantum-development-kit/``

   .. rubric:: Rigetti
      :name: rigetti
      :class: unnumbered

   A Startup Rigetti disponibiliza um processador quântico de 19 qubits
   e um ambiente de desenvolvimento, chamado Forest, para programação
   quântica. A empresa também disponibiliza um simulador, chamado
   Quantum Virtual Machine, de 26 qubits. A Rigetti também desenvolveu a
   linguagem open source Quil, que baseia-se em um modelo computacional
   clássico/quântico com memória compartilhada, e trabalha em uma
   biblioteca python para programação quântica, a pyQuil.

   |  
   | Fonte:
   | ``https://www.rigetti.com/``
   | ``https://www.rigetti.com/forest``
   | ``https://github.com/rigetticomputing/pyQuil``

   .. rubric:: Outras Startups e Empresas
      :name: outras-startups-e-empresas
      :class: unnumbered

   .. figure:: cap6/figuras/early_pioneers_in_qc.png
      :alt: 
