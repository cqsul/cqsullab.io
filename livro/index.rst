############################
Livro de Computação Quântica
############################

**********
Introdução
**********

Breve Histórico da Computação Quântica
======================================

No início do século XX, a Física Clássica enfrentava dificuldades em descrever alguns fenômenos observados à época, como, por exemplo, o espectro de radiação de corpo negro e o efeito fotoelétrico. Essa crise culminou na criação da Mecânica Quântica, que se consolidou por volta da década de 1920, e tem sido aplicada com sucesso em diversos fenômenos. (:cite:`book:qcqi_nc`, p.2).

O desenvolvimento tecnológico a partir da década de 1970 permitiu o controle de sistemas quânticos individuais permitindo-se aprisionar átomos individuais em armadilhas (`traps`), isolando-os do restante do ambiente, e medindo seus diversos aspectos com precisão notável. Nesse contexto, passou-se a considerar a possibilidade de se usar sistemas quânticos para realizar processamento e transmissão de informação, fazendo uso de fundamentos da Mecânica Quântica, como superposição e emaranhamento. Esses sistemas guardam analogia com os bits clássicos, e são chamados de qubits. (:cite:`book:qcqi_nc` p.3-4).

O físico R. Feynman, na década de 1980, sugeriu o uso de computadores quânticos para simular sistemas quânticos. E em 1994, o matemático P. Shor propôs um algoritmo quântico capaz de resolver o problema de fatoração de números em fatores primos de forma mais eficiente que os algoritmos clássicos conhecidos. Há uma expectativa de que tal algoritmo possa ameaçar alguns protocolos de criptografia, como o RSA, usado largamente na atualidade. O cientista da computação Lov Grover também elaborou um algoritmo quântico de busca em uma base de dados não estruturada, que possui ganho quadrático de desempenho em comparação ao melhor algoritmo clássico conhecido. (:cite:`book:pqci_benenti`, p.3).

Atualmente, a Computação Quântica e a Informação Quântica estão se consolidando como áreas de pesquisa com desenvolvimento acelerado nas últimas décadas. Empresas de tecnologia como IBM, Google, Intel e Microsoft têm projetos e pesquisas nessa área, e diversas Startups têm surgido nesse contexto. 

Motivação para a Computação Quântica
====================================


A `lei de Moore`, formulada em 1965, previu que a capacidade computacional dos sistemas digitais dobraria a cada dois
anos. Surpreendentemente, os avanços computacionais se mantiveram aproximadamente nesse ritmo até a atualidade. No entanto, à medida que a escala dos transistores se reduz, aproxima-se de limites físicos fundamentais, e aparentemente insuperáveis. Os efeitos quânticos, em alguns aspectos indesejáveis (tal como o tunelamento),
passam a interferir intensamente no funcionamento ideal do transistor. Para que o ritmo ditado pela lei de Moore continue, tem-se
apostado, entre outras abordagens, na Computação Quântica.
(:cite:`book:qcqi_nc`, p.4 e :cite:`book:pqci_benenti`, p.2).

Outro ponto refere-se ao consumo de energia. O princípio
de Landauer afirma que para cada bit de informação apagado,
dissipa-se no ambiente a energia correspondente a pelo menos $k T \ln 2$, em quem $k$ é a constante de Boltzmann e $T$ é a temperatura do sistema dada em Kelvin. Pelas características da Mecânica Quântica, as portas
lógicas quânticas precisam ser reversíveis. Isso corresponderia,
em princípio, à desnecessidade de se apagar informação e à
possibilidade de se ter um computador que não dissipe energia
no processamento. (:cite:`book:pqci_benenti`, p. 3).

Um terceiro ponto motivador das pesquisas em Computação Quântica é que a investigação nessa área pode elucidar aspectos da Mecânica Quântica, frequentemente contra intuitivos
em relação à Física Clássica, ou mesmo, apontar fenômenos
ainda não explorados. (:cite:`book:qcqi_nc`, p. 3).

Por fim, a Computação Quântica apresenta desafios em
diversas áreas, como manipulação de sistemas quânticos, novas
técnicas experimentais, desenvolvimento de algoritmos, teoria da
informação, entre outros, o que a torna bastante atrativa e multidisciplinar.


Sobre o TCC
===========

O presente trabalho tem como objetivo servir como um material introdutório e multidisciplinar sobre Computação Quântica. Visa-se com isso facilitar o primeiro contato com esse assunto por parte de estudantes de Engenharias, Ciências da Computação, Física, Matemática e áreas correlatas. 

O conteúdo disposto neste trabalho começou a ser apresentado em sessões semanais com o orientador no primeiro semestre de 2017. Conforme mais pessoas juntaram-se às reuniões semanais, criou-se o Grupo de Computação Quântica da UFSC (GCQ-UFSC), e as reuniões tiveram objetivo de trazer um material introdutório aos estudantes de graduação do grupo e discutir atualidades da área de maneira acessível. As reuniões do GCQ-UFSC contribuiram grandemente para dar forma ao presente texto.

Os capítulos \ref{cap2_AL} e \ref{cap3_MQ} contêm elementos de Álgebra Linear e Mecânica Quântica, pré-requisitos para o estudo da Computação Quântica, com ênfase nos pontos necessários para o restante do texto. Os capítulos \ref{cap4:CQ} e \ref{cap5:Alg_Q} trazem uma introdução à Computação Quântica com uma abordagem voltada a algoritmos quânticos. O capítulo \ref{cap6:panorama_CQ} contextualiza a Computação Quântica no cenário atual, discutindo expectativas de mercado e algumas das principais empresas envolvidas. Por fim, o capítulo \ref{cap7:CQ_no_IBMQE} apresenta a plataforma IBM Quantum Experience, em que é possível escrever um algoritmo quântico e submeter para execução em um protótipo de computador quântico de 5 qubits, disponível em nuvem.

.. toctree::
    :maxdepth: 2
    :hidden:

    algebra
    introducao
    bibliografia
