.. _`cap7:CQ_no_IBMQE`:

Computação Quântica com IBM Quantum Experience
==============================================

.. container:: section

   IBM Quantum Experience

   | A IBM Research disponibiliza um computador de 5 qubits e um de 16
     qubits acessíveis pela nuvem (IBM cloud). O acesso aos computadores
     se dá por meio do QISKit – Quantum Information Software Kit – um
     pacote de software para python. É possível também acessar o
     computador de 5 qubits por uma interface gráfica no navegador
     (Composer). Os detalhes serão vistos na seção
     `[cap6:sec_como_programar] <#cap6:sec_como_programar>`__.

   .. container:: subsection

      Computadores Disponíveis

      Os computadores quânticos disponíveis para acesso na nuvem são
      listados abaixo.

      .. table:: Processadores quânticos da IBM disponíveis para uso
      pela nuvem. Informação sobre o status dos computadores visualizada
      em maio de 2018.

         ============================= ========= ============================
         Nome                          # qubits  Status
         ============================= ========= ============================
         IBM Q 5 Yorktown (ibmqx2)     5 qubits  manutenção
         IBM Q 5 Tenerife (ibmqx4)     5 qubits  disponível (Composer/QISKit)
         IBM Q 16 Rueschlikon (ibmqx5) 16 qubits disponível (QISKit)
         ============================= ========= ============================

      | Pode-se consultar informações sobre esses processadores no
        GitHub. Os links são disponibilizados abaixo.
      | Informação sobre os processadores:
      | ``https://github.com/QISKit/qiskit-backend-information/tree/master/backends``
      | IBM Q 5 Yorktown (ibmqx2):
      | ``https://github.com/QISKit/qiskit-backend-information/tree/master/backends/``
      | ``yorktown/V1``
      | IBM Q 5 Tenerife (ibmqx4):
      | ``https://github.com/QISKit/qiskit-backend-information/tree/master/backends/``
      | ``tenerife/V1``
      | IBM Q 16 Rueschlikon (ibmqx5):
      | ``https://github.com/QISKit/qiskit-backend-information/tree/master/backends/``
      | ``rueschlikon/V1``

   .. container:: subsection

      Como Programar

      | A programação é feita por uma interface gráfica, como ilustrado
        na figura `1.1 <#cap7:fig_interface_grafica_ibmqx>`__. Além
        disso, é possível realizar a programação em modo texto (figura
        `1.2 <#cap7:fig_interface_texto_ibmqx>`__) por código OpenQASM –
        Open Quantum Assembly Language. Há opções de simular e de
        executar o algoritmo no computador quântico. A página do editor
        de algoritmos da IBM Quantum Experience é acessível pelo link:
      | ``https://quantumexperience.ng.bluemix.net/qx/editor``

      .. figure:: cap6/figuras/ibmqe.png
         :alt: 

      .. figure:: cap6/figuras/ibmqe_qasm.png
         :alt: 

      No IBM Quantum Experience, os qubits são dispostos de acordo com a
      convenção apresentada na figura .

      .. container:: float
         :name: cap7:fig_convencao_ibmqx

         .. math::

            \Qcircuit @C=12pt @R=12pt @!R 
              {
              \lstick{q[0]} & \qw & \qw \\
              \lstick{q[1]} & \qw & \qw \\
              \lstick{q[2]} & \qw & \qw \\
              \lstick{q[3]} & \qw & \qw \\
              \lstick{q[4]} & \qw & \qw
              } \quad \quad \quad
              \begin{array}{c} \\ \\ \\ \text{estado} = q[4]\, q[3]\, q[2]\, q[1]\, q[0]  \end{array}

      As portas lógicas quânticas disponíveis para uso são apresentadas
      na figura `1.4 <#cap7:fig_portas_logicas_no_ibmqx>`__. É possível
      utilizar as portas :math:`X, Y, Z` de Pauli, a porta de Hadamard
      :math:`H`, as portas de fase :math:`S, S^\dagger, T, T^\dagger`. A única
      porta de dois qubits disponível é a CNOT.

      .. figure:: cap6/figuras/quantum_gates.png
         :alt: 

      A porta CNOT não admite controle e alvo em qualquer par de qubits.
      As combinações possíveis são aquelas em que os dois qubits estão
      conectados por barramentos supercondutores. As opções são expostas
      na figura `1.5 <#cap6:fig_cnot_gates>`__.

      .. container:: float
         :name: cap6:fig_cnot_gates

         |image|   |image1|

      É possível realizar outras portas CNOT em função das CNOTs
      nativas. Para tanto, faz-se uso das identidades de circuito
      contidas na seção
      `[cap4:sec_id_circuitos] <#cap4:sec_id_circuitos>`__. Em especial,
      são úteis as proposições
      `[cap4:prop_cnot_troca_controle_alvo] <#cap4:prop_cnot_troca_controle_alvo>`__
      e
      `[cap4:prop_cnot_controle_qubits_distantes] <#cap4:prop_cnot_controle_qubits_distantes>`__.

      .. container:: float
         :name: cap6:fig_cnot_tecnicas

         .. math::

            \Qcircuit @C=10pt @R=4pt {& \targ    & \qw  & &   & & & \gate{H}  & \ctrl{2}  & \gate{H}  & \qw\\
                                       &          &      & & = & & &           &           &           &    \\
                                       & \ctrl{-2}& \qw  & &   & & & \gate{H}  & \targ     & \gate{H}  & \qw  }

         .. math::

            \Qcircuit @C=10pt @R=10pt {
               &  \ctrl{2} & \qw & &   & & & \qw      & \ctrl{1} & \qw      & \ctrl{1} & \qw \\
               &   \qw     & \qw & & = & & & \ctrl{1} & \targ    & \ctrl{1} & \targ    & \qw \\
               & \targ     &\qw  & &   & & & \targ    & \qw      & \targ    & \qw      & \qw  }

      A porta SWAP também pode ser realizada em função de CNOTs nativas.
      Usam-se as proposições
      `[cap4:prop_cnot_troca_controle_alvo] <#cap4:prop_cnot_troca_controle_alvo>`__
      e `[cap4:prop_SWAP_CNOT] <#cap4:prop_SWAP_CNOT>`__ da seção
      `[cap4:sec_id_circuitos] <#cap4:sec_id_circuitos>`__ sobre
      identidade de circuitos.

      .. container:: float
         :name: cap6:fig_swap_tecnicas

         .. math::

            \Qcircuit @C=10pt @R=4pt {& \qswap      & \qw  & &   & & & \ctrl{2} & \targ     & \ctrl{2} & \qw  & &   & & & \ctrl{2} & \gate{H}  & \ctrl{2}  & \gate{H}  & \ctrl{2} & \qw  \\
                                       &  \qwx       &      & & = & & &          &           &          &      & & = & & &          &           &           &           &          &      \\
                                       & \qswap \qwx & \qw  & &   & & & \targ    & \ctrl{-2} & \targ    & \qw  & &   & & & \targ    & \gate{H}  & \targ     & \gate{H}  & \targ    & \qw   }

      A porta Toffoli não é nativa no computador IBM Q 5 Tenerife
      (ibmqx4). Para realizá-la, pode-se aplicar a proposição
      `[cap4:prop_constr_porta_toffoli] <#cap4:prop_constr_porta_toffoli>`__.
      Essa proposição fornece a seguinte construção.

      .. container:: float
         :name: cap6:fig_toffoli_tecnicas

         .. math::

            \Qcircuit @C=6pt @R=4pt 
            {& \ctrl{2}  &\qw & &   & & & \qw      & \qw      & \qw           & \ctrl{4} & \qw      & \qw      & \qw           & \ctrl{4} & \qw           & \ctrl{2} & \qw           & \ctrl{2} & \gate{T}& \qw   \\
             &           &    & &   & & &          &          &               &          &          &          &               &          &               &          &               &          &         &       \\
             & \ctrl{2}  &\qw & & = & & & \qw      & \ctrl{2} & \qw           &  \qw     & \qw      & \ctrl{2} & \qw           & \qw      & \gate{T^\dagger} & \targ    & \gate{T^\dagger} & \targ    & \gate{S}& \qw   \\
             &           &    & &   & & &          &          &               &          &          &          &               &          &               &          &               &          &         &       \\
             & \targ     &\qw & &   & & & \gate{H} & \targ    & \gate{T^\dagger} &   \targ  & \gate{T} & \targ    & \gate{T^\dagger} & \targ    & \gate{T}      & \gate{H} & \qw           & \qw      & \qw     & \qw   }

      | A programação nos computadores quânticos também pode ser feita
        em python por meio dos pacotes QISKit. O acesso ao computador
        IBM Q 16 Rueschlikon, de 16 qubits, é feito apenas dessa forma.
        Um tutorial de QISKit pode ser encontrado no link abaixo.
      | ``https://developer.ibm.com/open/videos/qiskit-quantum-computing-tech-talk/``

   .. container:: subsection

      Informativos e Guias de Usuário

      .. rubric:: User Guides
         :name: user-guides
         :class: unnumbered

      A página do IBM Quantum Experience fornece guias para usuários. Os
      links estão dispostos a seguir.

      |  
      | Beginner’s Guide:
      | ``https://quantumexperience.ng.bluemix.net/qx/tutorial?sectionId=beginners-``
      | ``guide&page=introduction``

      |  
      | Full User Guide:
      | ``https://quantumexperience.ng.bluemix.net/qx/tutorial?sectionId=full-user-``
      | ``guide&page=introduction``

      |  
      | User Guides no GitHub:
      | ``https://github.com/QISKit/ibmqx-user-guides``

      .. rubric:: Informativos
         :name: informativos
         :class: unnumbered

      A página da IBM também fornece material informativo contendo
      palestras e vídeos curtos sobre Computação Quântica.

      ``http://www.research.ibm.com/ibm-q/learn/what-is-quantum-computing/``

.. container:: section

   Circuito Quantum Half Adder

   Nesta seção é apresentado um projeto de somador quântico análogo ao
   somador clássico *half adder*, apresentado no apêndice
   `[cap1:fig_circuito_half_adder] <#cap1:fig_circuito_half_adder>`__.
   Após o projeto, procedeu-se à simulação no IBM Quantum Experience e à
   execução no computador IBM Q 5 Tenerife (ibmqx4).

   .. container:: subsection

      Projeto do Circuito

      O circuito deve realizar um somador half adder para entradas na
      base computacional. A tabela verdade do half adder é apresentada a
      seguir.

      .. container::
         :name: cap6:tab_verdade_half_adder

         .. table:: Tabela verdade para o meio somador clássico (half
         adder). As entradas são simbolizadas por :math:`a` e :math:`b`
         e as saídas são :math:`s` (sum) e :math:`c` (carry out).

            ========= ========= ========= =========
            :math:`a` :math:`b` :math:`s` :math:`c`
            ========= ========= ========= =========
            0         0         0         0
            0         1         1         0
            1         0         1         0
            1         1         0         1
            ========= ========= ========= =========

      As equações booleanas para o bit de soma e o bit “vai um” (carry)
      são

      .. math::

         \begin{split}
               s &= a \oplus b \\
               c &= a \cdot b \ , 
              \end{split}

      em que :math:`\oplus` é a operação XOR (ou exclusivo) e
      :math:`\cdot` é a operação AND.

      Já que há 2 entradas e 2 saídas, é possível tentar realizar esse
      circuito com apenas 2 qubits. No entanto, há duas entradas
      distintas :math:`ab = 10` e :math:`ba = 01` que fornecem o mesmo
      resultado :math:`sc = 10`, de forma que a função booleana não é
      reversível. É necessário, então, usar pelo menos um qubit de
      trabalho. Um circuito quântico capaz de realizar as operações
      desejadas é dado na figura `1.9 <#cap6:fig_quantum_half_adder>`__.

      .. container:: float
         :name: cap6:fig_quantum_half_adder

         .. math::

            \Qcircuit @C=12pt @R=10pt @!R 
              {
              \lstick{\ket{a}} & \ctrl{1} & \ctrl{1} & \rstick{\ket{a}} \qw \\
              \lstick{\ket{b}} & \ctrl{1} & \targ    & \rstick{\ket{a \oplus b} = \ket{s}}\qw \\
              \lstick{\ket{0}} & \targ    & \qw      & \rstick{\ket{a \cdot b} \ \, = \ket{c}}\qw
              }

      O circuito da figura `1.9 <#cap6:fig_quantum_half_adder>`__ foi
      adaptado para implementação na plataforma IBM Quantum Experience.
      A porta Toffoli não é nativa no sistema, e deve ser realizada com
      as outras portas lógicas quânticas; uma maneira de se fazer isso é
      por meio da identidade de circuitos apresentada na proposição
      `[cap4:prop_constr_porta_toffoli] <#cap4:prop_constr_porta_toffoli>`__.

      Há uma limitação com relação ao uso das portas CNOT. As maneiras
      possíveis de se incluir uma porta CNOT no circuito são mostradas
      na figura `1.5 <#cap6:fig_cnot_gates>`__. Portanto, deve-se
      utilizar técnicas para construir outras portas CNOT em função das
      portas nativas. Algumas técnicas são apresentadas na figura
      `1.6 <#cap6:fig_cnot_tecnicas>`__. Alternativamente, pode-se
      tentar mapear os qubits de forma a conseguir realizar o circuito
      utilizando as CNOTs nativas.

      Outro detalhe é que o sistema inicializa os qubits no estado
      :math:`\ket{0}`, portanto para aplicar outras entradas é
      necessário modificar esses estados :math:`\ket{0}` com portas
      lógicas. Para inicializar com o estado :math:`\ket{1}`, por
      exemplo, deve-se aplicar uma porta :math:`X` ao :math:`\ket{0}`.

      Assim, o circuito implementado na plataforma IBM Quantum
      Experience ficou como disposto na figura
      `1.10 <#cap6:fig_quantum_half_adder_ibmqe>`__.

      .. figure:: cap6/figuras/quantum_half_adder_ibmqe.png
         :alt: 

   .. container:: subsection

      Simulação do Circuito no IBM QX

      Foram realizadas simulações do circuito
      `1.10 <#cap6:fig_quantum_half_adder_ibmqe>`__ para todas as 4
      combinações de entradas na base computacional. Para cada entrada,
      a simulação consiste na execução do circuito por 100 vezes,
      obtendo-se um gráfico de barras com a contagem dos resultados na
      base computacional. O número de disparos padrão é 100, mas outras
      configurações são possíveis.

      O resultado das simulações encontra-se na figura
      `1.11 <#cap6:fig_quantum_half_adder_simulation>`__.

      .. figure:: cap6/figuras/quantum_half_adder_simulation.png
         :alt: 

      Portanto, os resultados da simulação podem ser reescritos na
      tabela abaixo. Pode-se perceber que esses resultados correspondem
      ao disposto na tabela verdade
      `1.1 <#cap6:tab_verdade_half_adder>`__.

      .. container::
         :name: cap6:tab_simulacao_quantum_half_adder

         .. table:: Resultados da simulação do circuito quantum half
         adder.

            ========================== ==========================
            Entrada                    Saída
            :math:`\ket{\text{xx}ab0}` :math:`\ket{\text{xx}asc}`
            000                        000
            010                        010
            100                        110
            110                        101
            ========================== ==========================

   .. container:: subsection

      Execução do Circuito no IBM QX

      O circuito foi importado para o computador IBM Q 5 Tenerife
      (ibmqx4) por meio da interface IBM Quantum Experience. Foi
      realizada uma execução para cada entrada diferente, e cada
      execução consiste em 1024 disparos do circuito (esse número é
      configurável, podendo-se escolher entre as opções: 1, 1024, 4098 e
      8192). Os resultados encontram-se na figura
      `1.12 <#cap6:fig_quantum_half_adder_ibmqx4>`__.

      .. figure:: cap6/figuras/quantum_half_adder_ibmqx4.png
         :alt: 

      Percebe-se que os estados com maior número de contagens
      correspondem aos estados esperados como resposta para o circuito.
      Esses estados estão resumidos na tabela
      `1.3 <#cap6:tab_ibmqx4_quantum_half_adder>`__.

      .. container::
         :name: cap6:tab_ibmqx4_quantum_half_adder

         .. table:: Execução do circuito quantum half adder no
         computador IBM Q 5 Tenerife (ibmqx4). :math:`(\ast)` Percentual
         de disparos em que o circuito se comporta como projetado.

            +----------------------+----------------------+----------------------+
            | Entrada              | Saída                | Concordância com     |
            |                      |                      | simulação\           |
            |                      |                      |  :math:`{}^{(\ast)}` |
            +----------------------+----------------------+----------------------+
            | :math:               | :math:               |                      |
            | `\ket{\text{xx}ab0}` | `\ket{\text{xx}asc}` |                      |
            +----------------------+----------------------+----------------------+
            | 00000                | 00000                | :math:`\tfrac{40     |
            |                      |                      | 4}{1024} = 39,\!5\%` |
            +----------------------+----------------------+----------------------+
            | 00010                | 00010                | :math:`\tfrac{28     |
            |                      |                      | 5}{1024} = 27,\!8\%` |
            +----------------------+----------------------+----------------------+
            | 00100                | 00110                | :math:`\tfrac{25     |
            |                      |                      | 8}{1024} = 25,\!2\%` |
            +----------------------+----------------------+----------------------+
            | 00110                | 00101                | :math:`\tfrac{28     |
            |                      |                      | 7}{1024} = 28,\!0\%` |
            +----------------------+----------------------+----------------------+

      Apesar de o estado esperado aparecer com maior frequência, a
      quantidade de resultados espúrios impede que o algoritmo se
      comporte como projetado de maneira satisfatória. Esses resultados
      indicam que é necessário, para a tecnologia atual, utilizar
      correção de erros no projeto de algoritmos quânticos.

.. |image| image:: cap6/figuras/cnot_gates.png
.. |image1| image:: cap6/figuras/ibmqx4_tenerife_connections.png
