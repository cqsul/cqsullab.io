.. role:: raw-latex(raw)
   :format: latex
..

.. _`cap5:Alg_Q`:

Protocolos e Algoritmos Quânticos
=================================

Neste capítulo serão abordados alguns protocolos e algoritmos quânticos
conhecidos na literatura. Quando possível, é feita uma comparação com os
algoritmos clássicos conhecidos.

.. container:: section

   Codificação Superdensa

   A codificação superdensa é um protocolo que envolve duas partes,
   Alice e Bob [1]_, que queiram se comunicar trocando bits. Alice quer
   enviar bits de mensagem para Bob.

   Não é possível, classicamente, codificar 2 bits de mensagem em 1 bit
   transmitido, já que só há :math:`2^1 = 2` palavras código – as
   palavras 0 e 1 – e :math:`2^2 = 4` palavras de mensagem que podem ser
   enviadas – as palavras 00, 01, 10 e 11. No entanto, é possível
   codificar 2 bits de mensagem em 1 qubit transmitido, e é essa a
   função do *circuito de codificação superdensa*.

   Uma referência para este conteúdo é :raw-latex:`\cite{book:qcqi_nc}`,
   seção 2.3, p.97-98.

   .. container:: subsection

      Visão geral

      A codificação superdensa envolve o compartilhamento prévio de um
      par de qubits emaranhados, no estado de Bell
      :math:`\ket{\beta_{00}} = \frac{\ket{00} + \ket{11}}{\sqrt{2}}`.
      Esse estado independe da mensagem que Alice quer enviar a Bob, e
      pode ter sido distribuído por uma fonte externa de pares
      emaranhados.

      Assim, o primeiro qubit está com Alice e o segundo, com Bob. Alice
      pode realizar operações em seu qubit em função dos bits de
      mensagem que ela quer enviar. Após as operações, ela envia seu
      qubit a Bob, que passa a estar em posse dos dois qubits. Bob pode
      medí-los de maneira a obter os bits de mensagem.

   .. container:: subsection

      Circuito

      O circuito completo para a codificação superdensa é representado
      abaixo. Seu funcionamento detalhado será abordado a seguir.

      .. container:: float

         .. math::

            \Qcircuit @C=10pt @R=6pt @!R  
            { % Alice
                            &  \mbox{Alice} &     &     &               &              &      &      &     &                                & & \\
             \lstick{b_0}             &\cw  & \cw & \cw   & \control \cw         &      &      &     &                                &     & & \\
             \lstick{b_1}             & \cw & \cw & \control \cw   &   \cwx &      &      &     &                                &     & & \\
                                      & \qw & \qw & \gate{X} \cwx & \gate{Z}\cwx &  \qw & \qw  &     &                                &     & & \\
              % Bob                                                                                                           
            \lstick{\ket{\beta_{00}} \quad} &     &     &               &              &      & \qwx & \qw & \multimeasureD{1}{\mathcal{B}} & \cw & \cw & \rstick{\!\!\!\!b_0} \\
                                      &\qw  & \qw & \qw           & \qw          &  \qw & \qw  & \qw & \ghost{\mathcal{B}}    & \cw & \cw & \rstick{\!\!\!\!b_1} 
              \gategroup{2}{2}{4}{10}{1.6em}{--} \gategroup{5}{2}{6}{10}{1.5em}{--} \gategroup{4}{1}{6}{1}{1.5em}{\{}  \\
               &  \mbox{Bob}    &     &               &              &      &      &     &                                &     &  &  }

   .. container:: subsection

      Funcionamento Detalhado

      .. rubric:: Setup
         :name: setup
         :class: unnumbered

      Num primeiro momento, Alice e Bob compartilham o estado de Bell

      .. math:: \ket{\beta_{00}} = \frac{\ket{00} + \ket{11}}{\sqrt{2}} \ .

      Usamos os rótulos :math:`A` para o primeiro qubit (da Alice) e
      :math:`B` para o segundo qubit (que está com Bob).

      .. rubric:: Codificação - Alice
         :name: codificação---alice
         :class: unnumbered

      Se forem realizadas as operações que constam na tabela
      `[cap5:tab_operacoes_cod_superdensa] <#cap5:tab_operacoes_cod_superdensa>`__,
      Alice conseguirá 4 estados da base de Bell distintos em função dos
      bits de mensagem.

      .. math::

         \begin{array}{ccc}
           \text{Mensagem} & \text{Operação} & \text{Resultado} \bigstrut[b] \\ \hline 
         \bigstrut[t]   00 & I_A & \frac{\ket{00} + \ket{11}}{\sqrt{2}} = \ket{\beta_{00}} \\
           01 & Z_A & \frac{\ket{00} - \ket{11}}{\sqrt{2}} = \ket{\beta_{01}}  \\
           10 & X_A & \frac{\ket{01} + \ket{10}}{\sqrt{2}} = \ket{\beta_{10}}  \\
           11 & iY_A = Z_A X_A & \frac{\ket{01} - \ket{10}}{\sqrt{2}} = \ket{\beta_{11}} 
          \end{array}

      Observa-se que :math:`ZX = iY` pela identidade de circuitos dada
      na proposição
      `[cap4:id_para_matr_pauli] <#cap4:id_para_matr_pauli>`__. Os
      estados de Bell :math:`\ket{\beta_{00}}`,
      :math:`\ket{\beta_{01}}`, :math:`\ket{\beta_{10}}` e
      :math:`\ket{\beta_{11}}` formam uma base para o espaço de 2 qubits
      (seção `[cap3:sec_estados_bell] <#cap3:sec_estados_bell>`__).
      Observa-se também que, se a mensagem é :math:`b_0 b_1`, então
      quando :math:`b_1 = 1` aplica-se :math:`X_A` e quando
      :math:`b_0 = 1`, aplica-se :math:`Z_A`. Dessa forma, para todos os
      valores da mensagem :math:`b_0 b_1`, pode-se escrever a operação
      no qubit :math:`A` por :math:`Z_A^{\,\, b_0} X_A^{\,\, b_1}`.
      Portanto, a operação que Alice deve fazer em seu qubit pode ser
      representada pelo seguinte circuito controlado por cbits:

      .. container:: float

         .. math::

            \Qcircuit @C=10pt @R=6pt @!R 
              {
             \lstick{b_0} &\cw  & \cw & \cw            & \control \cw &      &    \\
             \lstick{b_1} & \cw & \cw & \control \cw   &   \cwx       &      &    \\
                          & \qw & \qw & \gate{X} \cwx  & \gate{Z}\cwx &  \qw &  \rstick{\!\!\!\!\text{enviar para Bob}}  }

      .. rubric:: Decodificação - Bob
         :name: decodificação---bob
         :class: unnumbered

      Alice envia, então, seu qubit a Bob, que realiza uma medida na
      base de Bell. Bob consegue distinguir em qual estado o par de
      qubits se encontra com essa medida, e, consequentemente, consegue
      saber quais bits de mensagem foram enviados: se o resultado for
      :math:`\ket{\beta_{b_0b_1}}`, então a mensagem é :math:`b_0 b_1`.

      A medição na base de Bell pode ser realizada em função da medição
      na base computacional pelo seguinte circuito:

      .. container:: float
         :name: cap5:fig_med_base_bell

         .. math::

            \phantom{\ket{\beta_{b_0b_1}}}
              \Qcircuit @C=10pt @R=10pt @!R  
               { 
               \dstick{\ket{\beta_{b_0b_1}} \quad \quad \quad \quad \quad } & \multimeasureD{1}{\mathcal{B}} & \cw & \rstick{\!\!\!\!\!\!b_0}  \\
                                                                            & \ghost{\mathcal{B}}            & \cw & \rstick{\!\!\!\!\!\!b_1}
                \gategroup{1}{1}{2}{1}{1.2em}{\{}
                 }
              \quad  \quad \begin{array}{c} \\ \\ = \end{array}\quad \quad  \phantom{\ket{\beta_{b_0b_1}}}
               \Qcircuit @C=10pt @R=6pt @!R  
               { 
               \dstick{\ket{\beta_{b_0b_1}} \quad \quad \quad \quad \quad } &  \gate{H} & \ctrl{1} & \meter & \cw & \rstick{\!\!\!\!\!\!b_0} \\
                                                                            &   \qw     &   \targ  & \meter & \cw & \rstick{\!\!\!\!\!\!b_1}
               \gategroup{1}{1}{2}{1}{1.2em}{\{} 
               }

.. container:: section

   Circuito de Teletransporte

   O circuito de teletransporte também envolve duas partes, chamadas de
   Alice e Bob, como de costume. Dessa vez, Alice está em posse de um
   qubit :math:`\ket{\psi}` cujo estado lhe é desconhecido e precisa
   enviá-lo a Bob. No entanto, o único meio de comunicação entre os dois
   é um canal clássico, por onde só é possível enviar cbits. Pode-se
   pensar em uma linha telefônica, ou uma conexão de internet entre os
   dois, por exemplo, e que os dois estão a uma grande distância um do
   outro. Enviando apenas bits clássicos, Alice deve tentar enviar o
   estado de seu qubit a Bob.

   Aparentemente não seria possível realizar essa tarefa, pois o estado
   de um qubit :math:`\ket{\psi} = a\ket{0} + b \ket{1}` é definido por
   dois números complexos :math:`a` e :math:`b`, que demandariam muitos
   bits para serem representados de maneira satisfatória (mas aproximada
   apenas). Além disso, Alice não tem conhecimento sobre o estado do seu
   qubit, e uma medida não seria suficiente para conseguir encontrar os
   coeficientes :math:`a` e :math:`b`. Seriam necessárias muitas medidas
   de cópias do sistema em bases diferentes para se conseguir obter
   estimativas das probabilidades de o resultado ser :math:`\ket{0}` ou
   :math:`\ket{1}`, e isso não é possível de se fazer quando não se tem
   cópias do sistema.

   No entanto, se Alice e Bob estiverem compartilhando um par de qubits
   emaranhados, a situação torna-se mais favorável. O par emaranhado
   :math:`\ket{\beta_{00}}` pode ter sido distribuído previamente, e não
   depende do qubit :math:`\ket{\psi}` de Alice. O *circuito de
   teletransporte* faz uso desse par emaranhado para realizar essa
   tarefa de enviar o estado de um qubit (não conhecido) fazendo-se uso
   apenas de um canal clássico. O nome “teletransporte” tornou-se
   popular para fazer referência ao circuito, mas não é muito adequado.
   Esse circuito envia apenas a informação sobre o estado, não ocorrendo
   deslocamento físico do qubit em questão.

   Uma referência para esse circuito pode ser encontrado em
   :raw-latex:`\cite{book:qcqi_nc}`, seção 1.3.7, p.26-28.

   .. container:: subsection

      Visão Geral

      Como na codificação superdensa, o circuito de teletransporte
      necessita do compartilhamento prévio de dois qubits emaranhados no
      estado de Bell :math:`\ket{\beta_{00}}`, que independe do qubit
      que Alice quer enviar a Bob.

      Além desse par compartilhado, Alice tem um qubit em um estado
      :math:`\ket{\psi}` desconhecido. Alice realiza determinadas
      operações nos seus dois qubits e realiza medidas na base
      computacional. Ao medir seus dois qubits, ela obtém informação
      clássica (cbits), e envia-as a Bob.

      Bob recebe os cbits e, em função do resultado, realiza algumas
      operações em seu qubit (o segundo qubit do par emaranhado
      compartilhado previamente). Essas operações o ajudam a recuperar o
      estado do qubit que Alice queria enviar, completando a tarefa.

      Nesse processo, o qubit :math:`\ket{\psi}` de Alice tem seu estado
      destruído pela medida, mas reaparece no qubit de Bob por causa do
      emaranhamento, restando apenas realizar uma correção em função do
      resultado da medida de Alice.

   .. container:: subsection

      Circuito

      O circuito abaixo realiza o teletransporte do estado de 1 qubit de
      Alice para Bob utilizando apenas o envio de 2 cbits. O
      funcionamento detalhado do Circuito de Teletransporte será visto
      adiante.

      .. container:: float

         .. math::

            \Qcircuit @C=12pt @R=10pt @!R  
            { % Alice
                                           &                                                & \mbox{Alice} \gategroup{2}{3}{3}{10}{3em}{--}     &         &          &        &                  &                      &                      &     &   &          \\
             \lstick{\ket{\psi}}           & \qw                                            & \qw                                               &\ctrl{1} & \gate{H} & \meter & \ustick{b_0} \cw & \cw                  & \control \cw \cwx[3] &     &   &          \\
                                           & \qw                                            &                                               \qw &\targ    & \qw      & \meter & \ustick{b_1} \cw & \control \cw \cwx[2] &                      &     &   &          \\
             \lstick{\ket{\beta_{00}}\quad}&                                                &                                                   &         &          &        &                  &                      &                      &     &   &          \\
              % Bob                                                                                                                          
                                           & \qw     \gategroup{3}{1}{5}{1}{1.5em}{\{}      &\qw                                                &\qw      & \qw      & \qw    & \qw              & \gate{X}             & \gate{Z}             & \qw &\qw& \rstick{\ket{\psi}} \qw  \\
                                           &                                                &  \mbox{Bob}  \gategroup{5}{3}{5}{10}{3em}{--}     &         &          &        &                  &                      &                      &     &   &   
             %           1                 %            2                                   % 3                                                 %   4     %   5      % 6      % 7                %  8                   %  9                   % 10  % 11                                 
                       }

   .. container:: subsection

      Funcionamento Detalhado

      .. rubric:: Setup
         :name: setup-1
         :class: unnumbered

      Alice e Bob previamente compartilham o estado de Bell

      .. math:: \ket{\beta_{00}} = \frac{\ket{00} + \ket{11}}{\sqrt{2}} \ .

      Alice também possui um qubit
      :math:`\ket{\psi} = a\ket{0} + b\ket{1}` em um estado não
      necessariamente conhecido por ela. Sua intenção é que Bob tenha
      uma “cópia” do estado desse qubit. Usando-se os rótulos
      :math:`A_1` e :math:`A_2` para os qubits de Alice e :math:`B` para
      o de Bob, o estado do sistema completo pode ser escrito como

      .. math::

         \begin{split}
                \ket{\psi_0} 
                &= \ket{\psi}_{A_1} \ket{\beta_{00}}_{A_2 B} \\
                &=  \big( a \ket{0}_{A_1} + b \ket{1}_{A_1} \big) \frac{1}{\sqrt{2}}\big(\ket{0}_{A_2}\ket{0}_B +  \ket{1}_{A_2}\ket{1}_B \big) \\
                &=  \tfrac{a}{\sqrt{2}} \ket{0}_{A_1} \ket{0}_{A_2}\ket{0}_B  + \tfrac{a}{\sqrt{2}} \ket{0}_{A_1} \ket{1}_{A_2}\ket{1}_B  \\
                &\phantom{=} \quad + \tfrac{b}{\sqrt{2}} \ket{1}_{A_1} \ket{0}_{A_2}\ket{0}_B  + \tfrac{b}{\sqrt{2}} \ket{1}_{A_1} \ket{1}_{A_2}\ket{1}_B \\
                &=  \left(  \tfrac{a}{\sqrt{2}} \ket{0}_{A_1} \ket{0}_{A_2} + \tfrac{b}{\sqrt{2}} \ket{1}_{A_1} \ket{0}_{A_2}\right) \ket{0}_B   \\
                &\phantom{=} \quad  + \left( \tfrac{a}{\sqrt{2}} \ket{0}_{A_1} \ket{1}_{A_2} + \tfrac{b}{\sqrt{2}} \ket{1}_{A_1} \ket{1}_{A_2} \right)\ket{1}_B    \ . 
               \end{split}

      .. rubric:: Preparação - Alice
         :name: preparação---alice
         :class: unnumbered

      Alice realiza as operações CNOT nos dois qubits e Hadamard em
      :math:`A_1`, e o sistema completo passa a ficar no estado:

      .. math::

         \begin{split}
             \ket{\psi_1} 
             &= \text{CNOT}_{A_1,A_2} \ket{\psi_0} \\
             &= \text{CNOT}_{A_1,A_2} \bigg[ \left(  \tfrac{a}{\sqrt{2}} \ket{0}_{A_1} \ket{0}_{A_2} + \tfrac{b}{\sqrt{2}} \ket{1}_{A_1} \ket{0}_{A_2}\right) \ket{0}_B   \\
              &\phantom{=} \quad  + \left( \tfrac{a}{\sqrt{2}} \ket{0}_{A_1} \ket{1}_{A_2} + \tfrac{b}{\sqrt{2}} \ket{1}_{A_1} \ket{1}_{A_2} \right)\ket{1}_B \bigg]\\
            &=\left( \tfrac{a}{\sqrt{2}} \text{CNOT}_{A_1,A_2} \ket{0}_{A_1} \ket{0}_{A_2} + \tfrac{b}{\sqrt{2}}  \text{CNOT}_{A_1,A_2} \ket{1}_{A_1} \ket{0}_{A_2}\right) \ket{0}_B   \\
                &\phantom{=} \quad  + \left( \tfrac{a}{\sqrt{2}} \text{CNOT}_{A_1,A_2}\ket{0}_{A_1} \ket{1}_{A_2} + \tfrac{b}{\sqrt{2}} \text{CNOT}_{A_1,A_2} \ket{1}_{A_1} \ket{1}_{A_2} \right)\ket{1}_B \\
            &= \left( \tfrac{a}{\sqrt{2}} \ket{0}_{A_1} \ket{0}_{A_2} + \tfrac{b}{\sqrt{2}}  \ket{1}_{A_1} \ket{1}_{A_2}\right) \ket{0}_B   \\
                &\phantom{=} \quad  + \left( \tfrac{a}{\sqrt{2}} \ket{0}_{A_1} \ket{1}_{A_2} + \tfrac{b}{\sqrt{2}} \ket{1}_{A_1} \ket{0}_{A_2} \right)\ket{1}_B \\
            \end{split}

      .. math::

         \begin{split}
             \ket{\psi_2} 
             &= H_{A_1} \ket{\psi_1} \\
             &= H_{A_1} \left( \tfrac{a}{\sqrt{2}} \ket{0}_{A_1} \ket{0}_{A_2} + \tfrac{b}{\sqrt{2}}  \ket{1}_{A_1} \ket{1}_{A_2}\right) \ket{0}_B   \\
                &\phantom{=} \quad  + H_{A_1} \left( \tfrac{a}{\sqrt{2}} \ket{0}_{A_1} \ket{1}_{A_2} + \tfrac{b}{\sqrt{2}} \ket{1}_{A_1} \ket{0}_{A_2} \right)\ket{1}_B \\
             &=  \left( \tfrac{a}{\sqrt{2}} H_{A_1}\ket{0}_{A_1} \ket{0}_{A_2} + \tfrac{b}{\sqrt{2}}  H_{A_1}\ket{1}_{A_1} \ket{1}_{A_2}\right) \ket{0}_B   \\
                &\phantom{=} \quad  + \left( \tfrac{a}{\sqrt{2}} H_{A_1}\ket{0}_{A_1} \ket{1}_{A_2} + \tfrac{b}{\sqrt{2}} H_{A_1} \ket{1}_{A_1} \ket{0}_{A_2} \right)\ket{1}_B \\
             &=  \left( \tfrac{a}{2} \big(\ket{0}_{A_1} + \ket{1}_{A_1}\big) \ket{0}_{A_2} + \tfrac{b}{2} \big(\ket{0}_{A_1} - \ket{1}_{A_1}\big) \ket{1}_{A_2}\right) \ket{0}_B   \\
                &\phantom{=} \quad  + \left( \tfrac{a}{2} \big(\ket{0}_{A_1} + \ket{1}_{A_1}\big)\ket{1}_{A_2} + \tfrac{b}{2} \big(\ket{0}_{A_1} - \ket{1}_{A_1}\big) \ket{0}_{A_2} \right)\ket{1}_B \\   
             &= \ket{00}_{A_1A_2}\left( \tfrac{a}{2} \ket{0}_B + \tfrac{b}{2} \ket{1}_B \right) + \ket{01}_{A_1A_2}\left( \tfrac{b}{2} \ket{0}_B + \tfrac{a}{2} \ket{1}_B \right) \\
                &\phantom{=} \quad  + \ket{10}_{A_1A_2}\left( \tfrac{a}{2} \ket{0}_B - \tfrac{b}{2} \ket{1}_B \right)  + \ket{11}_{A_1A_2}\left(- \tfrac{b}{2} \ket{0}_B + \tfrac{a}{2} \ket{1}_B   \right) 
             \end{split}

      Alice realiza, então, a medida dos seus dois qubits na base
      computacional. Essa medida faz com que o sistema total encontre-se
      no estado

      .. math:: \ket{\psi_3} = \ket{b_0 b_1}_{A_1A_2} \ket{\psi_3}_B \ .

      em que o resultado da medida é :math:`b_0 b_1`. O qubit que está
      com Bob passa a ficar no estado :math:`\ket{\psi_3}_B`, que
      depende do valor da medida. As opções possíveis são listadas na
      tabela a seguir.

      .. math::

         \begin{array}{cc}
              \bigstrut \text{Resultado da medida} & \text{Estado do qubit $B$} \\ \hline
               00 & \phantom{-}a \ket{0} + b \ket{1} \bigstrut[t] \\
               01 & \phantom{-}b \ket{0} + a \ket{1} \\
               10 & \phantom{-}a \ket{0} - b \ket{1} \\
               11 & -b \ket{0} + a \ket{1} 
               \end{array}

      O cálculo da primeira linha da tabela
      `[cap5:tab_result_med_circ_teletr] <#cap5:tab_result_med_circ_teletr>`__
      é exemplificado a seguir. Caso o resultado da medida tenha sido
      :math:`00`, o estado do sistema total :math:`\ket{\psi_3}` é
      obtido pela projeção :math:`\ketbra{00}{00}_{A_1 A_2}` seguida de uma
      normalização do vetor resultante. É conveniente lembrar que
      :math:`\abs{a}^2 + \abs{b}^2 = 1` pela normalização do estado
      :math:`\ket{\psi} = a\ket{0} + b\ket{1}` do início do algoritmo.

      .. math:: \ketbra{00}{00}_{A_1 A_2} \ket{\psi_2} = \ket{00}_{A_1A_2}\left( \tfrac{a}{2} \ket{0}_B + \tfrac{b}{2} \ket{1}_B \right)

      .. math::

         \begin{split}
               \ket{\psi_2} 
               &= \frac{\ket{00}_{A_1A_2}\left( \tfrac{a}{2} \ket{0}_B + \tfrac{b}{2} \ket{1}_B \right)}{ \sqrt{\abs{\tfrac{a}{2}}^2 + \abs{\tfrac{b}{2}}^2 } }  \\
               &= \frac{\ket{00}_{A_1A_2}\left( \tfrac{a}{2} \ket{0}_B + \tfrac{b}{2} \ket{1}_B \right)}{ \tfrac{1}{2} \sqrt{\abs{a}^2 + \abs{b}^2 } }  \\
               &= \ket{00}_{A_1A_2}\left( a \ket{0}_B + b \ket{1}_B \right) \ .
             \end{split}

      Os outros estados da tabela são obtidos com contas similares.

      Agora, Alice pode enviar o resultado da medida para Bob e o qubit
      em posse dele ficará no estado correspondente na tabela
      `[cap5:tab_result_med_circ_teletr] <#cap5:tab_result_med_circ_teletr>`__.

      .. container:: float

         .. math::

            \Qcircuit @C=12pt @R=10pt @!R  
            { % Alice
              \lstick{\ket{\psi}}          & \qw                                            & \qw                                               &\ctrl{1} & \gate{H} & \meter & \cw &  \rstick{b_0}  \cw         \\
                                           & \qw                                            &                                               \qw &\targ    & \qw      & \meter & \cw &  \rstick{b_1}  \cw         \\
                                           &                                                &                                                   &         &          &        &                  &                 }
              \phantom{b_0} \quad \ \ 
              \begin{array}{c}
               \\ \\ \text{enviar para Bob}
              \end{array}

      .. rubric:: Processamento final - Bob
         :name: processamento-final---bob
         :class: unnumbered

      Nesse ponto, Bob já tem conhecimento do resultado da medida de
      Alice e o estado do seu qubit corresponde à entrada correspondente
      na tabela
      `[cap5:tab_result_med_circ_teletr] <#cap5:tab_result_med_circ_teletr>`__.
      Para recuperar o estado :math:`a\ket{0} + b\ket{1}`, Bob deve
      fazer algumas operações para corrigir o estado do seu qubit, em
      função do valor da medida informado a ele.

      Se a medida for :math:`00`, seu qubit está em
      :math:`a\ket{0} + b\ket{1}` e nada precisa ser feito. Se a medida
      resultou em :math:`01`, seu qubit está em
      :math:`b\ket{0} + a\ket{1}`; nesse caso, é possível perceber que a
      porta :math:`X` fornece novamente o estado desejado
      :math:`b\ket{1} + a\ket{0}`. Considerando-se todos os resultados
      possíveis da medida, monta-se a correção necessária para cada
      caso, conforme disposto na tabela
      `[cap5:tab_operacoes_circ_teletransporte] <#cap5:tab_operacoes_circ_teletransporte>`__.

      .. math::

         \begin{array}{cccc}
             \bigstrut   \text{Medida} & \text{Estado do qubit $B$} & \text{Aplicar operações} & \text{Estado final} \\ \hline
               00 & \phantom{-}a \ket{0} + b \ket{1} &  I_B  &  a \ket{0} + b \ket{1} = \ket{\psi} \bigstrut[t] \\
               01 & \phantom{-}b \ket{0} + a \ket{1} & X_B   &  a \ket{0} + b \ket{1} = \ket{\psi}\\
               10 & \phantom{-}a \ket{0} - b \ket{1} & Z_B   &  a \ket{0} + b \ket{1} = \ket{\psi}\\
               11 & -b \ket{0} + a \ket{1}           & Z_B X_B &  a \ket{0} + b \ket{1} = \ket{\psi}     
               \end{array}

      Essas operações, como na codificação superdensa, podem ser
      resumidas na operação controlada classicamente
      :math:`Z_B^{\, b_0} X_B^{\, b_1}`, em que :math:`b_0b_1` é o
      resultado da medida. Dessa forma, pode-se representar o
      processamento de Bob pelo circuito a seguir.

      .. container:: float

         .. math::

            \Qcircuit @C=12pt @R=4pt @!R  
            {
             \lstick{b_0}     & \cw                  & \control \cw \cwx[2] &     &   &          \\
             \lstick{b_1}     & \control \cw \cwx[1] &                      &     &   &          \\
                              & \gate{X}             & \gate{Z}             & \qw &\qw& \rstick{\ket{\psi}} \qw  
                       }

      Com isso, independente do resultado da medida, Bob tem, ao final,
      o estado :math:`\ket{\psi}` que Alice queria transmitir.

.. container:: section

   Oráculos Quânticos

   Os oráculos são funções booleanas
   :math:`f \colon \{ 0,1 \}^n \to \{ 0,1 \}` consideradas como “caixas
   pretas”. Dado um vetor de bits :math:`x`, o oráculo clássico fornece
   :math:`f(x)`. Alguns problemas computacionais são escritos em termos
   de oráculos, como o problema de Deutsch-Jozsa, o problema de Simon e
   o problema de busca de Grover. Para abordar esses problemas, é
   necessário definir uma versão quântica desse oráculo, o que será
   abordado nesta seção.

   Há duas maneiras de se escrever um análogo quântico ao oráculo
   clássico: o *oráculo XOR* e o *oráculo de fase*.

   .. container:: subsection

      Oráculo XOR

      O *oráculo XOR* é uma operação unitária que realiza a função
      booleana :math:`f` por meio de um bit extra, que sinaliza as
      entradas em que :math:`f` vale 1.

      .. container:: float

         .. math::

            \Qcircuit @C=5pt @R=10pt @!R 
                {
               \lstick{\ket{x}} &  \ustick{\ n}\qw  & {/} \qw & \qw &\ctrl{1} & \qw & \qw &  \rstick{\ket{x}}                  \qw     \\
               \lstick{\ket{b}} & \qw               &\qw      & \qw & \gate{O}& \qw & \qw & \rstick{\ket{b\oplus f(x)}} \qw  
                }

      .. container:: float

         .. math::

            \Qcircuit @C=5pt @R=5pt %@!R 
                {
                                           &  \qw \gategroup{1}{1}{4}{1}{0.6em}{\{}  &   \qw & \ctrl{2} & \qw & \qw& \qw  \gategroup{1}{7}{4}{7}{0.6em}{\}}   \\
                  \dstick{\ket{x} \quad \quad \quad }                          &   \qw                               &   \qw & \ctrl{2} &\qw    & \qw &  \dstick{ \quad \quad \quad \ket{x}}\qw       \\
                       &  \push{\vdots}                                 &                  &          &  \push{\vdots}  &   \\    
                                           &   \qw                                   &   \qw & \ctrl{1} & \qw & \qw&  \qw     \\
               \lstick{\ket{0}}            & \qw                                     & \qw   & \gate{O} & \qw & \qw& \rstick{\ket{f(x)}} \qw  
                }

      O oráculo XOR pode ser generalizado para funções
      :math:`f \colon  \{ 0,1 \}^n \to  \{ 0,1 \}^n` ou, ainda, para
      funções :math:`f \colon  \{ 0,1 \}^n \to  \{ 0,1 \}^m`.

      .. container:: float

         .. math::

            \Qcircuit @C=5pt @R=10pt @!R 
                {
               \lstick{\ket{x}} &  \ustick{\ n}\qw  & {/} \qw & \qw &\ctrl{1} & \qw & \qw &  \rstick{\ket{x}}                  \qw     \\
               \lstick{\ket{y}} &  \ustick{\ m}\qw  & {/} \qw & \qw & \gate{O}& \qw & \qw & \rstick{\ket{y \oplus f(x)}} \qw  
                }

   .. container:: subsection

      Oráculo de Fase

      O *oráculo de fase* é uma operação unitária que sinaliza as
      entradas em que :math:`f` vale 1 introduzindo uma fase de
      :math:`\pi`, isto é, uma multiplicação por :math:`-1`.

      .. container:: float

         .. math::

            \Qcircuit @C=5pt @R=10pt @!R 
                {
               \lstick{\ket{x}} &  \ustick{\ n}\qw  & {/} \qw & \qw & \gate{O} & \qw &  \rstick{(-1)^{f(x)}\ket{x}}   \qw     \\
                }

   .. container:: subsection

      Construção do Oráculo de Fase usando o
      Oráculo XOR Pode-se obter o oráculo de fase a partir do oráculo
      XOR com o uso do qubit alvo como um qubit auxiliar. Ao usarmos
      :math:`\ket{-}` na entrada alvo do oráculo XOR, obtemos, para
      qualquer estado :math:`\ket{x}` da base computacional:

      .. math::

         \ket{x}\ket{-} = \ket{x}\tfrac{\ket{0}-\ket{1}}{\sqrt{2}} \xrightarrow{O_{\text{XOR}}} 
           \begin{cases}
            \ket{x}\frac{\ket{0}-\ket{1}}{\sqrt{2}} = \ket{x}\ket{-}    &\text{se $f(x)=0$} \vspace{5pt} \\
            \ket{x}\frac{\ket{1}-\ket{0}}{\sqrt{2}} = \ket{x}(-\ket{-}) &\text{se $f(x)=1$} \ ,
           \end{cases}

      o que pode ser resumido por
      :math:`\ket{x}\big((-1)^{f(x)}\ket{-} \big)`. Além disso, o fator
      multiplicativo :math:`(-1)^{f(x)}` pode ser movido para qualquer
      entrada tensorial por multilinearidade do produto tensorial:

      .. math:: \ket{x}\otimes(-1)^{f(x)}\ket{-} =  (-1)^{f(x)} \ket{x}\otimes\ket{-} \ .

      A figura a seguir ilustra a construção do oráculo de fase.

      .. container:: float

         .. math::

            \Qcircuit @C=5pt @R=10pt @!R 
                {
               \lstick{\ket{x}} &  \ustick{\ n}\qw  & {/} \qw & \qw &\ctrl{1}              & \qw & \qw &  \push{\ket{x}}            \qw  & \dstick{=} & & \rstick{(-1)^{f(x)}\ket{x}}  \\
               \lstick{\ket{-}} & \qw               &\qw      & \qw & \gate{O_{\text{XOR}}}& \qw & \qw &  \push{(-1)^{f(x)} \ket{-}} \qw &            & & \rstick{\ket{-}} 
                }

.. container:: section

   Algoritmo de Deutsch-Jozsa

   O Algoritmo de Deutsch-Jozsa é um algoritmo quântico projetado para
   resolver o Problema de Deutsch-Jozsa. Esse problema não tem especial
   ênfase em aplicações, mas torna-se laboratório interessante para
   investigar técnicas e possíveis vantagens da Computação Quântica. As
   principais referências para esta seção são o livro
   :raw-latex:`\cite{book:qcqi_nc}`, seção 1.4.4, p.34-36, e as
   videoaulas U2.2, subunidade SU2 de
   :raw-latex:`\cite{videolecture:qisI_p2}`.

   .. container:: subsection

      Problema de Deutsch-Jozsa

      Antes de enunciar o problema de Deutsch-Jozsa é conveniente
      escrever algumas definições.

      .. container:: definition

          

         A função booleana :math:`f \colon \{ 0,1 \}^n \to \{0,1\}` é
         dita *constante* se :math:`f` assume o mesmo valor em todas as
         entradas:

         .. math::

            \begin{array}{ll}
                   f(x) = 0 \ , \ \ \forall x \in  \{ 0,1 \}^n &  \text{ ou} \\
                    f(x) = 1 \ , \ \ \forall x \in  \{ 0,1 \}^n &  \ .
                  \end{array}

         A função :math:`f` é dita *balanceada* se admite o valor
         :math:`0` em metade das suas entradas e admite :math:`1` na
         metade complementar das entradas.

      .. container:: example

         A função booleana :math:`f(x) = 1` é constante.

      .. container:: example

         Denote :math:`x \in \{ 0,1 \}^n` por
         :math:`x = x_{n-1} \ldots x_1 x_0`. A função booleana
         :math:`f(x) = x_0` é balanceada, pois para exatamente metade
         das entradas :math:`x` tem-se :math:`x_0 = 0` e para a outra
         metade, tem-se :math:`x_0 = 1`.

      .. container:: example

         Considere a função booleana com entradas de :math:`n=2` bits
         dada por :math:`f(a,b) = a\cdot b`, em que, lembrando,
         :math:`\cdot` representa a porta AND. A tabela verdade dessa
         função é representada abaixo.

         .. math::

            \begin{array}{cc|c}
                a & b & f(a,b) = a \cdot b \\ \hline
                0 & 0 & 0 \\
                0 & 1 & 0 \\
                1 & 0 & 0 \\
                1 & 1 & 1 
               \end{array}

         Essa função não é balanceada nem constante.

      O problema desta seção tem o seguinte enunciado.

      .. container:: problem*

         de Deutsch-Jozsa Seja uma função booleana
         :math:`f \colon \{ 0,1 \}^n \to \{0,1\}` que pode ser apenas ou
         constante ou balanceada. Decidir se :math:`f` é constante ou
         balanceada.

      Deseja-se, dada uma função :math:`f` considerada como caixa preta,
      e com o compromisso de ser ou constante ou balanceada, decidir
      qual dos dois casos mutuamente excludentes é verdadeiro.

   .. container:: subsection

      Algoritmo de Deutsch-Jozsa Para resolver o problema de
      Deutsch-Jozsa com um algoritmo quântico, é necessário ter uma
      versão quântica da função booleana :math:`f`, dada como oráculo,
      isto é, dada como uma caixa preta em que não se pode visualizar a
      subrotina que calcula :math:`f`.

      Considere que :math:`f` seja dada por meio do oráculo de fase
      (como na seção
      `[cap5:sec_oraculo_fase] <#cap5:sec_oraculo_fase>`__). O algoritmo
      de Deutsch-Jozsa para decidir se :math:`f` é constante ou
      balanceada é dado pelo procedimento abaixo.

      .. rubric:: Algoritmo de Deutsch-Jozsa
         :name: algoritmo-de-deutsch-jozsa
         :class: unnumbered

      | **Entrada:** :math:`O_\text{F}(f) = O`   (oráculo de fase
        associado à função booleana :math:`f`)
      | **Procedimento:**
      | :math:`\begin{array}{lll}
           \text{etapa 0:} & \ket{0}^{\otimes n} & \text{\small{preparação do estado inicial}} \\
           \text{etapa 1:} & \ket{+}^{\otimes n} & \text{\small{superposição de estados com $H^{\otimes n}$}} \\
           \text{etapa 2:} & O_\text{F}\ket{+}^{\otimes n} & \text{\small{aplicação de $f$ (oráculo de fase)}} \\
           \text{etapa 3:} & \bra{+}^{\otimes n}O_\text{F}\ket{+}^{\otimes n} & \text{\small{testar para $\ket{+}^{\otimes n}$ (base girada $\mathcal{X}$)}} \\
          \end{array}`
      | **Saída:** Probabilidade da medida de :math:`\ket{\psi_2}`
        resultar em :math:`\ket{+}^{\otimes n}` é

        .. math::

           P = 
              \begin{cases}
              1 & \text{se $f$ é constante} \\
              0 & \text{se $f$ é balanceada}
              \end{cases}

        Portanto, se o estado após a medida na base :math:`\mathcal{X}`
        for :math:`\ket{+}^{\otimes n}`, então decide-se que :math:`f` é
        constante. E se o estado após a medida for qualquer outro,
        decide-se que :math:`f` é balanceada.

      .. rubric:: Circuito
         :name: circuito
         :class: unnumbered

      .. container:: float
         :name: cap5:circuito_deutsch_jozsa

         | Notação compacta:

           .. math::

              \Qcircuit @C=5pt @R=10pt @!R 
                  {
                 \lstick{\ket{0}^{\otimes n}} &  \ustick{\ n}\qw  & {/} \qw & \qw & \gate{H^{\otimes n}} & \gate{O_\text{F}(f)} & \gate{H^{\otimes n}} & \qw & \qw &\meter}
         | Notação expandida:

           .. math::

              \Qcircuit @C=5pt @R=10pt @!R 
                  {
                 \lstick{\ket{0}} & \qw & \qw & \qw & \gate{H} & \multigate{3}{O_\text{F}(f)} & \qw & \gate{H} & \qw & \qw &\meter \\
                 \lstick{\ket{0}} & \qw & \qw & \qw & \gate{H} & \ghost{O_\text{F}(f)}        & \qw &  \gate{H} & \qw & \qw &\meter \\
                 \lstick{\vdots}  &     &     &     & \vdots   & \ghostnoqw{O_\text{F}(f)}    &     &  \vdots   &     &     & \vdots  \\
                 \lstick{\ket{0}} & \qw & \qw & \qw & \gate{H} & \ghost{O_\text{F}(f)}        & \qw &  \gate{H} & \qw & \qw &\meter \gategroup{1}{8}{4}{11}{12pt}{--} \\
                                  &     &     &     &          &                              & & & & \mbox{\footnotesize Medição na base girada $\mathcal{X}$} 
                 }

      .. container:: remark

         A porção destacada na figura
         `1.2 <#cap5:circuito_deutsch_jozsa>`__ corresponde à medição na
         base :math:`\mathcal{X}` feita a partir da medição na base
         computacional. De fato, o operador de Hadamard realiza mudança
         de base de :math:`\mathcal{X}` (base girada) para
         :math:`\mathcal{I}` (base computacional), conforme exemplo
         `[cap2:ex_matriz_hadamard_mudança_base_x] <#cap2:ex_matriz_hadamard_mudança_base_x>`__,
         de forma que o resultado medido na base computacional
         corresponde a uma medição na base :math:`\mathcal{X}`. A figura
         `1.3 <#cap5:fig_med_base_girada_x>`__ ilustra a medição na base
         girada feita em função da medição na base computacional.

         .. container:: float
            :name: cap5:fig_med_base_girada_x

            .. math::

               \Qcircuit @C=8pt @R=10pt @!R 
                   {
                   & \ustick{\ket{+}} \qw & \qw & \gate{H} & \ustick{\ \ket{0}} \qw & \qw & \meter 
                  } 
                  \qquad \qquad
                   \Qcircuit @C=8pt @R=10pt @!R 
                   {
                   & \ustick{\ket{-}} \qw & \qw & \gate{H} & \ustick{\ \ket{1}} \qw & \qw & \meter 
                  }

      .. rubric:: Análise detalhada do algoritmo
         :name: análise-detalhada-do-algoritmo
         :class: unnumbered

      Na etapa 1, aplica-se :math:`H` para cada qubit de entrada,
      resultando em:

      .. math::

         \begin{split}
             \ket{\psi_1} 
             &= H^{\otimes n} \ket{0}^{\otimes n} \\
             &= \ket{+}^{\otimes n} \\
             &= \left(\frac{\ket{0} + \ket{1}}{\sqrt{2}} \right) \ldots \left(\frac{\ket{0} + \ket{1}}{\sqrt{2}} \right) \\
             &= \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}_n} \ket{x} \ ,
            \end{split}

      em que :math:`\mathbb{B}_n` representa o conjunto de todas as
      palavras de :math:`n` bits. Isto é,

      .. math::

         \begin{split}
                \mathbb{B}_n 
                &= \{ 0\ldots 00 \, , \, \, 0 \ldots 01 \, , \, \, 0\ldots 10 \, , \, \, 0 \ldots 11 \, , \, \, \ldots \, , \, \, 1 \ldots 11 \}  \\
                &= \{ 0, 1, 2, 3, \ldots, 2^n-1 \}  \ .
              \end{split}

      .. container:: remark

         Por vezes é útil fazer a identificação entre vetores de bits e
         números inteiros sem sinal, para simplificar a notação. Por
         exemplo, :math:`0 = 0\ldots000`, :math:`1 = 0\ldots001`,
         :math:`2 = 0\ldots010`, :math:`3 = 0\ldots011` e assim por
         diante, até :math:`2^n-1 = 1 \ldots 111`. Essa identificação
         consta na tabela
         `[cap1:tab_vetor_bits_representacao_nros_inteiros] <#cap1:tab_vetor_bits_representacao_nros_inteiros>`__
         do apêndice `[cap1_comp_classica] <#cap1_comp_classica>`__.

      A aplicação do oráculo na etapa 2 fornece:

      .. math::

         \begin{split}
             \ket{\psi_2} 
             &= O_\text{F} \ket{\psi_1}  \\
             &= \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}_n} O \ket{x} \\
             &= \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}_n} (-1)^{f(x)} \ket{x} \ .
             \end{split}

      Se a função for constante, o fator :math:`(-1)^{f(x)}` se tornará
      um sinal global :math:`+` ou :math:`-`, que essencialmente não
      altera o estado anterior.

      A última etapa consiste na medição na base girada
      :math:`\mathcal{X}`. Para realizar essa medida, pode-se aplicar
      :math:`H` a todos os qubits e medir na base computacional, como
      ilustrado na figura `1.2 <#cap5:circuito_deutsch_jozsa>`__.
      Calculando a probabilidade de se obter
      :math:`\ket{+}^{\otimes n}`, consegue-se:

      .. math::

         \begin{split}
             \bra{+}^{\otimes n} \ket{\psi_2}
             &= \left( \frac{1}{\sqrt{2^n}} \sum_{y \in \mathbb{B}_n} \bra{y} \right) \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}_n} (-1)^{f(x)} \ket{x} \\
             &= \frac{1}{2^n} \sum_{x \in \mathbb{B}_n} \sum_{y \in \mathbb{B}_n} (-1)^{f(x)}  \bra{y}\ket{x} \\
             &= \frac{1}{2^n} \sum_{x \in \mathbb{B}_n} \sum_{y \in \mathbb{B}_n} (-1)^{f(x)}  \delta_{x,y} \\
             &= \frac{1}{2^n} \sum_{x \in \mathbb{B}_n}  (-1)^{f(x)} \ .
             \end{split}

      Caso a função seja constante, a última equação fornece

      .. math:: \frac{1}{2^n} \sum_{x \in \mathbb{B}_n}  (-1)^{f(x)} =   \pm \frac{1}{2^n} 2^n = \pm 1 \ .

      E caso a função seja balanceada, metade das parcelas contribui com
      :math:`1` e a outra metade com :math:`-1`, portanto

      .. math:: \frac{1}{2^n} \sum_{x \in \mathbb{B}_n}  (-1)^{f(x)} =    \frac{1}{2^n} 0 = 0 \ .

      A probabilidade :math:`P` de se obter :math:`\ket{+}^{\otimes n}`
      é dada pelo módulo ao quadrado do resultado obtido, logo

      .. math::

         P = \abs{ \bra{+}^{\otimes n} \ket{\psi_2} }^2 = 
              \begin{cases}
            1 & \text{se $f$ é constante} \\
            0 & \text{se $f$ é balanceada} \ .
            \end{cases}

      Dessa forma, decide-se por “:math:`f` é constante” se a medida
      resultar no estado :math:`\ket{+}^{\otimes n}` e por “:math:`f` é
      balanceada”, se resultar em um estado diferente. Esse teste é
      realizado no algoritmo por uma mudança de base, realizada pela
      porta Hadamard, e uma medição na base computacional, como
      ilustrado nas figuras `1.2 <#cap5:circuito_deutsch_jozsa>`__ e
      `1.3 <#cap5:fig_med_base_girada_x>`__.

   .. container:: subsection

      Algoritmo Clássico

      Agora considere o problema de Deutsch-Jozsa no contexto clássico.
      Tem-se :math:`f` dada como uma caixa preta e se quer decidir se
      :math:`f` é constante ou balanceada. A seguir serão vistas
      brevemente as abordagens clássicas determinística e aleatória para
      o problema.

      .. rubric:: Algoritmo Clássico Determinístico
         :name: algoritmo-clássico-determinístico
         :class: unnumbered

      A Computação Clássica Determinística é um tipo de computação em
      que se busca algoritmos que não façam uso de recursos
      probabilísticos para resolver um problema. Os algoritmos
      determinísticos são tais que, ao serem executados diversas vezes
      para uma mesma entrada, produz-se sempre a mesma saída. Para que
      se resolva o problema nesse tipo de computação, é necessário
      realizar aplicações sucessivas de :math:`f` para diversas entradas
      até se ter certeza de qual opção é válida (se :math:`f` é
      constante ou balanceada). Por exemplo [2]_, calcula-se
      :math:`f(0)`, :math:`f(1)`, :math:`f(2)`, :math:`\ldots` e se
      verifica se :math:`f(1) = f(0)`, :math:`f(2) = f(1)`,
      :math:`\ldots` ou não. Caso ocorra :math:`f(j) \neq f(i)`, então a
      opção certa é “:math:`f` é balanceada”, e caso isso não ocorra, a
      opção correta é “:math:`f` é constante”.

      Para se distinguir com certeza as duas opções, deve-se aplicar
      :math:`f` a metade das entradas possíveis mais uma, ou seja, a
      :math:`2^n/2 + 1` entradas. Isso porque, na pior das hipóteses, a
      função era balanceada e, obteve-se um mesmo resultado, por azar,
      para as :math:`2^n/2` entradas testadas, impedindo que se faça a
      escolha com certeza.

      Dessa forma, o custo computacional desse algoritmo é de
      :math:`2^n/2 + 1` aplicações de :math:`f`.

      .. rubric:: Algoritmo Clássico Probabilístico
         :name: algoritmo-clássico-probabilístico
         :class: unnumbered

      Um algoritmo probabilístico utiliza a probabilidade como recurso
      computacional. Para esse tipo de computação, é possível que
      entradas iguais produzam saídas diferentes, e que a máquina passe
      por estados diferentes durante a computação, em função de fatores
      probabilísticos presentes no algoritmo. Nesse contexto, se for
      permitida uma probabilidade de erro :math:`\varepsilon` na decisão
      e o uso de sorteios aleatórios em certas etapas, é possível
      reduzir o custo computacional do algoritmo clássico
      determinístico.

      Primeiramente, permite-se que as entradas :math:`i` sejam tiradas
      aleatoriamente, cada uma com mesma probabilidade
      :math:`p(i) = 1/2^n`. Por exemplo, se :math:`f` for constante
      :math:`1` (:math:`f(i) = 1 \forall j`), a probabilidade de
      resultar :math:`1` é :math:`1 = 100\%` e a de resultar :math:`0` é
      :math:`0 = 0\%`. Se :math:`f` for balanceada, a probabilidade de
      resultar :math:`1` é :math:`0,\!5 = 50\%` e o mesmo vale para o
      resultado :math:`0`. Supõe-se, para simplificar a discussão, que o
      sorteio das entradas é feito sem memória [3]_, isto é, com chance
      de se sortear duas entradas iguais.

      .. container:: float
         :name: cap5:fig_probabilidade_resultado_f_dj

         .. math::

            \begin{array}{cccc}
                   \bigstrut \text{$f$ constante} & & & \text{$f$ balanceada}  \vspace{8 pt} \\ 
                   \begin{array}{lc} 
                   f=0\colon & \begin{array}{c} \text{\Large $\circ$} \ \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} 
                                             \\ \text{\Large $\circ$} \ \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} \end{array}\\
                                     & \text{\footnotesize $p(f(i)=0) = 1$} \\
                                     & \text{\footnotesize $p(f(i)=1) = 0$}
                                     \\ \\
                   f=1\colon & \begin{array}{c} \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} 
                                             \\ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \\ \end{array}\\
                                     &  \text{\footnotesize $p(f(i)=0) = 0$} \\
                                      & \text{\footnotesize $p(f(i)=1) = 1$}
                                     \end{array}
                    & & &
                    \begin{array}{c} 
                                \begin{array}{c} \text{\Large $\bullet$} \ \ \text{\Large $\circ$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} 
                                              \\ \text{\Large $\circ$}   \ \ \text{\Large $\circ$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\circ$}  \\ 
                   \text{\footnotesize $p(f(i)=0) = 0,\!5$} \\
                   \text{\footnotesize $p(f(i)=1) = 0,\!5$}
                   \end{array} \\ \\ \\
                   \begin{array}{l} \text{Legenda:} \\ \text{\Large $\circ$} \text{\footnotesize : entrada $i$ tal que $f(i) = 0$} \\ \text{\Large $\bullet$} \text{\footnotesize : entrada $i$ tal que $f(i) = 1$} \end{array}
                  \end{array} 
                  \end{array}

      A primeira avaliação :math:`f(i_1)` não traz mais informação para
      distinguir entre constante e balanceada.

      .. container:: float

         .. math::

            \begin{array}{cccc}
                   \bigstrut \text{$f$ constante} & & & \text{$f$ balanceada}  \vspace{8 pt} \\ 
                   \begin{array}{lc} 
                   f=0\colon & \begin{array}{c} \text{\Large $\circ$} \ \text{\Large $\boxed{\circ}$} \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} 
                                             \\ \text{\Large $\circ$} \ \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} \end{array}\\
                   %                  & \text{\footnotesize $p(f(i)=0) = 1$} \\
                   %                  & \text{\footnotesize $p(f(i)=1) = 0$}
                                     \\ \\
                   \cancel{f=1\colon} & \begin{array}{c} \text{\Large $\bullet$} \ \text{\Large $\boxed{\bullet}$} \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} 
                                             \\ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \\ \end{array}\\
                   %                  &  \text{\footnotesize $p(f(i)=0) = 0$} \\
                   %                   & \text{\footnotesize $p(f(i)=1) = 1$}
                                    \\ \end{array}
                    & & &
                    \begin{array}{c} \\
                                \begin{array}{c} \text{\Large $\bullet$} \ \text{\Large $\boxed{\circ}$} \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} 
                                              \\ \text{\Large $\circ$}   \ \ \text{\Large $\circ$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\circ$}  \\ 
                   %                       \text{\footnotesize $p(f(i)=0) = 0,\!5$} \\
                   %                       \text{\footnotesize $p(f(i)=1) = 0,\!5$}
                   \end{array} \\ \\ 
                   \begin{array}{l} \text{Legenda:} \\ \text{\Large $\circ$} \text{\footnotesize : entrada $i$ tal que $f(i) = 0$} \\ \text{\Large $\bullet$} \text{\footnotesize : entrada $i$ tal que $f(i) = 1$} \\ \\ 
                   \text{Caso: $f(i_1) = 0$ ($i_1= 1$)} \end{array}
                  \end{array} 
                  \end{array}

      A segunda aplicação, se resultar :math:`f(i_2) \neq f(i_1)`, já
      resolve com certeza que :math:`f` é balanceada.

      .. container:: float

         .. math::

            \begin{array}{cccc}
                   \bigstrut \cancel{\text{$f$ constante}} & & & \text{$f$ balanceada}  \vspace{8 pt} \\ 
                   \begin{array}{lc} 
                   \cancel{f=0\colon} & \begin{array}{c} \ \text{\Large $\circ$} \ \text{\Large $\boxed{\circ}$} \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} 
                                             \\ \ \text{\Large $\circ$}  \ \ \text{\Large $\circ$} \ \text{\Large $\boxed{\circ}$} \ \text{\Large $\circ$} \end{array}\\
                   %                  & \text{\footnotesize $p(f(i)=0) = 1$} \\
                   %                  & \text{\footnotesize $p(f(i)=1) = 0$}
                                     \\ \\
                   \cancel{f=1\colon} & \begin{array}{c} \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\bullet}$} \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} 
                                             \\ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\bullet}$} \ \text{\Large $\bullet$} \\ \end{array}\\
                   %                  &  \text{\footnotesize $p(f(i)=0) = 0$} \\
                   %                   & \text{\footnotesize $p(f(i)=1) = 1$}
                                   \\  \end{array}
                    & & &
                    \begin{array}{c} \\
                                \begin{array}{c} \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\circ}$} \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} 
                                              \\ \ \text{\Large $\circ$}   \ \ \text{\Large $\circ$} \ \text{\Large $\boxed{\bullet}$} \ \text{\Large $\circ$}  \\ 
                   %                       \text{\footnotesize $p(f(i)=0) = 0,\!5$} \\
                   %                       \text{\footnotesize $p(f(i)=1) = 0,\!5$}
                   \end{array} \\ \\ 
                   \begin{array}{l} \text{Legenda:} \\ \text{\Large $\circ$} \text{\footnotesize : entrada $i$ tal que $f(i) = 0$} \\ \text{\Large $\bullet$} \text{\footnotesize : entrada $i$ tal que $f(i) = 1$} \\ \\ 
                   \text{Caso: $f(i_1) = 0$ ($i_1 = 1$)} \\ \text{\phantom{Caso: }$f(i_2)=1$ ($i_2 = 6$)} \end{array}
                  \end{array} 
                  \end{array}

      Se o resultado for :math:`f(i_2) = f(i_1)`, tende-se a pensar que
      :math:`f` seria constante e a probabilidade de se estar errado é a
      probabilidade de tirar duas saídas iguais aleatoriamente numa
      função balanceada, ou seja, :math:`P_e = 1 \cdot 0,\!5 = 0,\!5`.

      .. container:: float

         .. math::

            \begin{array}{cccc}
                   \bigstrut \text{$f$ constante} & & & \text{$f$ balanceada}  \vspace{8 pt} \\ 
                   \begin{array}{lc} 
                   f=0\colon & \begin{array}{l} \ \text{\Large $\circ$} \ \text{\Large $\boxed{\circ}$} \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} \ 
                                             \\ \ \text{\Large $\circ$}  \ \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} \ \text{\Large $\boxed{\circ}$} \end{array}\\
                   %                  & \text{\footnotesize $p(f(i)=0) = 1$} \\
                   %                  & \text{\footnotesize $p(f(i)=1) = 0$}
                                     \\ \\
                   \cancel{f=1\colon} & \begin{array}{l} \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\bullet}$} \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$}\ 
                                             \\ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\bullet}$} \\ \end{array}\\
                   %                  &  \text{\footnotesize $p(f(i)=0) = 0$} \\
                   %                   & \text{\footnotesize $p(f(i)=1) = 1$}
                                   \\  \end{array}
                    & & &
                    \begin{array}{c} \\
                                \begin{array}{l} \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\circ}$} \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ 
                                              \\ \ \text{\Large $\circ$}   \ \ \text{\Large $\circ$} \ \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\circ}$}   \\ 
                   %                       \text{\footnotesize $p(f(i)=0) = 0,\!5$} \\
                   %                       \text{\footnotesize $p(f(i)=1) = 0,\!5$}
                   \end{array} \\ \\ 
                   \begin{array}{l} \text{Legenda:} \\ \text{\Large $\circ$} \text{\footnotesize : entrada $i$ tal que $f(i) = 0$} \\ \text{\Large $\bullet$} \text{\footnotesize : entrada $i$ tal que $f(i) = 1$} \\ \\ 
                   \text{Caso: $f(i_1) = 0$ ($i_1 = 1$)} \\ \text{\phantom{Caso: }$f(i_2)=0$ ($i_2 = 7$)} \end{array}
                  \end{array} 
                  \end{array}

      Na terceira etapa, caso :math:`f(i_3) \neq f(i_2)`, resolve-se com
      certeza que :math:`f` é balanceada e caso :math:`f(i_3) = f(i_2)`,
      conclui-se pela opção constante com probabilidade de erro igual a
      :math:`P_e = 1 \cdot 0,\!5 \cdot 0,\!5 = 0,\!25`, correspondente à
      probabilidade de que, numa função balanceada, tenha-se o mesmo
      resultado para 3 entradas sorteadas aleatoriamente com igual
      probabilidade.

      .. container:: float

         .. math::

            \begin{array}{cccc}
                   \bigstrut \cancel{\text{$f$ constante}} & & & \text{$f$ balanceada}  \\ 
                   \begin{array}{lc} 
                   \cancel{f=0\colon} & \begin{array}{l} \ \text{\Large $\circ$} \ \text{\Large $\boxed{\circ}$} \text{\Large $\boxed{\circ}$} \ \text{\Large $\circ$} 
                                             \\ \ \text{\Large $\circ$}  \ \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} \ \text{\Large $\boxed{\circ}$} \end{array}\\
                   %                  & \text{\footnotesize $p(f(i)=0) = 1$} \\
                   %                  & \text{\footnotesize $p(f(i)=1) = 0$}
                                     \\ \\
                   \cancel{f=1\colon} & \begin{array}{l} \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\bullet}$} \text{\Large $\boxed{\bullet}$} \ \text{\Large $\bullet$} 
                                             \\ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\bullet}$} \\ \end{array}\\
                   %                  &  \text{\footnotesize $p(f(i)=0) = 0$} \\
                   %                   & \text{\footnotesize $p(f(i)=1) = 1$}
                                   \\  \end{array}
                    & & &
                    \begin{array}{c} \\
                                \begin{array}{l} \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\circ}$}  \text{\Large $\boxed{\bullet}$} \ \text{\Large $\bullet$} 
                                              \\ \ \text{\Large $\circ$}   \ \ \text{\Large $\circ$} \ \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\circ}$}  \\ 
                   %                       \text{\footnotesize $p(f(i)=0) = 0,\!5$} \\
                   %                       \text{\footnotesize $p(f(i)=1) = 0,\!5$}
                   \end{array} \\ \\ 
                   \begin{array}{l} \text{Legenda:} \\ \text{\Large $\circ$} \text{\footnotesize : entrada $i$ tal que $f(i) = 0$} \\ \text{\Large $\bullet$} \text{\footnotesize : entrada $i$ tal que $f(i) = 1$} \\ \\ 
                   \text{Caso: $f(i_1) = 0$ ($i_1 = 1$)} \\ \text{\phantom{Caso: }$f(i_2)=0$ ($i_2 = 7$)} \\ \text{\phantom{Caso: }$f(i_3)=1$ ($i_3 = 2$)} \end{array}
                  \end{array} 
                  \end{array}

      .. container:: float

         .. math::

            \begin{array}{cccc}
                   \bigstrut  \text{$f$ constante} & & & \text{$f$ balanceada}  \\ 
                   \begin{array}{lc} 
                   f=0\colon & \begin{array}{l} \ \text{\Large $\circ$} \ \text{\Large $\boxed{\circ}$} \ \text{\Large $\circ$} \ \ \text{\Large $\circ$} 
                                             \\ \ \text{\Large $\circ$}  \ \text{\Large $\boxed{\circ}$} \ \text{\Large $\circ$} \ \text{\Large $\boxed{\circ}$} \end{array}\\
                   %                  & \text{\footnotesize $p(f(i)=0) = 1$} \\
                   %                  & \text{\footnotesize $p(f(i)=1) = 0$}
                                     \\ \\
                   \cancel{f=1\colon} & \begin{array}{l} \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\bullet}$} \ \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} 
                                             \\ \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\bullet}$} \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\bullet}$} \\ \end{array}\\
                   %                  &  \text{\footnotesize $p(f(i)=0) = 0$} \\
                   %                   & \text{\footnotesize $p(f(i)=1) = 1$}
                                   \\  \end{array}
                    & & &
                    \begin{array}{c} \\
                                \begin{array}{l} \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\circ}$} \  \text{\Large $\bullet$} \ \ \text{\Large $\bullet$} 
                                              \\ \ \text{\Large $\circ$}   \ \text{\Large $\boxed{\circ}$} \ \text{\Large $\bullet$} \ \text{\Large $\boxed{\circ}$}  \\ 
                   %                       \text{\footnotesize $p(f(i)=0) = 0,\!5$} \\
                   %                       \text{\footnotesize $p(f(i)=1) = 0,\!5$}
                   \end{array} \\ \\ 
                   \begin{array}{l} \text{Legenda:} \\ \text{\Large $\circ$} \text{\footnotesize : entrada $i$ tal que $f(i) = 0$} \\ \text{\Large $\bullet$} \text{\footnotesize : entrada $i$ tal que $f(i) = 1$} \\ \\ 
                   \text{Caso: $f(i_1) = 0$ ($i_1 = 1$)} \\ \text{\phantom{Caso: }$f(i_2)=0$ ($i_2 = 7$)} \\ \text{\phantom{Caso: }$f(i_3)=0$ ($i_3 = 5$)} \end{array}
                  \end{array} 
                  \end{array}

      Seguindo essa ideia, na :math:`m`-ésima aplicação de :math:`f`, se
      ocorrer :math:`f(i_m) \neq f(i_{m-1})`, conclui-se com certeza a
      opção “:math:`f` é balanceada” e se :math:`f(i_m) = f(i_{m-1})`,
      pode-se concluir que “:math:`f` é constante” com probabilidade de
      erro

      .. math:: P_e = 1 \cdot 0,\!5 \cdot \ldots \cdot 0,\!5 = (0,\!5)^{m-1} = 1/2^{m-1} \ .

      Para uma probabilidade de erro :math:`P_e < 1/2` na decisão,
      deve-se repetir o algoritmo até que a probabilidade de erro
      :math:`P_{e,m} = 1/2^{m-1}` satisfaça

      .. math:: \frac{1}{2^{m-1}} < \frac{1}{2} \implies 2^m > 2^2 \implies m >2 \implies m \geq 3  \ .

      Se forem :math:`m=3` aplicações, a probabilidade de erro será
      limitada por :math:`\varepsilon = 1/2^{m-1} = 0,\!25 < 0,\!5`,
      como visto anteriormente.

   .. container:: subsection

      Comparação de Desempenho

      A tabela abaixo traz a comparação entre o desempenho dos
      algoritmos clássico determinístico, clássico probabilístico e
      quântico.

      .. table:: Comparação de desempenho entre os algoritmos quântico,
      clássico determinístico e clássico probabilístico (com
      probabilidade de erro :math:`<50\%`) para o problema de
      Deutsch-Jozsa.

         ============ ======================================
         Algoritmo    Desempenho (# aplicações de :math:`f`)
         ============ ======================================
         Class. Det.  :math:`2^n/2 + 1`
         Class. Prob. 3
         Quântico     1
         ============ ======================================

      Essa comparação entre o desempenho clássico e quântico, no
      entanto, não pode ser considerada muito seriamente. Há que se
      levar em conta que são arquiteturas diferentes: aplicar uma
      operação :math:`f` clássica (correspondente a chamar uma subrotina
      “caixa preta”) e aplicar o oráculo de fase :math:`O_\text{F}(f)`
      em um circuito quântico são coisas distintas. Não é claro que
      essas operações têm custo computacional equivalente para que sejam
      comparadas diretamente como na tabela apresentada. Por outro lado,
      como comparação simplificada, essa análise serve para se ter uma
      noção dos ganhos que a Computação Quântica poderia trazer em
      relação a Computação Clássica.

      Em relação ao algoritmo clássico determinístico, o algoritmo
      quântico apresenta ganho exponencial em desempenho. Já em relação
      ao algoritmo clássico probabilístico, o desempenho é semelhante.

.. container:: section

   Algoritmo de Simon

   Assim como no caso do Algoritmo de Deutsch-Jozsa, o Algoritmo de
   Simon é um algoritmo quântico projetado para resolver o Problema de
   Simon. Esse problema também tem propósito de funcionar como
   laboratório de testes para a Computação Quântica, não apresentando
   aplicações conhecidas. A principal referência para esta seção são as
   videoaulas U2.3 da subunidade SU2 disponíveis em
   :raw-latex:`\cite{videolecture:qisI_p2}`.

   .. container:: subsection

      Problema de Simon

      O problema de Simon é um problema de promessa, em que é dada uma
      função booleana :math:`f \colon \{ 0,1 \}^n \to  \{ 0,1 \}^n` que
      pode ser ou 1-para-1 ou 2-para-1. Esses termos serão definidos e
      acompanhados de exemplos para melhor entendimento. Em seguida, o
      enunciado do problema de Simon será escrito formalmente.

      .. container:: definition

         Seja :math:`f \colon \{ 0,1 \}^n \to  \{ 0,1 \}^n` uma função
         booleana de :math:`n` para :math:`n` bits.

         A função :math:`f` é dita *1-para-1* se é uma bijeção [4]_.
         Nesse caso, isso significa que cada resultado
         :math:`y \in  \{ 0,1 \}^n` é obtido por exatamente uma entrada
         :math:`x_1`, ou seja, :math:`f(x_1) = y`. Cada duas entradas
         distintas :math:`x_1 \neq x_2` geram resultados diferentes
         :math:`f(x_1) \neq f(x_2)`.

         A função :math:`f` é dita *2-para-1* se cada resultado
         :math:`y \in  \{ 0,1 \}^n` é obtido por exatamente duas
         entradas :math:`x_1` e :math:`x_2`, isto é,
         :math:`f(x_1) = f(x_2) = y`.

      O problema de Simon requer um compromisso para a função booleana
      de entrada. A propriedade que a função deve satisfazer é chamada,
      no presente trabalho, de *propriedade de Simon*.

      .. container:: definition

         Sejam :math:`f \colon \{ 0,1 \}^n \to  \{ 0,1 \}^n` uma função
         booleana de :math:`n` para :math:`n` bits e
         :math:`c \in  \{ 0,1 \}^n`.

         Diz-se que :math:`f` satisfaz a *propriedade de Simon* se

         .. math:: f(x_1) = f(x_2) \Longleftrightarrow x_2 = x_1 \oplus c \ ,

         em que a operação :math:`\oplus` é a XOR (ou seja, adição
         módulo 2) realizada bit a bit nos dois vetores de bits.

      .. container:: example

         A função booleana :math:`f \colon \{ 0,1 \}^2 \to  \{ 0,1 \}^2`
         definida pela tabela abaixo é 1-para-1.

         .. math::

            \begin{array}{c|c}
                 x & f(x) \\ \hline 
                 00  & 10 \\
                 01  & 11 \\
                 10  & 00 \\
                 11  & 01
                \end{array}

      .. container:: example

         A função booleana :math:`f \colon \{ 0,1 \}^2 \to  \{ 0,1 \}^2`
         definida pela tabela abaixo é 2-para-1.

         .. math::

            \begin{array}{c|c}
                 x   & f(x) \\ \hline 
                 00  & 10 \\
                 01  & 01 \\
                 10  & 10 \\
                 11  & 01
                \end{array}

         Essa função também satisfaz a propriedade de Simon com
         :math:`c = 10`. De fato,

         .. math::

            \begin{array}{lll}
                    10 = 00 \oplus 10 & & f(00) = f(10) = 10  \\
                    11 = 01 \oplus 10 & & f(01) = f(11) = 01 \ .
                   \end{array}

      .. container:: remark

         Nem toda função booleana 2-para-1 satisfaz a propriedade de
         Simon. De fato, a função
         :math:`f \colon \{ 0,1 \}^3 \to  \{ 0,1 \}^3` dada por [5]_

         .. math::

            \begin{array}{c|cccc|c}
                 x    & f(x) & & & x & f(x) \\ \cline{1-2} \cline{5-6}
                 000  & 2 & & & 100  & 2 \\
                 001  & 5 & & & 101  & 3 \\
                 010  & 1 & & & 110  & 1 \\
                 011  & 5 & & & 111  & 3
                \end{array}

         Essa função é 2-para-1. Se satisfizesse a propriedade de Simon,
         existiria :math:`c` satisfazendo :math:`f(x\oplus c ) = f(x)`
         para todo :math:`x`. No entanto,

         .. math:: f(000) = f(100), 100 = 000 \oplus 100 \implies c = 100

         e tem-se que

         .. math:: 101 = 001 \oplus 100 \ \text{mas} \ f(101) \neq f(001) \ .

         Essa contradição significa que a propriedade de Simon não é
         satisfeita.

      .. container:: remark

         Se :math:`f` satisfaz a propriedade de Simon com
         :math:`c=0\ldots0`, então :math:`f` é 1-para-1. E se :math:`f`
         satisfaz a propriedade de Simon com :math:`c\neq 0\ldots0`,
         então :math:`f` é 2-para-1.

      De posse dessas definições, o problema de Simon tem o seguinte
      enunciado.

      .. container:: problem*

         de Simon Dada uma função booleana
         :math:`f\colon \{ 0,1 \}^n \to  \{ 0,1 \}^n` satisfazendo a
         propriedade de Simon, distinguir se :math:`f` é 1-para-1 ou se
         é 2-para-1 e encontrar o período :math:`c`.

   .. container:: subsection

      Algoritmo de Simon

      O algoritmo a ser apresentado pressupõe que a função booleana
      :math:`f` seja dada como um oráculo XOR (ver seção
      `[cap5:sec_oraculo_xor] <#cap5:sec_oraculo_xor>`__). Também são
      necessários 2 registradores de :math:`n` qubits. O algoritmo
      descreve uma subrotina a ser repetida da ordem de :math:`n` vezes.
      Em cada iteração, obtém-se um pouco de informação, que será
      processada classicamente para obter como saída o valor de
      :math:`c` e a decisão se :math:`f` é 1-para-1 ou 2-para-1.

      .. rubric:: Algoritmo de Simon
         :name: algoritmo-de-simon
         :class: unnumbered

      | **Entrada:** :math:`O_\text{XOR}(f) = O`   (oráculo XOR
        associado a :math:`f`)
      | **Procedimento:**
      | :math:`\begin{array}{lll}
           \text{etapa 0:} & \ket{0}\ket{0} & \text{\small{preparação do estado inicial}} \vspace{2pt} \\ 
           \text{etapa 1:} &\displaystyle \tfrac{1}{\sqrt{2^n}} \sum_{x\in\mathbb{B}_n} \ket{x}\ket{0} & \text{\small{$H^{\otimes n}$ no registrador 1}} \vspace{2pt} \\ 
           \text{etapa 2:} &\displaystyle \tfrac{1}{\sqrt{2^n}} \sum_{x\in\mathbb{B}_n} \ket{x}\ket{f(x)} & \text{\small{aplicação de $f$ (oráculo XOR)}} \vspace{2pt} \\ 
           \text{etapa 3:} &\displaystyle \tfrac{1}{2^n} \sum_{x\in\mathbb{B}_n}\sum_{y\in\mathbb{B}_n} \ket{y}\ket{f(x)} & \text{\small{$H^{\otimes n}$ no registrador 1}} \vspace{2pt} \\ 
           \text{etapa 4:} &\text{\small{Medição}}    & \text{\small{obtém informação sobre a resposta}} \vspace{2pt} \\ 
           \text{etapa 5:} &\multicolumn{2}{l}{\text{\small{Repetir etapas 0-4 }} \text{\small{da ordem de $n$ vezes}} } \vspace{2pt} \\ 
           \text{etapa 6:} &\multicolumn{2}{l}{\text{\small{Computar a resposta }} \text{\small{classicamente com as }} \text{\small{informações obtidas}}  } 
          \end{array}`
      | **Saída:** Após processamento clássico final, valor do período
        :math:`c` e decisão se :math:`f` é 1-para-1 ou 2-para-1.

      .. rubric:: Circuito
         :name: circuito-1
         :class: unnumbered

      .. container:: float
         :name: cap5:circuito_simon

         | Notação compacta:

           .. math::

              \Qcircuit @C=5pt @R=10pt @!R 
                  {
                  \lstick{\text{reg.1} \ \ \ket{0}^{\otimes n}} &  \ustick{\ n}\qw  & {/} \qw & \qw & \gate{H^{\otimes n}} & \ctrl{1}               & \gate{H^{\otimes n}} & \qw & \qw &\meter \\
                 \lstick{\text{reg.2} \ \ \ket{0}^{\otimes n}}  &  \ustick{\ n}\qw  & {/} \qw & \qw & \qw                  & \gate{O_\text{XOR}(f)} & \qw                  & \qw & \qw &\meter 
                 }
         | Notação expandida:

           .. math::

              \Qcircuit @C=5pt @R=4pt %@!R 
                  {
                  & & & \lstick{\ket{0}}  & \qw \gategroup{1}{1}{4}{1}{1.2em}{\{}& \qw & \gate{H} & \ctrl{1}                       & \gate{H} & \qw & \qw &\meter \\
                   & & & \lstick{\ket{0}}  & \qw & \qw & \gate{H} & \ctrl{2}                       & \gate{H} & \qw & \qw &\meter \\
                      \ustick{\text{reg.1}\quad \quad \quad \quad \ \ }  & & & &\vdots&        &          &                                &          &     &\vdots&   \\
                   & & & \lstick{\ket{0}}  & \qw & \qw & \gate{H} & \ctrl{2}                       & \gate{H} & \qw & \qw &\meter  \\
                            & & &          &     &     &          &                                &          &     &     &       \\
                  & & & \lstick{\ket{0}}  & \qw \gategroup{6}{1}{9}{1}{1.2em}{\{}& \qw & \qw      & \multigate{3}{O_\text{XOR}(f)} & \qw      & \qw & \qw &\meter \\
                  & & & \lstick{\ket{0}}   & \qw & \qw & \qw      & \ghost{O_\text{XOR}(f)}        & \qw      & \qw & \qw &\meter \\
                  \ustick{\text{reg.2}\quad \quad \quad \quad \ \ }    & & &   &\vdots&        &          & \ghostnoqw{O_\text{XOR}(f)}    &          &     &\vdots&      \\
                 & & &  \lstick{\ket{0}}  & \qw & \qw & \qw      & \ghost{O_\text{XOR}(f)}        & \qw      & \qw & \qw &\meter 
                 }

      .. rubric:: Contas auxiliares
         :name: contas-auxiliares
         :class: unnumbered

      .. container:: definition

         Usa-se a seguinte notação, com o intuito de simplificar algumas
         expressões:

         .. math::

            \begin{split} 
                   \tilde{0} &= +  \\
                   \tilde{1} &= - \ . 
                   \end{split}

         Dado um vetor de bits :math:`x \in \mathbb{B}_n`, escreve-se
         :math:`\ket{\tilde{x}} = \ket{\tilde{x}_0 \tilde{x}_1 \ldots \tilde{x}_{n-1} }`
         para designar um produto tensorial de estados :math:`\ket{+}` e
         :math:`\ket{-}`. Por exemplo,

         .. math:: \ket{x} = \ket{0110} \Longleftrightarrow \ket{\tilde{x}} = \ket{+--+} \ .

      .. container:: definition

         :math:`\mathbb{B}_n` é o conjunto de todos os vetores de
         :math:`n` bits.

      .. container:: proposition

         Vale que

         .. math:: H^{\otimes n} = \sum_{y \in \mathbb{B}_n} \ketbra{\tilde{y}}{y} \ .

      .. container:: proof

         *Proof.* Prova-se por indução em :math:`n`.

         Vale para :math:`n=1` qubit, já que :math:`H` pode ser escrito
         como

         .. math:: H = \ketbra{+}{0} + \ketbra{-}{1} \ .

         Vale para :math:`n=2` qubits, pois

         .. math::

            \begin{split} 
                   H^{\otimes 2} 
                   &= H \otimes H \\
                   &= \big( \ketbra{+}{0} + \ketbra{-}{1} \big) \otimes  \big( \ketbra{+}{0} + \ketbra{-}{1} \big) \\
                   &= \ketbra{++}{00} + \ketbra{+-}{01} + \ketbra{-+}{10} + \ketbra{--}{11} \\
                   &= \sum_{y \in \mathbb{B}_2}\ketbra{\tilde{y}}{y} \ . 
                   \end{split}

         Supõe-se, então, que seja válido para :math:`n` qubits.
         Verifica-se o caso :math:`n+1`:

         .. math::

            \begin{split} 
                   H^{\otimes n+1} 
                   &= H^{\otimes n} \otimes H \\
                   &= \left( \sum_{y \in \mathbb{B}_n}\ketbra{\tilde{y}}{y} \right) \otimes \big( \ketbra{+}{0} + \ketbra{-}{1} \big) \\
                   &= \left( \sum_{y \in \mathbb{B}_n}\ketbra{\tilde{y}_0 \tilde{y}_1 \ldots \tilde{y}_{n-1}}{y_0 y_1 \ldots y_{n-1}} \right) \otimes \big( \underbrace{\ketbra{+}{0}}_{y_n = 0} + \underbrace{\ketbra{-}{1}}_{y_n = 1} \big) \\
                   &= \sum_{y \in \mathbb{B}_{n+1}} \ketbra{\tilde{y}_0 \tilde{y}_1 \ldots \tilde{y}_{n-1} \tilde{y}_n}{y_0 y_1 \ldots y_{n-1} y_n} \\
                   &= \sum_{y \in \mathbb{B}_{n+1}} \ketbra{\tilde{y}}{y} \ .
                   \end{split}

         Isso conclui a indução em :math:`n`. ◻

      .. container:: proposition

         Vale que

         .. math:: \ket{\tilde{x}} = \frac{1}{\sqrt{2^n}} \sum_{y\in\mathbb{B}_n} (-1)^{x\cdot y} \ket{y} \ ,

         em que
         :math:`x \cdot y = x_0 y_0 + x_1 y_1 + \ldots + x_{n-1}y_{n-1}`.

      .. container:: proof

         *Proof.* Mostra-se por indução em :math:`n`.

         Para :math:`n=1` qubit, tem-se que

         .. math::

            \begin{split}
                    \ket{\tilde{0}} &= \ket{+} = \frac{1}{\sqrt{2}} \big( \ket{0} + \ket{1} \big) \\
                    \ket{\tilde{1}} &= \ket{-} = \frac{1}{\sqrt{2}} \big( \ket{0} - \ket{1} \big) \ .
                   \end{split}

         Para :math:`n=2` qubits, tem-se que

         .. math::

            \begin{split}
                    \ket{\tilde{0} \tilde{0} } &= \ket{++} =  \frac{1}{\sqrt{2^2}} \big( \ket{00} + \ket{01} + \ket{10} + \ket{11} \big) \\
                    \ket{\tilde{0} \tilde{1} } &= \ket{+-} =  \frac{1}{\sqrt{2^2}} \big( \ket{00} - \ket{01} + \ket{10} - \ket{11}\big) \\ 
                    \ket{\tilde{1} \tilde{0} } &= \ket{-+} =  \frac{1}{\sqrt{2^2}} \big( \ket{00} + \ket{01} - \ket{10} - \ket{11} \big) \\ 
                    \ket{\tilde{1} \tilde{1} } &= \ket{--} =  \frac{1}{\sqrt{2^2}} \big( \ket{00} - \ket{01} - \ket{10} + \ket{11} \big) \ ,
                   \end{split}

         que pode ser resumido em

         .. math:: \ket{\tilde{x}} = \frac{1}{\sqrt{2^2}} \sum_{y\in\mathbb{B}_n} (-1)^{x\cdot y} \ket{y}  \ .

         Assume-se que o enunciado seja válido para :math:`n` qubits. O
         caso :math:`n+1` fica como a seguir.

         .. math:: \ket{\tilde{x}_0 \tilde{x}_1 \ldots \tilde{x}_{n-1} \tilde{x}_n } = \ket{\tilde{x}_0 \tilde{x}_1 \ldots \tilde{x}_{n-1}} \otimes \ket{\tilde{x}_n }

         Caso :math:`\tilde{x}_n = +`, tem-se

         .. math::

            \begin{split}
                  &\ket{\tilde{x}_0 \tilde{x}_1 \ldots \tilde{x}_{n-1} + } \\
                  \phantom{x}  &= \ket{\tilde{x}_0 \tilde{x}_1 \ldots \tilde{x}_{n-1}} \otimes \ket{+} \\
                   &=  \left(\frac{1}{\sqrt{2^n}} \sum_{y\in\mathbb{B}_n} (-1)^{(x_0 \ldots x_{n-1})\cdot y} \ket{y} \right) \otimes \ket{+}  \\
                   &=  \frac{1}{\sqrt{2^n}} \sum_{y\in\mathbb{B}_n} (-1)^{(x_0 \ldots x_{n-1})\cdot y} \ket{y_0 \ldots y_{n-1}}  \otimes \frac{1}{\sqrt{2}} \big( \ket{0} + \ket{1} \big) \\
                   &=  \frac{1}{\sqrt{2^{n+1}}} \sum_{y\in\mathbb{B}_n} (-1)^{(x_0 \ldots x_{n-1})\cdot y} \big( \underbrace{\ket{y_0 \ldots y_{n-1} 0}}_{y_n = 0} +  \underbrace{\ket{y_0 \ldots y_{n-1} 1}}_{y_n = 1} \big) \\
                   &=  \frac{1}{\sqrt{2^{n+1}}} \Biggg( \sum_{y\in\mathbb{B}_n} (-1)^{(x_0 \ldots x_{n-1})\cdot y + 0 \cdot 0} \underbrace{\ket{y_0 \ldots y_{n-1} 0}}_{y_n = 0} \ + \\
                   & \qquad \qquad \qquad \qquad \qquad +\ (-1)^{(x_0 \ldots x_{n-1})\cdot y + 0 \cdot 1} \underbrace{\ket{y_0 \ldots y_{n-1} 1}}_{y_n = 1} \Biggg) \\
                   &= \frac{1}{\sqrt{2^{n+1}}} \sum_{y\in\mathbb{B}_{n+1}} (-1)^{(x_0 \ldots x_{n-1}0)\cdot y} \ket{y_0 \ldots y_{n-1} y_n } \ .
                  \end{split}

         No caso :math:`\tilde{x}_n = -`, tem-se

         .. math::

            \begin{split}
                  &\ket{\tilde{x}_0 \tilde{x}_1 \ldots \tilde{x}_{n-1} - } \\
                  \phantom{x}  &= \ket{\tilde{x}_0 \tilde{x}_1 \ldots \tilde{x}_{n-1}} \otimes \ket{-} \\
                   &=  \left(\frac{1}{\sqrt{2^n}} \sum_{y\in\mathbb{B}_n} (-1)^{(x_0 \ldots x_{n-1})\cdot y} \ket{y} \right) \otimes \ket{-}  \\
                   &=  \frac{1}{\sqrt{2^n}} \sum_{y\in\mathbb{B}_n} (-1)^{(x_0 \ldots x_{n-1})\cdot y} \ket{y_0 \ldots y_{n-1}}  \otimes \frac{1}{\sqrt{2}} \big( \ket{0} - \ket{1} \big) \\
                   &=  \frac{1}{\sqrt{2^{n+1}}} \sum_{y\in\mathbb{B}_n} (-1)^{(x_0 \ldots x_{n-1})\cdot y} \big( \underbrace{\ket{y_0 \ldots y_{n-1} 0}}_{y_n = 0} -  \underbrace{\ket{y_0 \ldots y_{n-1} 1}}_{y_n = 1} \big) \\
                   &=  \frac{1}{\sqrt{2^{n+1}}} \Biggg( \sum_{y\in\mathbb{B}_n} (-1)^{(x_0 \ldots x_{n-1})\cdot y + 1 \cdot 0} \underbrace{\ket{y_0 \ldots y_{n-1} 0}}_{y_n = 0} \ + \\
                   & \qquad \qquad \qquad \qquad \qquad + \ (-1)^{(x_0 \ldots x_{n-1})\cdot y + 1 \cdot 1} \underbrace{\ket{y_0 \ldots y_{n-1} 1}}_{y_n = 1} \Biggg) \\
                   &= \frac{1}{\sqrt{2^{n+1}}} \sum_{y\in\mathbb{B}_{n+1}} (-1)^{(x_0 \ldots x_{n-1}1)\cdot y} \ket{y_0 \ldots y_{n-1} y_n } \ .
                  \end{split}

         Isso conclui a demonstração. ◻

      .. container:: proposition

         O produto tensorial de :math:`n` operadores de Hadamard é dado
         por

         .. math:: H^{\otimes n} = \frac{1}{\sqrt{2^n}} \sum_{x,y \in \mathbb{B}_n} (-1)^{x \cdot y} \ketbra{x}{y} \ .

      .. container:: proof

         *Proof.* Usando as proposições
         `[cap5:prop_produto_tensorial_de_H_1] <#cap5:prop_produto_tensorial_de_H_1>`__
         e
         `[cap5:prop_produto_tensorial_estados\_+-] <#cap5:prop_produto_tensorial_estados_+->`__,
         tem-se que

         .. math:: H^{\otimes n} = \sum_{y \in \mathbb{B}_n} \ketbra{\tilde{y}}{y} = \sum_{y \in \mathbb{B}_n} \frac{1}{\sqrt{2^n}} \sum_{x\in\mathbb{B}_n} (-1)^{x\cdot y}\ketbra{x}{y} \ . \qedhere

          ◻

      .. container:: proposition

         A aplicação de :math:`H^{\otimes n}` a um estado
         :math:`\ket{x} = \ket{x_{0} x_1 \ldots x_{n-1}}` na base
         computacional é dada por

         .. math:: H^{\otimes n} \ket{x} = \frac{1}{\sqrt{2^n}} \sum_{y\in\mathbb{B}_n} (-1)^{x\cdot y} \ket{y} \ ,

         em que
         :math:`x \cdot y = x_0 y_0 + x_1 y_1 + \ldots + x_{n-1}y_{n-1}`.

      .. container:: proof

         *Proof.* Usando as proposição
         `[cap5:prop_expressao_para_prod_tens_portas_hadamard] <#cap5:prop_expressao_para_prod_tens_portas_hadamard>`__,
         tem-se que

         .. math::

            \begin{split}
                    H^{\otimes n} \ket{x} 
                    &= \frac{1}{\sqrt{2^n}} \sum_{y,z\in\mathbb{B}_n} (-1)^{y\cdot z} \ket{y}\bra{z} \ket{x} \\
                    &= \frac{1}{\sqrt{2^n}} \sum_{y,z\in\mathbb{B}_n} (-1)^{y\cdot z} \ket{y} \delta_{z,x} \\
                    &= \frac{1}{\sqrt{2^n}} \sum_{y \in\mathbb{B}_n} (-1)^{y\cdot x} \ket{y} \ . 
                   \end{split}

          ◻

      .. container:: remark

         A soma e o produto em

         .. math:: x \cdot y = x_0 y_0 + x_1 y_1 + \ldots + x_{n-1}y_{n-1} \tag{int}

         \ podem ser entendidos como operações com números ou como
         operações com bits

         .. math:: x \cdot y = x_0 y_0 \oplus x_1 y_1 \oplus \ldots \oplus x_{n-1}y_{n-1} \tag{bit}

         em que o produto é dado pela AND e a soma :math:`\oplus` é dada
         pela XOR. Ambas as expressões resultam no mesmo sinal
         :math:`(-1)^{x\cdot y}`, pois a expressão (bit) corresponde a
         (int) módulo 2, visto que a AND se comporta como um produto e a
         XOR, como uma adição módulo 2.

      .. rubric:: Etapas da subrotina de Simon
         :name: etapas-da-subrotina-de-simon
         :class: unnumbered

      As etapas da subrotina quântica são mostradas em detalhes no texto
      que segue. Inicialmente, aplica-se :math:`H^{\otimes n}` ao
      primeiro registrador, obtendo-se

      .. math:: \ket{\psi_1} = \frac{1}{\sqrt{2^n}} \sum_{x\in\mathbb{B}_n} \ket{x}\ket{0} ,

      em que o primeiro ket engloba :math:`n` qubits e representa o
      primeiro registrador, e o segundo ket contém :math:`n` bits e
      representa o segundo registrador.

      A aplicação do oráculo na etapa 2 mantém o primeiro registrador e
      faz a XOR bit a bit de :math:`0` com :math:`f(x)`:

      .. math::

         \begin{split}
                \ket{\psi_2} 
                &= O_{\text{XOR}} \ket{\psi_1} \\
                &= \frac{1}{\sqrt{2^n}} \sum_{x\in\mathbb{B}_n} O_{\text{XOR}} \ket{x}\ket{0} \\
                &= \frac{1}{\sqrt{2^n}} \sum_{x\in\mathbb{B}_n} \ket{x}\ket{f(x)} \ .
               \end{split}

      Na etapa 3, aplica-se novamente :math:`H^{\otimes n}` ao primeiro
      registrador:

      .. math::

         \begin{split} 
                \ket{\psi_3} 
                &= \frac{1}{\sqrt{2^n}} \sum_{x\in\mathbb{B}_n} \big(H^{\otimes n}\ket{x}\big)\ket{f(x)} \\
                &= \frac{1}{\sqrt{2^n}} \sum_{x\in\mathbb{B}_n}\frac{1}{\sqrt{2^n}} \sum_{y\in\mathbb{B}_n} (-1)^{x\cdot y} \ket{y}\ket{f(x)} \\
                &= \frac{1}{2^n} \sum_{x\in\mathbb{B}_n} \sum_{y\in\mathbb{B}_n} (-1)^{x\cdot y} \ket{y}\ket{f(x)} \ . 
               \end{split} \tag{$\ast$}

      A medida na base computacional, na etapa 4, faz o estado do
      sistema colapsar em :math:`\ket{y}\ket{z}`, com :math:`z=f(x)`.
      Como :math:`f` tem a propriedade de Simon, os únicos valores que
      resultam em :math:`z` pela aplicação de :math:`f` são

      .. math:: z = f(x_1) = f(x_2) \ , \ \ x_2 = x_1 \oplus c \ .

      .. rubric:: Probabilidades nas medições
         :name: probabilidades-nas-medições
         :class: unnumbered

      Caso :math:`c \neq 0`, o coeficiente multiplicando o estado
      :math:`\ket{y}\ket{z}` em (:math:`\ast`) é dado por

      .. math::

         \begin{split} 
              a_{y,z}
              &= \frac{1}{2^n} \big( (-1)^{x_1 \cdot y} + (-1)^{x_2 \cdot y} \big) \\
              &= \frac{1}{2^n} \big( (-1)^{x_1 \cdot y} + (-1)^{(x_1 \oplus c) \cdot y} \big) \\
              &= \frac{1}{2^n} \big( (-1)^{x_1 \cdot y} + (-1)^{(x_1 \cdot y) \oplus (c \cdot y)} \big) \\
              &= \frac{1}{2^n} \big( (-1)^{x_1 \cdot y} + (-1)^{x_1 \cdot y} (-1)^{ c \cdot y} \big) \\
              &= \frac{1}{2^n} (-1)^{(x_1 \cdot y)} \big( 1 + (-1)^{ c \cdot y} \big) \\
              &= \begin{cases} 
                  0 & \ \ \   c\cdot y = 1 \ , \\
                  (-1)^{(x_1 \cdot y)} \frac{2}{2^n} & \ \ \   c \cdot y = 0 \ .
                 \end{cases}
              \end{split}

      A probabilidade de se encontrar o sistema no estado
      :math:`\ket{y}\ket{z}` é, então,

      .. math::

         \begin{split}
               p_{y,z} 
               &= \abs{a_{y,z}}^2 = \begin{cases} 
                  0 & \ \ \  c\cdot y = 1 \\
                  \frac{2^2}{2^{2n}} & \ \ \  c \cdot y = 0
                 \end{cases}
              \end{split} \tag{p1}

      Assim, a medida só fornece vetores de bits :math:`y`
      perpendiculares a :math:`c`. A informação que se ganha para
      encontrar :math:`c` é a equação

      .. math:: c \cdot y = c_1 y_1 \oplus c_1 y_1 \oplus \ldots \oplus c_n y_n = 0  \ .

      Caso :math:`c = 0`, o coeficiente em (:math:`\ast`) fica apenas
      :math:`a_{y,z}
            = \frac{1}{2^n} (-1)^{x_1 \cdot y}` e a probabilidade de se
      encontrar o sistema em :math:`\ket{y}\ket{z}` é

      .. math::

         p_{y,z}
              =  \abs{a_{y,z}}^2 = 2^{-2n} \ . \tag{p2}

      Logo, qualquer vetor de bits :math:`y` pode sair como resultado da
      medição.

      .. rubric:: Encontrando o valor do período :math:`c` – exemplo
         :name: encontrando-o-valor-do-período-c-exemplo
         :class: unnumbered

      Primeiramente, apresenta-se o processamento para encontrar o
      período :math:`c` em um exemplo, com o objetivo de facilitar a
      compreensão do método no caso geral.

      .. container:: example

          

         Seja :math:`n = 4` bits. Aplica-se a primeira iteração da
         subrotina quântica do algoritmo de Simon. Suponha que se obteve
         o resultado :math:`y^{(1)} = 0111`. Esse resultado gera a
         equação

         .. math:: y^{(1)} \cdot c = 0 \implies c_2 \oplus c_3 \oplus c_4 = 0 \implies c_2 = c_3 \oplus c_4 \ .

         Continua-se aplicando a subrotina até se obter :math:`n-1 = 3`
         equações LI.

         A segunda iteração fornece :math:`y^{(2)} = 1001`. Esse
         resultado corresponde à equação

         .. math:: y^{(2)} \cdot c = 0 \implies c_1 \oplus c_4 = 0  \ ,

         e o sistema, após simplificação, fica

         .. math::

            \begin{cases}
                       c_2 = c_3 \oplus c_4 \\
                       c_1 = c_4 \ .
                     \end{cases}

         Como ainda não são 3 equações LI, continua-se a iteração.

         Na terceira iteração, o resultado é :math:`y^{(3)} = 1110`. A
         equação correspondente é

         .. math:: y^{(3)} \cdot c = 0 \implies c_1 \oplus c_2 \oplus c_3 = 0  \implies c_4 \oplus (c_3 \oplus c_4)\oplus c_3 = 0 \implies 0 = 0 \ ,

         e essa equação não fornece informação útil. O sistema continua
         sendo de 2 equações LI:

         .. math::

            \begin{cases}
                       c_2 = c_3 \oplus c_4 \\
                       c_1 = c_4 \ .
                     \end{cases}

         Na quarta iteração obtém-se :math:`y^{(4)} = 0001`, e a equação
         que esse resultado gera é

         .. math:: y^{(4)} \cdot c = 0 \implies c_4 = 0  \ .

         O sistema fica

         .. math::

            \begin{cases}
                       c_2 = c_3 \\
                       c_1 = 0  \\
                       c_4 = 0  \ .
                     \end{cases}

         Agora são :math:`3 = n-1` equações LI. As soluções são
         :math:`c' = 0000` e :math:`c'' = 0110`. Poder-se-ia concluir
         que :math:`f` é 2-para-1 com período :math:`c=0110`. No
         entanto, a probabilidade de se estar errado nesse caso é a
         probabilidade de se obter 4 resultados no mesmo subespaço de
         dimensão 3, sendo que :math:`f` seria 1-para-1 e os
         :math:`2^n = 2^4` resultados seriam equiprováveis:

         .. math:: \varepsilon_4 \lesssim 1 \cdot 1 \cdot 1 \cdot \frac{2^3}{2^4} = \frac{1}{2}

         Para reduzir a probabilidade de erro, aplica-se a subrotina
         novamente. Suponha que obtém-se :math:`y^{(5)} = 1111`.
         Verifica-se se :math:`y^{(5)} \perp c''` ou não. Caso não
         fosse, concluir-se-ia que haveria mais uma equação independente
         e o sistema só teria solução :math:`c'=0`. Não é esse o caso
         aqui, pois

         .. math:: y^{(5)} \cdot c'' = 1111 \cdot 0110 = 0 \oplus 1 \oplus 1 \oplus 0 = 0 \implies y^{(5)} \perp c'' \ .

         Poderia concluir-se, nessa etapa, que :math:`f` é 2-para-1 com
         probabilidade de erro igual à probabilidade de se sortear
         aleatoriamente 5 vetores e todos cairem no mesmo subespaço
         vetorial de dimensão 3:

         .. math:: \varepsilon_5 \lesssim 1 \cdot 1 \cdot 1 \cdot \frac{2^3}{2^4}\cdot \frac{2^3}{2^4} = \frac{1}{4} \ .

         Terminando o algoritmo na iteração 5, tem-se que :math:`f` é
         2-para-1 e que o período é :math:`c = 0110`.

         A título de curiosidade, a :math:`f` utilizada neste exemplo é
         disposta na tabela abaixo, em que :math:`A, B, C, D, E, F, G` e
         :math:`H` denotam 8 palavras distintas de 4 bits.

         .. math::

            \begin{array}{c|cccc|c}
                 x    & f(x) & & & x & f(x) \\ \cline{1-2} \cline{5-6}
                 0000  & A & & & 1000  & E \\
                 0001  & B & & & 1001  & F \\
                 0010  & C & & & 1010  & G \\
                 0011  & D & & & 1011  & H \\
                 0100  & C & & & 1100  & G \\
                 0101  & D & & & 1101  & H \\
                 0110  & A & & & 1110  & E \\
                 0111  & B & & & 1111  & F
                \end{array}

         Repare que, de fato, :math:`f` é 2-para-1 com :math:`c = 0110`.

      .. rubric:: Encontrando o valor do período :math:`c` – caso geral
         :name: encontrando-o-valor-do-período-c-caso-geral
         :class: unnumbered

      Repetindo a subrotina :math:`m` vezes, obtém-se os resultados
      :math:`y^{(1)}, \ldots, y^{(m)}` das medidas no registrador 1 e o
      sistema de equações lineares na incógnita
      :math:`c = c_1 c_2 \ldots c_n`:

      .. math::

         \begin{cases}
            \ y^{(1)} \cdot c = 0\\
            \ y^{(2)} \cdot c = 0\\
            \ \phantom{y^{(1)} \cdot c} \vdots \\
            \ y^{(m)} \cdot c = 0
           \end{cases} \tag{s}

      Esse sistema sempre admite a solução :math:`c = 0`. Supõe-se que
      se tenha obtido, após a aplicação da subrotina por um número
      suficiente de vezes, um sistema linear com um número suficiente de
      equações linearmente independentes (ficará mais claro o que
      significaria “suficiente” nesse contexto).

      .. container:: remark

         Para analisar esse sistema, é interessante considerar
         :math:`\mathbb{B}_n` como espaço vetorial sobre os escalares
         :math:`\mathbb{B} = \{ 0,1 \}` e com a soma de vetores dada
         pela XOR bit a bit :math:`\oplus`. É possível verificar que
         esse espaço satisfaz os axiomas de espaço vetorial. Além disso,
         é possível imitar o produto interno em :math:`\mathbb{R}^n` ou
         :math:`\mathbb{C}^n` com a operação
         :math:`r\cdot s = r_1 s_1 \oplus \ldots \oplus r_n s_n`.

         O espaço :math:`\mathbb{B}_n` tem dimensão :math:`n` e contém
         :math:`2^n` vetores. A maioria dos resultados de Álgebra Linear
         se mantém para esse caso, exceto que esses espaços vetoriais
         têm um número finito de vetores e que é possível ter
         :math:`x \neq 0` e :math:`x \cdot x = 0`, de forma que o
         produto :math:`\cdot` não é um produto interno em
         :math:`\mathbb{B}_n`. Os subespaços de :math:`\mathbb{B}_n` têm
         dimensão :math:`2^m, m\leq n`. O subespaço gerado por
         :math:`c \neq 0` é :math:`\{0,c\}` e tem dimensão 1. Se
         :math:`W` é um subespaço, então o conjunto :math:`W^\perp` dos
         vetores perpendiculares a :math:`W` é também um subespaço, e
         vale que :math:`\dim W + \dim W^\perp = \dim \mathbb{B}_n = n`.

         Os livros de Códigos Corretores de Erros (da Computação
         Clássica) costumam denotar :math:`\mathbb{B}` por
         :math:`GF(2)`, chamado *campo de Galois* (*Galois field*) de
         dois elementos. Uma referência contendo um resumo da teoria
         relacionada ao espaço vetorial :math:`GF(2)^n` é
         :raw-latex:`\cite{book:ecc_moon}`, seção 2.4, p.75-80.

      Se :math:`f` for 1-para-1, espera-se que o sistema admita apenas a
      solução trivial :math:`c=0`. Os valores
      :math:`y^{(1)}, \ldots, y^{(m)}` podem ser quaisquer dos
      :math:`2^n` vetores de bits em :math:`\mathbb{B}_n`, por causa da
      equação (p2). Como a dimensão de :math:`\mathbb{B}_n` é :math:`n`,
      há no máximo :math:`n` vetores de bits LI nesse espaço. Isso
      significa que o sistema acima é equivalente a um sistema de
      :math:`n` equações LI, e que só admite a solução trivial
      :math:`c=0`, como esperado.

      Por outro lado, se :math:`f` for 2-para-1, espera-se que o sistema
      tenha duas soluções: :math:`0` e :math:`c\neq 0`. Nesse caso, os
      valores :math:`y^{(1)}, \ldots, y^{(m)}` são, obrigatoriamente,
      perpendiculares ao vetor :math:`c`. Se :math:`W` é o subespaço
      gerado por :math:`c`, tem-se que
      :math:`y^{(1)}, \ldots, y^{(m)} \in W^\perp` e que
      :math:`\dim W = 1` e :math:`\dim W^\perp = n-1`, de forma que
      :math:`\dim W + \dim W^\perp = \dim \mathbb{B}_n`. Assim, há no
      máximo :math:`n-1` vetores LI e perpendiculares a :math:`c`. O
      sistema (s) é equivalente a um sistema de :math:`n-1` equações LI,
      e apresenta uma variável livre :math:`c_j`, que pode assumir os
      valores 0 ou 1, gerando as soluções :math:`0` e :math:`c\neq 0`,
      como era esperado.

      Resumindo, se as :math:`m` repetições da subrotina quântica do
      algoritmo de Simon produzirem um sistema de equações com o máximo
      possível de equações LI, a solução do sistema fornecerá :math:`c`
      e, dependendo se há apenas a solução nula ou se, além dessa, há
      uma solução :math:`c \neq 0`, pode-se distinguir os casos
      “:math:`f` é 1-para-1” ou “:math:`f` é 2-para-1”. O número de
      repetições :math:`m` requerido é proporcional a :math:`n`.

      .. rubric:: Probabilidade de erro
         :name: probabilidade-de-erro
         :class: unnumbered

      Em relação à probabilidade de erro no caso geral, tem-se o
      seguinte. A partir de um número de iterações suficientemente
      grande (da ordem de :math:`n`), a cada nova iteração, ou se
      descobre que :math:`f` é 1-para-1 (resultado cada vez mais
      improvável) ou se escolhe que :math:`f` é 2-para-1 com
      probabilidade de erro:

      .. math:: \varepsilon_m \lesssim \underbrace{1  \ldots  1}_{n-1 \text{ vezes}} \cdot \underbrace{ \frac{2^{n-1}}{2^n} \ldots \frac{2^{n-1}}{2^n}}_{m-(n-1) \text{ vezes}} = \frac{1}{2^{m-n+1}} \ .

      Essa estimativa não é exata, pois está considerando o caso em que
      :math:`n-1` resultados forneceram vetores de bits não-nulos e LI,
      e os outros resultados cairam no subespaço gerado pelos
      :math:`n-1` vetores LI. Poderia ter acontecido de se obter menos
      vetores LI e os outros cairem no subespaço gerado por eles, apesar
      de parecer menos provável. Essa estimativa, contudo, serve para
      dar uma pista quanto à quantidade de iterações do algoritmo
      quântico. Dessa forma, se o número de iterações :math:`m` for da
      ordem de :math:`n`, a probabilidade de erro pode ser feita menor
      que :math:`1/2`.

   .. container:: subsection

      Algoritmo Clássico

      .. rubric:: Algoritmo Clássico Determinístico
         :name: algoritmo-clássico-determinístico-1
         :class: unnumbered

      Um algoritmo clássico para o Problema de Simon consiste em
      escolher entradas :math:`x` em :math:`\mathbb{B}_n`, calcular
      :math:`f(x)` e comparar com os outros valores já obtidos, até que
      se encontre um par de vetores distintos :math:`x_{(1)}` e
      :math:`x_{(2)}` com :math:`f(x_{(1)}) = f(x_{(2)})` ou até que se
      possa concluir que :math:`f` é 1-para-1.

      Supondo que seja possível armazenar todas as entradas testadas e
      seus resultados pela aplicação da :math:`f`, na pior das
      hipóteses, deve-se calcular :math:`f` para metade das entradas
      mais uma, isto é, :math:`2^{n}/2 + 1` vezes. Caso :math:`f` seja
      1-para-1, todos os resultados seriam distintos, e caso :math:`f`
      seja 2-para-1, na pior das hipóteses, na tentativa de número
      :math:`2^{n}/2 + 1` será obtido um valor repetido após aplicação
      da função.

      .. rubric:: Algoritmo Clássico Probabilístico
         :name: algoritmo-clássico-probabilístico-1
         :class: unnumbered

      Para melhorar o desempenho do algoritmo determinístico acima,
      pode-se relaxar o desempenho, permitindo-se uma probabilidade de
      erro :math:`\varepsilon` na escolha. Sorteia-se aleatoriamente
      :math:`x` dentre o conjunto de entradas não testadas ainda,
      calcula-se :math:`f(x)` e compara-se o valor obtido com o
      fornecido pelas entradas já testadas. Repete-se por um número
      suficiente de tentativas ou até algum par ser encontrado; se
      nenhum par foi encontrado, decide-se por “:math:`f` é 1-para-1” e
      se for encontrado, decide-se por “:math:`f` é 2-para-1” e
      utiliza-se o par :math:`x_{(1)}, x_{(2)}` encontrado para calcular
      :math:`c = x_{(1)}\oplus x_{(2)}`.

      Após :math:`m` iterações, o número de pares já testados é
      :math:`N_{\text{ob}}`, em que

      .. math:: N_{\text{ob}} = \binom{m}{2} = \frac{m(m-1)}{2} \ ,

      isto é, o número de pares que se pode formar dentre :math:`m`
      elementos distintos sem importar a ordem em que se encontram no
      par.

      Caso :math:`f` seja 2-para-1, o número de pares desejado (ou seja,
      que resultam no mesmo valor após aplicação de :math:`f`) é
      :math:`N_{\text{des}}` dado por

      .. math:: N_{\text{des}} = \frac{2^n}{2} \ .

      A probabilidade de pelo menos um par desejado ter sido obtido após
      :math:`k` iterações é

      .. math:: p_m = \frac{N_{\text{ob}}}{N_{\text{des}}} = \frac{m(m-1)}{2^n} \ .

      Caso nenhum par desejado tenha sido encontrado, opta-se por
      “:math:`f` é 1-para-1” com probabilidade de erro dada por

      .. math:: \varepsilon_m = 1 - p_m = 1 - \frac{m(m-1)}{2^n} \ .

      Para que a probabilidade de erro seja :math:`\varepsilon < 1/2`,
      deve-se ter

      .. math:: \varepsilon_m < \frac{1}{2} \implies  \frac{m(m-1)}{2^n} > \frac{1}{2} \implies m^2 - m - 2^{n-1} > 0 \ .

      Resolvendo para :math:`m > 0`, deve-se ter

      .. math:: m > \frac{1 + \sqrt{1 + 2^{n+1}}}{2} \ ,

      logo :math:`m` deve ser da ordem de :math:`2^{n/2}` iterações.

   .. container:: subsection

      Comparação de Desempenho

      O desempenho dos algoritmos clássico determinístico, clássico
      probabilístico e quântico são resumidos na tabela abaixo.

      .. table:: Comparação de desempenho entre os algoritmos quântico,
      clássico determinístico e clássico probabilístico (com
      probabilidade de erro :math:`<50\%`) para o Problema de Simon.

         ============ ======================================
         Algoritmo    Desempenho (# aplicações de :math:`f`)
         ============ ======================================
         Class. Det.  da ordem de :math:`2^n/2` aplicações
         Class. Prob. da ordem de :math:`2^{n/2}` aplicações
         Quântico     da ordem de :math:`n` aplicações
         ============ ======================================

      Da mesma forma como no problema de Deutsch-Jozsa, essa comparação
      tem limitações, mas serve como laboratório para testar em que
      situações a Computação Quântica pode trazer vantagem computacional
      em relação à Computação Clássica. Em particular, esse é um exemplo
      em que o algoritmo quântico apresenta ganho exponencial em
      desempenho em relação aos algoritmos clássicos existentes.

.. container:: section

   Algoritmo de Busca de Grover

   Esta seção aborda o Algoritmo de Grover, um algoritmo quântico que
   permite encontrar um elemento em uma lista não ordenada. Uma
   descrição mais rigorosa desse problema, bem como as alternativas
   clássicas para o mesmo são apresentadas neste texto. As referências
   utilizadas nesta parte são o livro
   :raw-latex:`\cite{book:icq_portugal}`, capítulo 3, e as videoaulas
   U2.6 da subunidade SU4 do curso
   :raw-latex:`\cite{videolecture:qisI_p2}`.

   .. container:: subsection

      Problema de Grover

      O problema de Grover consiste em encontrar um elemento específico
      em uma lista de :math:`2^n` itens. A lista é formada por palavras
      binárias de :math:`n` dígitos. Há uma função booleana
      :math:`f\colon \{ 0,1 \}^n \to \{ 0,1 \}` que só sinaliza ’1’ para
      uma entrada :math:`x_0` em particular, que pode ser desconhecida.
      Em outras palavras, a função :math:`f` pode ser descrita por

      .. math::

         f(x) = \begin{cases}
                      0 \ , \ x \neq x_0 \\
                      1 \ , \  x = x_0 \ .
                     \end{cases}

      A maneira de saber se o item :math:`x` considerado corresponde ao
      desejado é por meio da aplicação de :math:`f`. Essa função poderia
      ser chamada de “teste”, e caso se queira saber se o item :math:`x`
      da lista é o desejado (em outras palavras, se :math:`x = x_0`),
      deve-se fazer o teste em :math:`x` e ver se o teste resulta em ’1’
      (item desejado foi encontrado) ou ’0’ (o item testado não é o
      desejado).

      .. container:: problem*

         de Grover Encontrar a única entrada
         :math:`x_0 \in  \{ 0,1 \}^n` tal que o resultado do teste
         :math:`f` sinaliza ’1’. Ou seja, encontrar único :math:`x_0`
         com

         .. math:: f(x_0) = 1 \ .

      Em seguida, um algoritmo quântico é apresentado para resolver o
      problema de forma mais eficiente do que seria possível
      classicamente.

   .. container:: subsection

      Algoritmo de Grover

      O algoritmo de Grover é um algoritmo quântico para resolução do
      problema de Grover, que consiste em encontrar o elemento em uma
      lista por meio de um teste :math:`f`, e o elemento desejado é o
      único :math:`x_0` que faz com que o teste sinalize 1. O algoritmo
      quântico consiste em aplicar uma subrotina quântica, o operador de
      Grover, por um número de vezes da ordem de :math:`\sqrt{N}`, em
      que :math:`N=2^n` é o número de itens na lista, indexados por
      :math:`n` bits.

      .. rubric:: Algoritmo de Grover
         :name: algoritmo-de-grover
         :class: unnumbered

      | **Entrada:** :math:`O_\text{F}(f) = O`   (oráculo de fase
        associado à função booleana :math:`f`)
      | **Procedimento:**
      | :math:`\begin{array}{lll}
           \text{etapa 0:} & \ket{0}^{\otimes n} & \text{\small{preparação do estado inicial}} \\
           \text{etapa 1:} & H^{\otimes n}\ket{0}^{\otimes n} & \text{\small{superposição dos estados na base computacional}} \\
           \text{etapa 2:} & G & \text{\small{aplicação do operador de Grover}} \\
           \text{etapa 3:} & \multicolumn{2}{l}{\text{\small{repetir etapa 2  por $k$ vezes, com}}}  \\
                           & \multicolumn{2}{l}{k = \dfrac{\acos(\frac{1}{\sqrt{N}})}{\acos(\frac{N-2}{N})} \ , \ \ N = 2^n \ .}
           \end{array}`
      | **Operador de Grover :math:`G`:**
      | :math:`\begin{array}{lll}
           \text{etapa 1:} & O_\text{F}\ket{+}^{\otimes n} & \text{\small{aplicação de $f$ (oráculo de fase)}}  \\ 
           \text{etapa 2:} & H^{\otimes n} & \\
           \text{etapa 3:} &  2\ketbra{0}{0} - I &  \\
           \text{etapa 4:} & H^{\otimes n} &  \\
          \end{array}`
      | **Saída:** Leitura do registrador fornece o item :math:`x_0`
        buscado, com probabilidade de acerto

        .. math:: P_a >\frac{N-1}{N} , \ \ N = 2^n  \ .

      .. rubric:: Circuito
         :name: circuito-2
         :class: unnumbered

      .. container:: float
         :name: cap5:circuito_grover

         Notação compacta:

         .. math::

            \Qcircuit @C=5pt @R=15pt @!R 
                {
               \lstick{\ket{0}^{\otimes n}} &  \ustick{\ n}\qw  & {/} \qw & \qw & \gate{H^{\otimes n}} & \gate{G} & \gate{G} & \qw & \push{\cdots\ \, } & \gate{G} 
               \gategroup{1}{6}{1}{10}{1.0em}{_\}}  & \qw & \qw &\meter \\
               & & & & & & & \ustick{k \text{ vezes}} & & & & & }

         .. math::

            \Qcircuit @C=5pt @R=15pt @!R 
                {
                &  \ustick{\ n}\qw  & {/} \qw & \qw & \gate{G} & \qw & \qw & \qw & & = & & & \qw & \qw  & \gate{O_\text{F}(f)} & \gate{H^{\otimes n}} & \gate{2\ketbra{0}{0} - I} & \gate{H^{\otimes n}} & \qw & \qw  }

      .. rubric:: Notação auxiliar
         :name: notação-auxiliar
         :class: unnumbered

      Para facilitar, lista-se abaixo a notação utilizada nesse
      algoritmo:

      .. math:: \ket{0} = \ket{0 \ldots 0} = \ket{0}^{\otimes n}

      .. math:: \ket{\psi} := \ket{+}^{\otimes n}

      .. math:: \mathbb{B}_n : \text{ conjunto de todas as palavras de $n$ bits}

      .. math::

         N := 2^n \ \ \begin{cases}
                             \text{$n$: número de qubits de cada item da lista} \\
                             \text{$N$: número de itens na lista } 
                            \end{cases}

      .. math:: \ket{\alpha} := \sum_{\scriptsize \begin{array}{c} x \in \mathbb{B}_n \\ x \neq x_0 \end{array}} \frac{\ket{x}}{\sqrt{N-1}}

      .. math:: \ket{\beta} := \ket{x_0} \text{: item desejado na lista}

      .. math:: S := \text{span}_\mathbb{R} \{ \ket{\alpha} , \ket{\beta} \} \text{: espaço vetorial gerado por $\ket{\alpha}$, $\ket{\beta}$ com escalares reais}

      .. rubric:: Contas auxiliares
         :name: contas-auxiliares-1
         :class: unnumbered

      Vale que:

      .. math::

         \begin{split} 
                \ket{\psi} 
                &= \ket{+}^{\otimes n} \\
                &= \sum_{x \in \mathbb{B}_n} \frac{\ket{x}}{\sqrt{N}} \\
                &= \frac{\sqrt{N-1}}{\sqrt{N}} \sum_{\scriptsize \begin{array}{c} x \in \mathbb{B}_n \\ x \neq x_0 \end{array}} \frac{\ket{x}}{\sqrt{N-1}} \ \   + \  \  \frac{\ket{x_0}}{\sqrt{N}} \\
                &= \frac{\sqrt{N-1}}{\sqrt{N}} \ket{\alpha} + \frac{1}{\sqrt{N}} \ket{\beta}
               \end{split} \tag{$\psi$}

      A aplicação de :math:`O_\text{F}` em :math:`\ket{\alpha}` e
      :math:`\ket{\beta}` fica:

      .. math::

         \begin{split}
             O_\text{F} \ket{\alpha} 
             &=  O_\text{F}\sum_{\scriptsize \begin{array}{c} x \in \mathbb{B}_n \\ x \neq x_0 \end{array}} \frac{\ket{x}}{\sqrt{N-1}}  \\
             &= \sum_{\scriptsize \begin{array}{c} x \in \mathbb{B}_n \\ x \neq x_0 \end{array}}\frac{1}{\sqrt{N-1}} O_\text{F}\ket{x} \\
             &= \sum_{\scriptsize \begin{array}{c} x \in \mathbb{B}_n \\ x \neq x_0 \end{array}}\frac{1}{\sqrt{N-1}} \ket{x} \\
             &= \ket{\alpha}  \ , 
            \end{split} \tag{$\alpha$}

      .. math::

         \begin{split}
                 O_\text{F} \ket{\beta} 
                 &=  O_\text{F} \ket{x_0}  \\
                 &=  - \ket{x_0} \\
                 &= - \ket{\beta} \ .
               \end{split} \tag{$\beta$}

      O operador :math:`G` pode ser escrito como:

      .. math::

         \begin{split}
                 G 
                 &= H^{\otimes n} (2\ketbra{0}{0} - I) H^{\otimes n} O_\text{F} \\
                 &= \big(2H^{\otimes n}\ketbra{0}{0}H^{\otimes n} - H^{\otimes n}I H^{\otimes n}  \big)O_\text{F} \\
                 &= \big(2H^{\otimes n}\ketbra{0}{0}(H^{\otimes n})^\dagger - H^{\otimes n} H^{\otimes n}  \big)O_\text{F} \\
                 &= (2\ketbra{\psi}{\psi} - I) O_\text{F} \ ,
                \end{split} \tag{$G$}

      em que foi definido :math:`\ket{\psi} = \ket{+}^{\otimes n}`.

      .. rubric:: Primeira aplicação do operador :math:`G`
         :name: primeira-aplicação-do-operador-g
         :class: unnumbered

      Antes de aplicar o operador :math:`G` pela primeira vez,
      prepara-se o estado inicial fazendo uma superposição com pesos
      iguais em cada qubit:

      .. math::

         \begin{split}
             \ket{\psi_0} 
             &= H^{\otimes n} \ket{0}^{\otimes n} \\
             &= \ket{+}^{\otimes n} \\
             &= \ket{\psi} \\
             &=  \frac{\sqrt{N-1}}{\sqrt{N}} \ket{\alpha} + \frac{1}{\sqrt{N}} \ket{\beta}  \ \ \text{usando ($\psi$)} \ . \\
            \end{split}

      Nota-se que :math:`\ket{\psi_0}` pertence ao subespaço vetorial
      :math:`S` gerado por :math:`\ket{\alpha},\ket{\beta}` e com
      escalares reais. Isso permitirá uma interpretação geométrica muito
      útil para o entendimento do algoritmo.

      Na etapa 1 de :math:`G`, aplica-se o oráculo de fase ao estado
      inicial :math:`\ket{\psi}`:

      .. math::

         \begin{split}
                \ket{\psi_1} 
                &= O_\text{F} \ket{\psi_0} \\
                &= O_\text{F} \left( \frac{\sqrt{N-1}}{\sqrt{N}} \ket{\alpha} + \frac{1}{\sqrt{N}} \ket{\beta}  \right) \ \ \text{usando ($\psi$)} \\
                &= \frac{\sqrt{N-1}}{\sqrt{N}} O_\text{F} \ket{\alpha} + \frac{1}{\sqrt{N}} O_\text{F} \ket{\beta} \\
                &= \frac{\sqrt{N-1}}{\sqrt{N}} \ket{\alpha} - \frac{1}{\sqrt{N}} \ket{\beta}  \ \ \text{usando ($\alpha$) e ($\beta$)}
               \end{split}

      O vetor :math:`\ket{\psi_1}` continua no espaço :math:`S`, e a
      aplicação do oráculo de fase pode ser visualizada como uma
      reflexão em torno do eixo :math:`\ket{\alpha}`.

      .. container:: float

      As etapas 2, 3 e 4 de :math:`G` redundam em aplicar
      :math:`2 \ketbra{\psi}{\psi} - I` em :math:`\ket{\psi_1}`, conforme
      equação (:math:`G`). E a aplicação desse operador pode ser vista
      geometricamente de acordo com a figura abaixo.

      .. math::

         \begin{split}
                \ket{\psi_2} 
                &= (2 \ketbra{\psi}{\psi} - I) \ket{\psi_1} \\
                &= 2 \ketbra{\psi}{\psi}\ket{\psi_1} - \ket{\psi_1}
               \end{split}

      .. container:: float

      Dessa forma, a primeira aplicação do operador :math:`G` fornece o
      seguinte.

      .. container:: float
         :name: cap5:fig_aplicacao_G_em_psi

      .. rubric:: Aplicações subsequentes do operador :math:`G`
         :name: aplicações-subsequentes-do-operador-g
         :class: unnumbered

      As aplicações sucessivas do operador de Grover têm o mesmo efeito
      descrito anteriormente. Cada aplicação de :math:`G` corresponde a
      uma rotação no sentido anti-horário. A figura a seguir ilustra a
      :math:`l`-ésima aplicação de :math:`G`.

      .. container:: float

      O operador :math:`G` é aplicado por :math:`k` vezes até que o
      vetor resultante esteja o mais próximo possível do eixo
      :math:`\ket{\beta}`.

      .. container:: float

      No livro :raw-latex:`\cite{book:icq_portugal}`, seção 3.3,
      demonstra-se algebricamente que uma aplicação de :math:`G` produz
      uma rotação em sentido horário de um mesmo ângulo :math:`\theta`.

      .. rubric:: Número necessário de aplicações de :math:`G`
         :name: número-necessário-de-aplicações-de-g
         :class: unnumbered

      O número de aplicações :math:`k` necessário é dado por

      .. math:: k \theta + \frac{\theta}{2} = \frac{\pi}{2} \implies k = \frac{\pi - \theta}{2\theta} \ ,

      em que se arredonda :math:`k` para o inteiro mais próximo.

      O ângulo :math:`\theta` (em radianos) é o ângulo que
      :math:`G\ket{\psi}` faz com :math:`\ket{\psi}`. Pode ser obtido
      por (ver figura `1.7 <#cap5:fig_aplicacao_G_em_psi>`__):

      .. math::

         \begin{split}
               \cos \frac{\theta}{2} 
               &= \braket{\beta}{\psi} = \frac{\sqrt{N-1}}{\sqrt{N}} \qquad \text{usando ($\psi$)} \\
               \sin \frac{\theta}{2} 
               &= \sqrt{1 - \abs{\braket{\beta}{\psi}}^2} = \frac{1}{\sqrt{N}} 
              \end{split}

      Fazendo-se a seguinte manipulação algébrica, pode-se encontrar
      outra expressão equivalente para o ângulo.

      .. math::

         \begin{split} 
                \ket{\psi_1} 
            &=  \frac{\sqrt{N-1}}{\sqrt{N}} \ket{\alpha} - \frac{1}{\sqrt{N}} \ket{\beta}  \\
            &=  \frac{\sqrt{N-1}}{\sqrt{N}} \ket{\alpha} + \frac{1}{\sqrt{N}} \ket{\beta} -  \frac{2}{\sqrt{N}} \ket{\beta} \\
            &= \ket{\psi} - \frac{2}{\sqrt{N}} \ket{\beta}   \qquad \text{usando ($\psi$)} \phantom{xxxxxxxxxxxxxxxxx}
               \end{split}

      .. math::

         \begin{split}
                \ket{\psi_2} 
                &= G \ket{\psi} \\
                &= \big( 2 \ketbra{\psi}{\psi} - I \big)\ket{\psi_1} \\
                &= \big( 2 \ketbra{\psi}{\psi} - I \big)\left( \ket{\psi} - \frac{2}{\sqrt{N}}\ket{\beta} \right) \\
                &= 2\ket{\psi}\braket{\psi}{\psi} - \ket{\psi} - \frac{4}{\sqrt{N}}\ket{\psi}\braket{\psi}{\beta} + \frac{2}{\sqrt{N}}\ket{\beta} \\
                &= 2\ket{\psi} - \ket{\psi} - \frac{4}{\sqrt{N}} \frac{1}{\sqrt{N}}\ket{\psi} + \frac{2}{\sqrt{N}}\ket{\beta}  \qquad \text{usando ($\psi$)} \\
                &= \frac{N-4}{N} \ket{\psi} + \frac{2}{\sqrt{N}}\ket{\beta}  \\
          %      &= \frac{N-4}{N} \left( \frac{\sqrt{N-1}}{\sqrt{N}} \ket{\alpha} + \frac{1}{\sqrt{N}} \ket{\beta} \right)  + \frac{2}{\sqrt{N}}\ket{\beta} \\
          %      &=  \frac{(N-4)\sqrt{N-1}}{N\sqrt{N}}\ket{\alpha} + \frac{N-2}{N\sqrt{N}} \ket{\beta}
               \end{split}

      .. math::

         \begin{split}
                \cos \theta 
                &= \braket{\psi}{G\psi} \\
                &= \bra{\psi}  \left(\frac{N-4}{N} \ket{\psi} + \frac{2}{\sqrt{N}}\ket{\beta} \right) \\
            %    & \qquad \qquad \qquad \text{usando ($\ast$)} \\
                &= \frac{N-4}{N} \braket{\psi}{\psi} + \frac{2}{\sqrt{N}}\braket{\psi}{\beta} \\
                &= \frac{N-4}{N} + \frac{2}{\sqrt{N}}\frac{1}{\sqrt{N}} \qquad \text{usando ($\psi$)} \\
                &= \frac{N-2}{N} \ . % \phantom{xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxi}
               \end{split}

      Portanto, pode-se escrever

      .. math:: \theta = 2\acos(\frac{\sqrt{N-1}}{\sqrt{N}}) = 2\asin(\frac{1}{\sqrt{N}}) = \acos(\frac{N-2}{N}) \ ,

      e o valor de :math:`k` é dado por

      .. math:: k = \frac{\pi - \theta}{2\theta} = \frac{\pi - \acos(\frac{N-2}{N})}{2\acos(\frac{N-2}{N})}

      ou por

      .. math:: k = \frac{\frac{\pi}{2} - \frac{\theta}{2}}{\theta} = \frac{\frac{\pi}{2} - \asin(\frac{1}{\sqrt{N}})}{\acos(\frac{N-2}{N}) } = \frac{\acos(\frac{1}{\sqrt{N}})}{\acos(\frac{N-2}{N})} \ .

      É possível verificar que o número :math:`k` é da ordem de
      :math:`\sqrt{N}`. De acordo com
      :raw-latex:`\cite{book:icq_portugal}`, p.56, tem-se

      .. math:: \lim_{N\to +\infty} k = +\infty

      .. math:: \lim_{N\to +\infty} \frac{k}{\sqrt{N}} = \frac{\pi}{4}

      .. math:: \lim_{N\to +\infty} \frac{k}{N} = 0 \ .

      .. rubric:: Probabilidade de acerto
         :name: probabilidade-de-acerto
         :class: unnumbered

      Ao aplicar a subrotina :math:`G` por um número :math:`k` de vezes
      especificado anteriormente, obtém-se o estado final o mais próximo
      possível de :math:`\ket{\beta} = \ket{x_0}`, o item desejado. A
      projeção na direção desse vetor permite encontrar a probabilidade
      de acerto do algoritmo. O estado final :math:`G^k \ket{\psi}` faz
      um ângulo com o estado :math:`\ket{x_0}` menor que
      :math:`\theta/2`, pois se fosse maior ou igual, seria possível
      aplicar novamente o operador :math:`G` para reduzi-lo. Portanto, o
      ângulo :math:`\cos \theta\big(\ket{x_0},G^k\ket{\psi}\big)` entre
      o estado desejado e o estado final é menor que :math:`\theta/2`, o
      que significa que seu cosseno é maior que :math:`\cos (\theta/2)`.

      .. container:: float

      Com isso, tem-se a probabilidade de acerto estimada em

      .. math::

         \begin{split}
                 P_a 
                 &= \norm{\ketbra{x_0}{x_0}G^k \ket{\psi}}^2 \\
                 &= \abs{\braket{x_0}{G^k\ket{\psi}}}^2  \\
                 &= \abs{\cos \theta\big(\ket{x_0},G^k\ket{\psi}\big)}^2 \\
                 &> \abs{\cos \theta/2}^2 \\
                 &= \left(\frac{\sqrt{N-1}}{\sqrt{N}}\right)^2 \\
                 &= \frac{N-1}{N} \ , \ \ N = 2^n \ .
               \end{split}

      .. rubric:: Generalização
         :name: generalização
         :class: unnumbered

      O algoritmo de Grover também funciona para o caso em que há mais
      de um elemento marcado, isto é, há :math:`x_0`, :math:`x_1`,
      :math:`\ldots`, :math:`x_{m-1}` elementos tais que
      :math:`f(x_0) = f(x_1) = \ldots = f(x_{m-1})` e os demais valores
      anulam :math:`f`. É possível adaptar a análise do algoritmo para
      esse caso. A interpretação geométrica continua valendo, mas desta
      vez, tem-se

      .. math:: \ket{\alpha} = \frac{1}{\sqrt{N-m}} \sum_{\scriptsize \begin{array}{c} x \in \mathbb{B}_n \\ f(x) = 0 \end{array} } \ket{x}

      .. math:: \ket{\beta} = \frac{1}{\sqrt{m}} \sum_{\scriptsize \begin{array}{c} x \in \mathbb{B}_n \\ f(x) = 1 \end{array} } \ket{x} \ .

   .. container:: subsection

      Algoritmo Clássico

      .. rubric:: Algoritmo Clássico Determinístico
         :name: algoritmo-clássico-determinístico-2
         :class: unnumbered

      Classicamente, se o teste é dado como uma caixa preta, em que não
      se sabe a estrutura interna de :math:`f`, a maneira de se
      encontrar :math:`x_0` é por busca exaustiva. Para garantir que o
      item :math:`x_0` seja encontrado, deve-se, no pior caso, olhar
      todas as :math:`N = 2^n` entradas possíveis. Portanto são
      necessários :math:`N` aplicações do teste :math:`f`.

      .. rubric:: Algoritmo Clássico Probabilístico
         :name: algoritmo-clássico-probabilístico-2
         :class: unnumbered

      Considere um algoritmo que sorteie aleatoriamente as entradas a
      serem testadas (e sem repetir entradas). Após o teste de :math:`k`
      entradas, a probabilidade de que se tenha localizado o item
      desejado :math:`x_0` é de

      .. math:: P = \frac{k}{N} \ .

      Para que haja probabilidade de encontrar a resposta seja maior que
      :math:`1/2`, deve-se testar pelo menos por um número de vezes

      .. math:: k > \frac{N}{2} \ .

      Desse modo, ainda deve-se aplicar o teste :math:`f` da ordem de
      :math:`N` vezes.

   .. container:: subsection

      Comparação de Desempenho

      O desempenho dos algoritmos são comparados na tabela abaixo.

      .. table:: Comparação de desempenho entre os algoritmos quântico,
      clássico determinístico e clássico probabilístico (com
      probabilidade de erro :math:`<50\%`) para o Problema de Grover. No
      algoritmo de Grover, o número de aplicações de :math:`f` coincide
      com o número de aplicações da subrotina :math:`G`.

         ============ =================================================
         Algoritmo    Desempenho (# aplicações de :math:`f`)
         ============ =================================================
         Class. Det.  da ordem de :math:`N = 2^{n}` aplicações
         Class. Prob. da ordem de :math:`N/2 = 2^{n}/2` aplicações
         Quântico     da ordem de :math:`\sqrt{N} = 2^{n/2}` aplicações
         ============ =================================================

      Dessa forma, o algoritmo de Grover apresentaria ganho quadrático
      de desempenho em relação aos algoritmos clássicos, se a aplicação
      de :math:`f` nos casos clássico e quântico forem equivalentes em
      termos de custos computacionais.

.. container:: section

   Algoritmo de Bernstein-Vazirani

   Há duas versões do algoritmo de Bernstein-Vazirani, ambas formuladas
   para resolver o problema de mesmo nome. Este problema, como os outros
   problemas abordados, não tem perspectivas de aplicações, no entanto,
   ajuda a entender em que situações a computação quântica pode
   apresentar vantagens.

   Neste texto, as duas versões do algoritmo são denominadas “versão
   XOR” e “versão fase”. Duas referências úteis para esses algoritmos
   são :raw-latex:`\cite{site:lpn_ibmqx}` (XOR) e
   :raw-latex:`\cite{book:lecture_notes_scott_aaronson}` (fase). Ambas
   apresentam um ganho exponencial em relação à computação possível
   classicamente, com a versão fase mais eficiente que a versão XOR.

   .. container:: subsection

      Problema de Bernstein-Vazirani

      O objeto desta seção também é um problema de caixa preta, como nos
      outros algoritmos apresentados até então. Portanto é fornecida uma
      função booleana :math:`f`, com determinada propriedade; neste
      caso, :math:`f` é da forma :math:`f(x) = a \cdot x`, com :math:`x`
      e :math:`a` vetores de bits, e o produto corresponde àquele
      análogo a um produto interno apresentado na observação
      `[cap5:rmk_notacao_analoga_a_produto_interno_de_vetores_de_bits] <#cap5:rmk_notacao_analoga_a_produto_interno_de_vetores_de_bits>`__
      e reforçado a seguir. Neste problema, não se conhece a estrutura
      interna de :math:`f`, ou seja, :math:`a` é uma incógnita e só se
      conhece que a função :math:`f` admite um :math:`a` e que pode ser
      escrita como :math:`f(x) = a \cdot x`. A demanda do problema é
      encontrar :math:`a` com o menor número possível de aplicações de
      :math:`f`.

      .. container:: problem*

         de Bernstein-Vazirani Seja
         :math:`f \colon \{ 0,1 \}^n \to \{0,1\}` uma função booleana da
         forma

         .. math:: f(x_1 \ldots x_n) = a_1 \cdot x_1 \oplus \ldots \oplus a_n \cdot x_n \ ,

         que também poderia ser denotada [6]_ por

         .. math:: f(x) = a \cdot x \ .

         A função :math:`f` é dada por uma caixa preta, sua estrutura
         interna não é conhecida. O problema pede para encontrar o vetor
         de bits :math:`a = a_1 \ldots a_n`.

      Os algoritmos quânticos, bem como o procedimento clássico, são
      apresentados a seguir.

   .. container:: subsection

      Algoritmo de Bernstein-Vazirani (versão XOR)

      O algoritmo quântico apresentado aqui conta com :math:`f`
      representado como um oráculo XOR. Uma referência para o algoritmo
      a ser apresentado é :raw-latex:`\cite{site:lpn_ibmqx}`. Apesar de
      menos eficiente que a versão fase, esse algoritmo é abordado para
      ilustrar a técnica de *pós-seleção*, presente em outros algoritmos
      quânticos ainda não apresentados, como o algoritmo (quântico) HHL
      para resolução de sistemas lineares
      :raw-latex:`\cite{article:hhl_algorithm_for_lin_sys_of_eq}`.

      .. rubric:: Algoritmo de Bernstein-Vazirani (versão XOR)
         :name: algoritmo-de-bernstein-vazirani-versão-xor
         :class: unnumbered

      | **Entrada:** :math:`O_\text{XOR}(f)`   (oráculo XOR associado à
        função booleana :math:`f`)
      | **Procedimento:**
      | :math:`\begin{array}{lll}
           \text{etapa 0:} & \ket{0}^{\otimes n}\ket{0} & \text{\small{preparação do estado inicial}} \\
           \text{etapa 1:} & \sum_{x} \ket{x}\ket{0} & \text{\small{superposição de estados com $H^{\otimes n}$ no reg.1}} \\
           \text{etapa 2:} & \sum_{x} \ket{x}\ket{f(x)} & \text{\small{aplicação de $f$ (oráculo XOR)}} \\
           \text{etapa 3:} & \sum_{x} \ket{x}H\ket{a \cdot x} & \text{\small{aplicação de $H$ no reg.2}} \\
           \text{etapa 4:} & \ket{0}\ket{0} + \ket{a}\ket{1}& \text{\small{aplicação de $H^{\otimes n}$ no reg.1}} \\
          \end{array}`
      | **Saída:** Mede-se o último qubit (reg.2). Caso o resultado seja
        1, o reg.1 carregará o valor de :math:`a`. Se o resultado da
        medição no reg.2 for 0, o algoritmo deve ser repetido até que se
        obtenha 1 nesse registrador. Isto significa fazer uma
        pós-seleção no reg.2

      .. rubric:: Circuito
         :name: circuito-3
         :class: unnumbered

      .. container:: float
         :name: cap5:circuito_bernstein-vazirani_xor

         | Notação compacta:

           .. math::

              \Qcircuit @C=5pt @R=10pt @!R 
                  {
                 \lstick{\ket{0}^{\otimes n}} &  \ustick{\ n}\qw  & {/} \qw & \qw & \gate{H^{\otimes n}} & \ctrl{1}               & \gate{H^{\otimes n}} & \qw & \qw &\meter \\
                 \lstick{\ket{0}\quad} &  \qw              &       \qw & \qw & \gate{H}             & \gate{O_\text{XOR}(f)} & \gate{H}             & \qw & \qw &\meter}
         | Notação expandida:

           .. math::

              \Qcircuit @C=5pt @R=4pt %@!R 
                  {
                  & & & \lstick{\ket{0}}  & \qw \gategroup{1}{1}{4}{1}{1.2em}{\{}& \qw & \gate{H} & \ctrl{1}                       & \gate{H} & \qw & \qw &\meter \\
                   & & & \lstick{\ket{0}}  & \qw & \qw & \gate{H} & \ctrl{2}                       & \gate{H} & \qw & \qw &\meter \\
                      \ustick{\text{reg.1}\quad \quad \quad \quad \ \ }  & & & &\vdots&        &          &                                &          &     &\vdots&   \\
                   & & & \lstick{\ket{0}}  & \qw & \qw & \gate{H} & \ctrl{2}                       & \gate{H} & \qw & \qw &\meter  \\
                            & & &          &     &     &          &                                &          &     &     &       \\
                  \lstick{\text{reg.2} \, \ \ }& \text{ \Large \{ \ \quad} & &  \lstick{\ket{0}}  & \qw & \qw & \qw      & \gate{O_\text{XOR}(f)} & \gate{H} & \qw & \qw &\meter  
                 }

      .. rubric:: Análise detalhada do algoritmo
         :name: análise-detalhada-do-algoritmo-1
         :class: unnumbered

      Nesta explanação, utiliza-se diversas vezes a proposição
      `[cap5:prop_expressao_para_hadamard_aplicada_a_vetor_na_base_comp] <#cap5:prop_expressao_para_hadamard_aplicada_a_vetor_na_base_comp>`__,
      que fornece a expressão

      .. math:: H^{\otimes n} \ket{x} = \frac{1}{\sqrt{2^n}} \sum_{y\in\mathbb{B}_n} (-1)^{x\cdot y} \ket{y} \ ,

      com :math:`\ket{x}` vetor da base computacional. Utiliza-se também
      a notação :math:`\ket{\text{reg.1}}\ket{\text{reg.2}}` para
      distinguir os dois conjuntos de qubits.

      A primeira etapa do algoritmo resulta no estado

      .. math::

         \begin{split}
          \ket{\psi_1} 
          &= \big( H^{\otimes n} \ket{0} \big) \ket{0}  \\
          &=  \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}^n} \ket{x}\ket{0} \ .
             \end{split}

      Obtém-se, ao aplicar :math:`f`,

      .. math::

         \begin{split}
               \ket{\psi_2} 
               &= \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}^n} O_\text{XOR}(f)\ket{x}\ket{0} \\
               &= \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}^n} \ket{x}\ket{f(x)} \\
               &= \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}^n} \ket{x}\ket{a\cdot x} \ .
              \end{split}

      Na etapa 3, aplica-se a porta Hadamard ao registrador 2:

      .. math::

         \begin{split}
               \ket{\psi_3} 
               &= \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}^n} \ket{x}\big( H\ket{a\cdot x} \big) \\
               &=  \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}^n} \ket{x} \left( \frac{\ket{0} + (-1)^{a \cdot x} \ket{1}}{\sqrt{2}} \right) \\
               &= \frac{1}{\sqrt{2^n}\sqrt{2}} \sum_{x \in \mathbb{B}^n} \ket{x}\ket{0} + \frac{1}{\sqrt{2^n}\sqrt{2}} \sum_{x \in \mathbb{B}^n} (-1)^{a \cdot x} \ket{x} \ket{1} \\
               &= \frac{1}{\sqrt{2}}  \big( H^{\otimes n} \ket{0} \big) \ket{0}    +   \frac{1}{\sqrt{2}} \big( H^{\otimes n} \ket{a} \big) \ket{1} \ . 
               \end{split}

      Assim, ao aplicar-se a as portas Hadamard ao registrador 1, na
      etapa 4, obtém-se que

      .. math::

         \begin{split}
               \ket{\psi_4} 
               &= \frac{1}{\sqrt{2}}  \big( H^{\otimes n} \ket{0} \big) \ket{0}    +   \frac{1}{\sqrt{2}} \big( H^{\otimes n} \ket{a} \big) \ket{1} \\
               &= \frac{1}{\sqrt{2}}  \big( H^{\otimes n} H^{\otimes n} \ket{0} \big) \ket{0}    +   \frac{1}{\sqrt{2}} \big(H^{\otimes n} H^{\otimes n} \ket{a} \big) \ket{1} \\
               &=  \frac{1}{\sqrt{2}}  \ket{0} \ket{0}    +   \frac{1}{\sqrt{2}}  \ket{a} \ket{1} \ . 
               \end{split}

      Dessa forma, ao se fazer uma medida nos registradores 1 e 2,
      obtém-se, com probabilidade
      :math:`\abs{\frac{1}{\sqrt{2}}}^2 = \frac{1}{2}` o resultado
      :math:`\ket{a} \ket{1}`. Faz-se uma *pós-seleção* no segundo
      registrador, e quando este resultado for 1, o outro registrador
      carregará consigo a resposta :math:`a` do problema.

   .. container:: subsection

      Algoritmo de Bernstein-Vazirani (versão fase)

      O algoritmo de Bernstein-Vazirani, em sua versão fase, possui
      desempenho ligeiramente melhor que o da versão XOR, e não
      necessita de pós-seleção. O algoritmo a ser apresentado consta na
      referência :raw-latex:`\cite{book:lecture_notes_scott_aaronson}`,
      e resolve o problema objeto desta seção de maneira exata, sem
      probabilidade de falha, com apenas uma aplicação do oráculo de
      fase representando :math:`f`.

      .. rubric:: Algoritmo de Bernstein-Vazirani (versão fase)
         :name: algoritmo-de-bernstein-vazirani-versão-fase
         :class: unnumbered

      | **Entrada:** :math:`O_\text{F}(f)`   (oráculo de fase associado
        à função booleana :math:`f`)
      | **Procedimento:**
      | :math:`\begin{array}{lll}
           \text{etapa 0:} & \ket{0}^{\otimes n} = \ket{0}  & \text{\small{preparação do estado inicial}} \\
           \text{etapa 1:} & H^{\otimes n}  \ket{0} = \sum_{x} \ket{x}  & \text{\small{superposição de estados com $H^{\otimes n}$ }} \\
           \text{etapa 2:} & \sum_{x} (-1)^{a \cdot x} \ket{x} =  H^{\otimes n} \ket{a} & \text{\small{aplicação de $f$ (oráculo de fase)}} \\
           \text{etapa 3:} & H^{\otimes n}H^{\otimes n} \ket{a} = \ket{a} & \text{\small{aplicação de $H$}} \\
          \end{array}`
      | **Saída:** Mede-se o último qubit (reg.2). Caso o resultado seja
        1, o reg.1 carregará o valor de :math:`a`. Se o resultado da
        medição no reg.2 for 0, o algoritmo deve ser repetido até que se
        obtenha 1 nesse registrador.

      .. rubric:: Circuito
         :name: circuito-4
         :class: unnumbered

      .. container:: float
         :name: cap5:circuito_bernstein-vazirani_xor

         | Notação compacta:

           .. math::

              \Qcircuit @C=5pt @R=10pt @!R 
                  {
                 \lstick{\ket{0}^{\otimes n}} &  \ustick{\ n}\qw  & {/} \qw & \qw & \gate{H^{\otimes n}} & \gate{O_{\text{F}}(f)} & \gate{H^{\otimes n}} & \qw & \qw &\meter  
                 }
         | Notação expandida:

           .. math::

              \Qcircuit @C=5pt @R=4pt %@!R 
                  {
                  \lstick{\ket{0}}  & \qw    & \qw & \gate{H} & \multigate{3}{O_\text{F}(f)}       & \gate{H} & \qw & \qw    &\meter \\
                  \lstick{\ket{0}}  & \qw    & \qw & \gate{H} & \ghost{O_\text{F}(f)}              & \gate{H} & \qw & \qw    &\meter \\
                                    & \vdots &     &          & \ghostnoqw{O_\text{F}(f)}          &          &     & \vdots &       \\
                  \lstick{\ket{0}}  & \qw    & \qw & \gate{H} & \ghost{O_\text{F}(f)}              & \gate{H} & \qw & \qw    &\meter 
                  }

      .. rubric:: Análise detalhada do algoritmo
         :name: análise-detalhada-do-algoritmo-2
         :class: unnumbered

      Nesta explanação, utiliza-se diversas vezes a proposição
      `[cap5:prop_expressao_para_hadamard_aplicada_a_vetor_na_base_comp] <#cap5:prop_expressao_para_hadamard_aplicada_a_vetor_na_base_comp>`__,
      que fornece a expressão

      .. math:: H^{\otimes n} \ket{x} = \frac{1}{\sqrt{2^n}} \sum_{y\in\mathbb{B}_n} (-1)^{x\cdot y} \ket{y} \ ,

      com :math:`\ket{x}` vetor da base computacional.

      A primeira etapa do algoritmo resulta em

      .. math::

         \begin{split}
          \ket{\psi_1} 
          &=   H^{\otimes n} \ket{0} =  \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}^n} \ket{x}  \ .
             \end{split}

      Obtém-se, ao aplicar o oráculo de fase, que

      .. math::

         \begin{split}
               \ket{\psi_2} 
               &= \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}^n} O_\text{F}(f)\ket{x}  \\
               &= \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}^n} (-1)^{f(x)} \ket{x} \\
               &= \frac{1}{\sqrt{2^n}} \sum_{x \in \mathbb{B}^n} (-1)^{a\cdot x}\ket{x} \\
               &= H^{\otimes n} \ket{a} \ .
              \end{split}

      Finalmente, aplicam-se as portas Hadamard, o que retorna

      .. math::

         \begin{split}
               \ket{\psi_3} 
               &=  H^{\otimes n}  H^{\otimes n} \ket{a}  =  \ket{a} 
               \end{split}

      Assim, uma medição no registrador fornecerá a incógnita :math:`a`
      do problema.

   .. container:: subsection

      Algoritmo Clássico

      .. rubric:: Algoritmo Clássico Determinístico
         :name: algoritmo-clássico-determinístico-3
         :class: unnumbered

      Na computação clássica, é possível resolver o problema de
      Bernstein-Vazirani aplicando-se a função booleana :math:`f` aos
      :math:`n` vetores de bits [7]_

      .. math:: x = x_1 x_2 \ldots x_n = 100\ldots0 \ , \ \ 010\ldots0 \ , \ \ \ldots \ , \ \ 0 \ldots 01 \ .

      De fato, se :math:`f(x) = a \cdot x`, com :math:`a` vetor de bits
      fixo, mas desconhecido, pode-se descobrir
      :math:`a = a_1 a_2 \ldots a_n` fazendo [8]_

      .. math::

         \begin{array}{llll}
               f(100\ldots0) 
               \!\!\!\! &= \ \ a_1 \cdot 1 \oplus  a_2 \cdot 0 \oplus \ldots \oplus a_n \cdot 0 
               \!\!\!\! &= a_1 \oplus 0 \oplus \ldots \oplus 0 
               \!\!\!\! &= a_1   \\
               f(010\ldots0) 
               \!\!\!\! &= \ \ a_1 \cdot 0 \oplus a_2 \cdot 1  \oplus \ldots \oplus a_n \cdot 0 
               \!\!\!\! &= 0 \oplus a_2 \oplus \ldots \oplus 0 
               \!\!\!\! &= a_2   \\
               \quad \vdots 
               & \qquad \vdots   \\
               f(0\ldots001) 
               \!\!\!\! &= a_1 \cdot 0   \oplus  \ldots \oplus a_{n-1} \cdot 0  \oplus  a_n \cdot 1 
               \!\!\!\! &= 0 \oplus \ldots \oplus 0 \oplus a_n 
               \!\!\!\! &= a_n
              \end{array}

      Com isso, são necessárias :math:`n` aplicações de :math:`f` para
      se descobrir o vetor de bits :math:`a`.

      .. rubric:: Algoritmo Clássico Probabilístico
         :name: algoritmo-clássico-probabilístico-3
         :class: unnumbered

      Parece não ser possível melhorar o algoritmo clássico prescrito
      acima, pois cada aplicação de :math:`f` nos fornece uma equação
      envolvendo :math:`a_1, \ldots , a_n`. Para encontrar todos os
      :math:`a_i`\ s são necessárias :math:`n` equações.

   .. container:: subsection

      Comparação de Desempenho

      Como nas outras seções, apresenta-se uma comparação de desempenho
      na tabela abaixo.

      .. table:: Comparação de desempenho entre os algoritmos quântico,
      clássico determinístico e clássico probabilístico (com
      probabilidade de erro :math:`<50\%`) para o Problema de
      Bernstein-Vazirani.

         =============== ======================================
         Algoritmo       Desempenho (# aplicações de :math:`f`)
         =============== ======================================
         Class. Det.     :math:`n` aplicações
         Quântico (XOR)  da ordem de :math:`2` aplicações
         Quântico (fase) 1 aplicação
         =============== ======================================

.. container:: section

   Transformada de Fourier Quântica - QFT

   .. container:: subsection

      Transformada Discreta de Fourier - DFT EXPLICAR o que é e fazer
      comparação?

   .. container:: subsection

      Definição e Exemplos

   .. container:: subsection

      Propriedades da QFT

      .. container:: subsubsection

         A QFT é unitária (fazer como proposição)

   .. container:: subsection

      Circuito para QFT

      .. container:: subsubsection

         QFT para :math:`n=1` qubit

      .. container:: subsubsection

         QFT para :math:`n=2` qubits

      .. container:: subsubsection

         QFT para :math:`n` qubits

   .. container:: subsection

      Algumas aplicações

.. container:: section

   Algoritmo de Shor

.. [1]
   Em Teoria da Informação e Criptografia Quântica, é uma convenção
   tácita rotular os dois lados da comunicação por Alice e Bob!

.. [2]
   Nesta parte, usou-se a notação que confunde uma palavra de bits com
   sua representação por número inteiro sem sinal. Ver observação
   `[cap5:rmk_notacao_vetor_bits_nro_inteiro] <#cap5:rmk_notacao_vetor_bits_nro_inteiro>`__.

.. [3]
   Para um número de bits :math:`n` grande, esse caso é semelhante ao
   caso com memória, em que não se permite repetir as entradas no
   sorteio.

.. [4]
   Isto é, (1) se não repete valores para diferentes entradas e (2) se o
   seu resultado varre todas as opções de palavras de :math:`n` bits. O
   item (1) significa que :math:`f` é *injetiva* e (2) significa que
   :math:`f` é *sobrejetiva*. Em símbolos, essas propriedades ficam:

   -  :math:`x_1,x_2 \in \mathbb{B}_n, \ \ x_1 \neq x_2 \implies f(x_1) \neq f(x_2)`

   -  :math:`\forall y \in \mathbb{B}_n, \exists x \in \mathbb{B}_n \colon y = f(x)`.

.. [5]
   Para simplificar, usou-se a notação que confunde uma palavra de bits
   com sua representação por número inteiro sem sinal. Ver observação
   `[cap5:rmk_notacao_vetor_bits_nro_inteiro] <#cap5:rmk_notacao_vetor_bits_nro_inteiro>`__.

.. [6]
   Fazendo uma analogia com o produto interno em :math:`\mathbb{C}^n`

.. [7]
   Os vetores de bits referidos são os com peso de Hamming 1. O peso de
   Hamming de um vetor de bits é o número de bits com valor igual a 1.
   Isso equivale à soma dos bits, considerando-os como números inteiros.
   O peso de Hamming de um vetor de :math:`n` bits está entre :math:`0`
   e :math:`n`.

.. [8]
   Para lembrar, as operações :math:`\cdot` e :math:`\oplus` se referem
   à AND e XOR. O “produto interno” :math:`a\cdot x` refere-se a
   :math:`(a_1 AND x_1) XOR \ldots XOR (a_n AND x_n) = a_1 \cdot x_1 \oplus \ldots \oplus a_n \cdot x_n`.
   Ver observação
   `[cap5:rmk_notacao_analoga_a_produto_interno_de_vetores_de_bits] <#cap5:rmk_notacao_analoga_a_produto_interno_de_vetores_de_bits>`__.
