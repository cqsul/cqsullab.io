.. role:: raw-latex(raw)
   :format: latex
..

.. _`cap4:CQ`:

Computação Quântica
===================

A *Computação Quântica* é um novo paradigma de computação em que
utilizam-se sistemas quânticos – os *qubits*, análogos dos bits
clássicos – para se realizar processamento de informação. Lançam-se mão
de alguns recursos não existentes na Computação Clássica, como
*superposição* e *emaranhamento*.

Há uma variedade de modelos de Computação Quântica, dentre os quais
pode-se citar:

-  **Computação Quântica de Circuitos** – computação realizada com
   portas lógicas quânticas, análogas às portas lógicas dos sistemas
   digitais clássicos;

-  **Computação Quântica Adiabática** – o sistema é preparado no estado
   fundamental e sofre a ação de um hamiltoniano que depende
   continuamente do tempo e que é projetado de forma que o estado
   fundamental, após a aplicação do hamiltoniano, contenha a solução do
   problema codificada (:raw-latex:`\cite{book:diss_vanessa}`, p.23);

-  **Máquina de Turing Quântica** – uma versão quântica da Máquina de
   Turing Clássica;

-  **Caminhada Aleatória Quântica** – versão quântica da caminhada
   aleatória clássica; é possível realizar computação quântica universal
   com esse modelo de computação.

O presente trabalho aborda apenas a Computação Quântica de Circuitos.

.. container:: section

   Computação Quântica de Circuitos Este modelo de Computação Quântica
   guarda analogia com a Computação Clássica com Portas Lógicas,
   apresentada no apêndice
   `[cap1_comp_classica] <#cap1_comp_classica>`__ (principalmente seção
   `[cap1:sec_nivel_logico] <#cap1:sec_nivel_logico>`__). A informação é
   codificada em qubits, e o processamento é feito por evolução temporal
   do sistema segundo operações unitárias (conforme postulado
   `[cap3:postulado_evol_temp] <#cap3:postulado_evol_temp>`__).

   .. figure:: cap4/figuras/esquema_comp_quantico.png
      :alt: 

   **Número de entradas e saídas**

   | Os circuitos quânticos devem ter o mesmo número de entradas e
     saídas, pois qubits não são introduzidos ou removidos durante o
     processamento e as portas lógicas quânticas são operadores, o que
     significa que preservam o número de qubits em que atuam.
     Diferentemente do que ocorre com os circuitos clássicos, nos quais
     há portas com diferentes números de bits à entrada e à saída (como
     as portas AND e NOT, por exemplo).
   | **Reversibilidade da Computação Quântica**

   Outra diferença é os circuitos quânticos são essencialmente
   *reversíveis*, ou seja, existe um circuito quântico inverso que
   consegue retornar as entradas originais do circuito a partir das
   saídas do mesmo. Isso ocorre porque o processamento se dá por
   operadores unitários, que são reversíveis, com inversos também dados
   por operadores unitários. Apesar de a maior parte da Computação
   Quântica ser reversível, há uma etapa irreversível: a medição dos
   qubits.

   Nos circuitos clássicos, a maioria das portas lógicas não são
   reversíveis. A porta AND, por exemplo, fornece resultado 1 se ambas
   as entradas forem 1 e fornece 0 caso contrário; no caso de a saída
   ser 0, não sabemos qual/quais das entradas é 0.

   Não há um impedimento absoluto para a Computação Clássica ser
   reversível. De fato, há investigações relacionadas a circuitos
   clássicos reversíveis, constituídos apenas por portas lógicas
   reversíveis.

   Há uma expectativa de que a computação reversível seja mais eficiente
   em termos energéticos que a computação irreversível. Isso se deve ao
   *princípio de Landauer*, que diz que o apagamento de 1 bit de
   informação está associado a uma dissipação de energia para o ambiente
   de, no mínimo, :math:`kT \ln 2` (ver referência
   :raw-latex:`\cite{book:pqci_benenti}`, p. 37). Em tese, se forem
   evitados os apagamentos de informação, a computação poderia ser feita
   sem gasto energético apreciável. Uma discussão interessante sobre o
   consumo energético e computação pode ser encontrada em
   :raw-latex:`\cite{book:qcqi_nc}`, na seção 3.2.5, e em
   :raw-latex:`\cite{book:pqci_benenti}`, seção 1.5.

.. container:: section

   O Qubit

   .. container:: subsection

      Descrição Matemática do Qubit O qubit é um sistema físico que pode
      ser descrito por um espaço de Hilbert de dimensão 2. O qubit já
      foi apresentado no capítulo anterior, exemplo
      `[cap3:ex_qubit] <#cap3:ex_qubit>`__. Os estados da base canônica
      são rotulados por :math:`\ket{0}` e :math:`\ket{1}`, e um estado
      geral para o qubit é o vetor unitário

      .. math:: \ket{\psi} = a \ket{0} + b\ket{1} \ ,

      em que :math:`|a|^2 + |b|^2 = 1` e
      :math:`a,b \in \mathbb{C}`.

   .. container:: subsection

      Fase Relativa e Fase Global

      .. rubric:: Fase Global
         :name: fase-global
         :class: unnumbered

      Chama-se *fase global* uma fase complexa multiplicando o estado de
      um qubit: :math:`e^{i\alpha}\ket{\psi}`. Dois estados
      :math:`\ket{\psi_1}` e
      :math:`\ket{\psi_2} = e^{i\alpha}\ket{\psi_1}`, iguais a menos de
      uma fase global, não podem ser distinguidos fisicamente. De fato,
      dado um observável :math:`A = \sum_k a_k P_k`, a probabilidade de
      o resultado de uma medida ser :math:`a_k`, nos dois casos, é
      igual:

      .. math:: p_2(a_k) =  ||P_k e^{i\alpha||\ket{\psi_1} }^2 = |e^{i\alpha}|^2 ||\ket{\psi_1||}^2 =  p_1(a_k) \ .

      A evolução temporal nos dois estados também é idêntica, a menos do
      fator :math:`e^{i\alpha}`, devido à linearidade dos operadores de
      evolução:

      .. math:: U\ket{\psi_2} = Ue^{i\alpha}\ket{\psi_1} = e^{i\alpha}U\ket{\psi_1}  \ ,

      e uma subsequente medição não conseguiria distinguir esses dois
      estados que diferem apenas por uma fase global. Na formação de um
      sistema composto, os vetores :math:`\ket{\psi_1}` e
      :math:`\ket{\psi_2} = e^{i\alpha}\ket{\psi}` produzem resultados
      idênticos, a menos da fase global :math:`\alpha`, dada a
      multilinearidade do produto tensorial:

      .. math:: \ket{\psi_2} \otimes \ket{\phi} = \big(e^{i\alpha}\ket{\psi_1}\big) \otimes \ket{\phi} = e^{i\alpha} \big( \ket{\psi_1} \otimes \ket{\phi} \ .

      Do mesmo modo, uma medida posterior não conseguiria distinguir
      esses dois estados.

      Dessa forma, a fase global não tem relevância física, e um sistema
      descrito por um vetor de estado :math:`\ket{\psi}` também pode ser
      descrito pelo vetor :math:`e^{i\alpha}\ket{\psi}`.

      .. rubric:: Fase Relativa
         :name: fase-relativa
         :class: unnumbered

      A *fase relativa* em um qubit é a diferença de fase entre os
      coeficientes que multiplicam o :math:`\ket{1}`. Por exemplo, os
      vetores

      .. math:: \ket{+} =  \frac{\ket{0} +i \ket{1}}{\sqrt{2}} \ \ \text{e} \ \  \ket{-} =  \frac{\ket{0} - \ket{1}}{\sqrt{2}}

      têm mesmo coeficiente multiplicando :math:`\ket{0}` e diferem
      apenas por um fator :math:`-1 = e^{i \pi}` multiplicando
      :math:`\ket{1}`, isto é, por uma fase relativa de :math:`\pi`.

      De forma geral, os estados

      .. math:: \ket{\psi_1} = a\ket{0} + b\ket{1} \ \ \text{e} \ \  \ket{\psi_2} = a\ket{0} + b e^{i\varphi}\ket{1}

      diferem por fase relativa :math:`\varphi`. Esses estados
      apresentam mesmas probabilidades em uma medida na base
      computacional:

      .. math::

         \begin{split}
            p_2(\ket{0}) = |a|^2 = p_1(\ket{0})
           \end{split}

      .. math:: p_2(\ket{1}) = |{b e^{i\varphi}|^2 = |b|^2  |e^{i\varphi}|^2 =  |b|^2 =  p_1(\ket{1}) \ ,

      no entanto, em bases diferentes, podem apresentar probabilidades
      diferentes. Exemplificando, os estados :math:`\ket{+}` e
      :math:`\ket{-}` diferem por uma fase relativa, no entanto formam
      uma base. E a medida nessa base fornece resultados distintos. A
      evolução por transformações unitárias também apresenta resultados
      diferentes. Por exemplo, a aplicação do operador unitário
      :math:`H` fornece :math:`H\ket{+} = \ket{0}` e
      :math:`H\ket{-} = \ket{1}`.

      Assim, ao contrário da fase global, a fase relativa apresenta
      relevância física.

   .. container:: subsection

      Representação de um Qubit na Esfera de Bloch

      .. rubric:: A Esfera de Bloch
         :name: a-esfera-de-bloch
         :class: unnumbered

      O estado :math:`\ket{\psi} = a \ket{0} + b\ket{1}` de um qubit
      pode ser reescrito, a menos de uma fase global, como

      .. math:: \ket{\psi} = \cos(\theta/2)\ket{0} + e^{i\varphi} \sin(\theta/2) \ket{1} \ , \ \ \theta \in [0,\pi] , \varphi \in [0,2\pi) \ .

      Isso será justificado na seção
      `[cap4:sec_justificativa_esf_bloch] <#cap4:sec_justificativa_esf_bloch>`__.
      Pode-se multiplicar o estado por uma fase global para que o termo
      multiplicando :math:`\ket{0}` seja real e positivo. Fazendo-se
      essa identificação em relação à fase global, tem-se o estado de um
      qubit descrito por dois parâmetros :math:`\theta` e
      :math:`\varphi`. Utilizando esses dois parâmetros no sistema de
      coordenadas esféricas, pode-se corresponder os estados de um qubit
      com os pontos na superfície de uma esfera de raio unitário, a
      chamada *Esfera de Bloch*.

      .. figure:: cap4/figuras/blochsphere3.png
         :alt: 

      .. rubric:: Pontos na Esfera de Bloch
         :name: pontos-na-esfera-de-bloch
         :class: unnumbered

      Os polos norte da esfera corresponde ao estado :math:`\ket{0}` e o
      polo sul, ao :math:`\ket{1}`. No equador, situam-se os estados da
      forma
      :math:`\frac{1}{\sqrt{2}} \ket{0} + \frac{e^{i\varphi}}{\sqrt{2}} \ket{1}`,
      isto é, superposições dos estados :math:`\ket{0}` e
      :math:`\ket{1}` com o mesmo peso e com alguma fase relativa.

      .. table:: Intersecção da esfera de Bloch com os eixos
      coordenados.

         +----------------------+----------------------+----------------------+
         | Ponto da esfera de   |                      | Estado               |
         | Bloch                |                      | :math:`\ket{\psi}`   |
         +======================+======================+======================+
         | :math:`\begin{array  | :math:`\begin{array} | :                    |
         | }{l} \phantom{-}\hat | {ll} \theta = \pi/2  | math:`\begin{array}{ |
         | {x} = (1,0,0) \\ -\h | & \varphi = 0 \\ \th | l} \ket{+} \\ \ket{- |
         | at{x} = (-1,0,0) \\  | eta = \pi/2 & \varph | } \\ \ket{+i} \\ \ke |
         | \phantom{-}\hat{y} = | i = \pi \\ \theta =  | t{-i} \\ \ket{0} \\  |
         |  (0,1,0) \\ -\hat{y} | \pi/2 & \varphi = \p | \ket{1} \end{array}` |
         |  = (0,-1,0) \\ \phan | i/2 \\ \theta = \pi/ |                      |
         | tom{-} \hat{z} = (0, | 2 & \varphi = 3\pi/2 |                      |
         | 0,1) \\ -\hat{z} = ( |  \\ \th              |                      |
         | 0,0,-1) \end{array}` | eta = 0 & \\ \theta  |                      |
         |                      | = \pi & \end{array}` |                      |
         +----------------------+----------------------+----------------------+

      Observe que os vetores da base :math:`\mathcal{X}` correspondem às
      intersecções da esfera com o eixo :math:`x`. De forma similar,
      isso vale para as bases :math:`\mathcal{Y}` e :math:`\mathcal{Z}`,
      que correspondem às intersecções da esfera com os eixos :math:`y`
      e :math:`z`, respectivamente. Essas bases foram abordadas na seção
      `[cap3:sec_autoval_autovet_diag_matrizes_pauli] <#cap3:sec_autoval_autovet_diag_matrizes_pauli>`__.

      .. figure:: cap4/figuras/blochsphere4.png
         :alt: 

      .. rubric:: Projeções nos eixos coordenados
         :name: projeções-nos-eixos-coordenados
         :class: unnumbered

      As projeções nos eixos :math:`x`, :math:`y` e :math:`x` de um
      ponto :math:`\vec{r}` na superfície da esfera de Bloch são
      dadas pelas coordenadas esféricas:

      .. math::

         \begin{split}
             \vec{r} 
             &= r_x \, \hat{x} + r_y \, \hat{y} + r_z \, \hat{z}  \\
             &= \sin \theta \cos \varphi \, \hat{x} + \sin \theta \sin \varphi \, \hat{y} + \cos \theta \, \hat{z} \ , \ \ \theta \in [0,\pi] , \varphi \in [0,2\pi) \ .
            \end{split}

      Essas projeções correspondem aos valores esperados dos operadores
      hermitianos :math:`X`, :math:`Y` e :math:`Z` de Pauli:

      .. math::

         \begin{split}
             r_x &= \sin \theta \cos \varphi = \braket{X} \\
             r_y &= \sin \theta \sin \varphi = \braket{Y} \\
             r_z &= \ \ \   \cos \theta \ \ \ \, = \braket{Z} \ . 
            \end{split}

   .. container:: subsection

      Justificativas para a seção anterior

      .. container:: proposition

         O estado de um qubit pode ser escrito, a menos de uma fase
         global, como

         .. math:: \ket{\psi} = \cos(\theta/2)\ket{0} + e^{i\varphi} \sin(\theta/2) \ket{1} \ , \ \ \theta \in [0,\pi] , \varphi \in [0,2\pi) \ .

      .. container:: proof

         *Proof.* Seja :math:`\ket{\psi} = a \ket{0} + b \ket{1}` com
         :math:`a,b \in \mathbb{C}` e :math:`|a|^2 + |b|^2 = 1`.
         Os coeficientes :math:`a` e :math:`b` são números complexos,
         portanto admitem módulo e fase complexa, podendo ser escritos
         na forma polar como

         .. math:: a = \rho_a e^{i\theta_a} \ , \ \ b = \rho_b e^{i\theta_b} \ ,

         com :math:`\rho_a , \rho_b \geq 0`. Tem-se

         .. math::

            |a|^2 + |b|^2 = 1 \implies \rho_a^2 + \rho_b^2 = 1 \implies 
            \begin{cases}
             \rho_a = \cos(\theta/2) \\
             \rho_b = \sin(\theta/2) 
            \end{cases}
            \theta \in [0,\pi] \ .

         Portanto, pode-se escrever

         .. math::

            \begin{split}
                \ket{\psi} 
                &= \cos(\theta/2) e^{i\theta_a} \ket{0} +  \sin(\theta/2) e^{i\theta_b} \ket{1} \\
                &= e^{i\theta_a} \left( \cos(\theta/2) \ket{0} +  \sin(\theta/2) e^{i(\theta_b - \theta_a)} \ket{1} \right) \ .
               \end{split}

         Como a fase global não tem efeito físico, pode-se multiplicar
         por um fator :math:`e^{-i\theta_a} \ket{0}` para que o
         coeficiente de :math:`\ket{0}` se torne um número real
         positivo. Definindo :math:`\varphi = \theta_b - \theta_a`,
         pode-se dizer, sem prejuízo, que :math:`\varphi \in [0,2\pi)`.
         Dessa forma, o estado :math:`\ket{\psi}` fica

         .. math:: \ket{\psi} = \cos(\theta/2)\ket{0} + e^{i\varphi} \sin(\theta/2) \ket{1} \ , \ \ \theta \in [0,\pi] , \varphi \in [0,2\pi) \ . \qedhere

          ◻

      .. container:: proposition

         Seja :math:`\vec{r}` um vetor na superfície da esfera
         de Bloch correspondendo ao estado :math:`\ket{\psi}`. As
         coordenadas :math:`x`, :math:`y` e :math:`z` desse vetor
         correspondem aos valores esperados dos operadores :math:`X`,
         :math:`Y` e :math:`Z` de Pauli referentes ao estado
         :math:`\ket{\psi}`:

         .. math::

            \begin{split}
                r_x &= \sin \theta \cos \varphi = \braket{X} \\
                r_y &= \sin \theta \sin \varphi = \braket{Y} \\
                r_z &= \ \ \   \cos \theta \ \ \ \, = \braket{Z} \ . 
               \end{split}

      .. container:: proof

         *Proof.* Seja
         :math:`\ket{\psi} = \cos(\theta/2)\ket{0} + e^{i\varphi} \sin(\theta/2) \ket{1}`.
         Calculando o valor esperado dos operadores :math:`X`, :math:`Y`
         e :math:`Z` para esse estado, tem-se:

         .. math::

            \begin{split}
                  \braket{X}
                  &=  \braket{X | \psi} \\
                  &= \big( \cos(\theta/2)\bra{0} + e^{-i\varphi} \sin(\theta/2) \bra{1}\big) X\big( \cos(\theta/2)\ket{0} + e^{i\varphi} \sin(\theta/2)  \ket{1} \big) \\
                  &= \big( \cos(\theta/2)\bra{0} + e^{-i\varphi} \sin(\theta/2) \bra{1}\big) \big( \cos(\theta/2)\ket{1} + e^{i\varphi} \sin(\theta/2)  \ket{0} \big)  \\
                  &= 0 + \cos(\theta/2) e^{i\varphi}  \sin(\theta/2) + 0 +  e^{-i\varphi} \sin(\theta/2)\cos(\theta/2) \\
                  &= \big(e^{i\varphi} + e^{-i\varphi}  \big)\sin(\theta/2)\cos(\theta/2) \\
                  &= 2 \cos \varphi \sin(\theta/2)\cos(\theta/2) \\
                  &= \sin \theta \cos \varphi \\
                  &= r_x
                \end{split}

         .. math::

            \begin{split}
                  \braket{Y}
                  &=  \braket{Y | \psi} \\
                  &= \big( \cos(\theta/2)\bra{0} + e^{-i\varphi} \sin(\theta/2) \bra{1}\big) Y\big( \cos(\theta/2)\ket{0} + e^{i\varphi} \sin(\theta/2)  \ket{1} \big) \\
                  &= \big( \cos(\theta/2)\bra{0} + e^{-i\varphi} \sin(\theta/2) \bra{1}\big) \big( i\cos(\theta/2)\ket{1} - i e^{i\varphi} \sin(\theta/2)  \ket{0} \big)  \\
                  &= 0 -i \cos(\theta/2) e^{i\varphi}  \sin(\theta/2) + 0 + i e^{-i\varphi} \sin(\theta/2)\cos(\theta/2) \\
                  &= -i \big(e^{i\varphi} - e^{-i\varphi}  \big)\sin(\theta/2)\cos(\theta/2) \\
                  &= -i (2 i \sin \varphi )\sin(\theta/2)\cos(\theta/2) \\
                  &= \sin \theta \sin \varphi \\
                  &= r_y
                \end{split}

         .. math::

            \begin{split}
                  \braket{Z}
                  &=  \braket{Z | \psi} \\
                  &= \big( \cos(\theta/2)\bra{0} + e^{-i\varphi} \sin(\theta/2) \bra{1}\big) Z\big( \cos(\theta/2)\ket{0} + e^{i\varphi} \sin(\theta/2)  \ket{1} \big) \\
                  &= \big( \cos(\theta/2)\bra{0} + e^{-i\varphi} \sin(\theta/2) \bra{1}\big) \big( \cos(\theta/2)\ket{0}  - e^{i\varphi} \sin(\theta/2)  \ket{1} \big)  \\
                  &= \cos(\theta/2) \cos(\theta/2) + 0 + 0 -  e^{-i\varphi} \sin(\theta/2)e^{i\varphi} \sin(\theta/2)  \\
                  &=  \cos^2(\theta/2) - \sin^2(\theta/2)\\
                  &= \sin \theta \\
                  &= r_z
                \end{split}

         Nas contas, foram usadas as seguintes identidades complexas:

         .. math:: \cos \varphi = \frac{  e^{i\varphi} +  e^{-i\varphi}}{2} \ , \ \   \sin \varphi = \frac{  e^{i\varphi} -  e^{-i\varphi}}{2i} \ . \qedhere

          ◻

.. container:: section

   Notação de Circuitos Os detalhes da notação utilizada para
   representar circuitos quânticos serão vistos nesta seção. A
   referência utilizada nesta parte é o capítulo 2 do livro
   :raw-latex:`\cite{book:icq_portugal}`.

   Um circuito quântico é ilustrado na figura
   `1.3 <#cap4:fig_exemplo_circuito_quantico>`__ disposta abaixo. Alguns
   detalhes pertinentes são abordados na sequência.

   .. figure:: cap4/figuras/exemplo_circuito_quantico_teletransporte.png
      :alt: 

   | **Entradas e saídas**
   | O circuito deve conter o mesmo número de entradas e saídas (às
     vezes podem estar omitidas quando não utilizadas). Cada qubit é
     representado por uma linha horizontal, e linhas duplas representam
     bits clássicos. Pode-se pôr rótulos nos qubits para indicar em que
     estado se encontram na entrada.

   .. container:: float

      :math:`\begin{array}{ccccc}
        \text{(a)} & & \text{(b)} & & \text{(c)} \\ & & & & \\
        \Qcircuit @C=1em @R=.7em {\lstick{\ket{\psi}} & \qw & \qw & \qw }
        & &
        \quad \phantom{\text{$\ket{\psi}$}}  
        \Qcircuit @C=1em @R=1.5em { \lstick{ \begin{array}{c} \\ \ket{\beta_{00}} \end{array} \hspace{5pt}} & \qw & \qw & \qw \\
                           & \qw \gategroup{1}{1}{2}{1}{1.5em}{\{} & \qw & \qw }
        & &
        \phantom{0 ou 1 \ \ }\Qcircuit @C=1em @R=.7em {\lstick{\text{0 ou 1}}& \cw & \cw & \cw}
       \end{array}`

   | Os qubits de entrada podem se encontrar em estados :math:`\ket{0}`,
     :math:`\ket{1}` ou em superposições desses estados. Também podem
     encontrar-se em estados emaranhados, não possíveis de se exprimir
     como produto tensorial de estados de 1 qubit.
   | **Sequência de operações**
   | A passagem do tempo, e portanto a sequência de operações, é
     representada da esquerda para a direita. Ocasionalmente pode-se
     representar o circuito na forma vertical, e a passagem do tempo é
     representada de cima para baixo.
   | **Símbolos para Portas Lógicas Quânticas**
   | As portas lógicas quânticas são representadas por caixas contendo o
     mesmo número de entradas e saídas. As portas lógicas controladas
     são portas lógicas de mais de 1 qubit em que pelo menos um dos
     qubits age como controle; o qubit de controle é representado por um
     círculo (mais detalhes serão vistos posteriormente).

   .. container:: float

      :math:`\begin{array}{ccc}
        \text{(a)} & \text{(b)} & \text{(c)} \\ & & \\
        \Qcircuit @C=1em @R=.7em { &          & \\ 
                                   & \gate{U} & \qw }
        &
        \centering \Qcircuit @C=1em @R=.7em { & \ctrl{1} & \qw \\
                                              & \gate{U} & \qw }
        &
        \Qcircuit @C=1em @R=.7em {
      & \multigate{2}{U} & \qw \\
      & \ghost{U}& \qw \\
      & \ghost{U} & \qw
      }
       \end{array}`

   | **Medições de qubits**
   | As medições são as únicas operações potencialmente irreversíveis de
     um circuito quântico. Em geral são realizadas na base computacional
     :math:`\ket{0}` e :math:`\ket{1}`. A notação para medições é
     ilustrada abaixo, em que, novamente, fica implícito que a base de
     medidas é a computacional. Após a medição na base computacional, o
     resultado é um bit clássico.

   .. container:: float

      :math:`\begin{array}{ccc}
        \text{(a)} &  \text{(b)}  & \text{(c)} \\ & & \\
        \Qcircuit @C=1em @R=.7em { & \qw & \meter & \cw & \cw}
        &  
        \Qcircuit @C=1em @R=.7em { & \qw & \measureD{} & \cw & \cw} 
        &
        \Qcircuit @C=1em @R=.7em { & \qw & \measureD{\mathcal{O}} &  & }
       \end{array}`

.. container:: section

   Portas Lógicas Quânticas

   As portas lógicas quânticas são operações unitárias aplicadas em um
   ou mais qubits. Nesta seção, essas portas lógicas são apresentadas em
   detalhes.

   .. container:: subsection

      Portas Lógicas de 1 Qubit Estas portas lógicas atuam em 1 qubit
      apenas. São descritas por matrizes unitárias :math:`2 \times 2`.
      As portas lógicas apresentadas nesse tópico são as portas
      :math:`X`, :math:`Y` e :math:`Z` de Pauli (também denotadas por
      :math:`\sigma_x`, :math:`\sigma_y` e :math:`\sigma_z`,
      respectivamente), a porta :math:`H` de Hadamard, a porta de fase
      ou porta :math:`S` e a porta :math:`\tfrac{\pi}{8}` ou porta
      :math:`T`.

      .. rubric:: Porta X de Pauli ou NOT Quântica
         :name: porta-x-de-pauli-ou-not-quântica
         :class: unnumbered

      A porta :math:`X` de Pauli é a operação unitária de 1 qubit que,
      na base computacional, é representada pela matriz de Pauli
      :math:`X = \sigma_x`. Algumas informações dessa porta estão
      resumidas na figura abaixo.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :math:            |   | :math:            |
         | `\begin{array}{c} |   | `\begin{array}{c} |   | `\begin{array}{l} |
         |        \Qcircuit  |   |      X = \mqty[   |   |                   |
         | @C=1em @R=.7em {& |   |  0 & 1 \\ 1 & 0 ] |   |  \begin{array}{l} |
         |  \gate{X} & \qw } |   |      \\           |   |         X \ke     |
         |        \\ \\      |   |      \\ \\        |   | t{0} = \ket{1} \\ |
         |        \Qcirc     |   |                   |   |         X \ke     |
         | uit @C=1em @R=1em |   |    \text{Notação  |   | t{1} = \ket{0} \\ |
         |  {& \targ & \qw } |   | alternativa}\\ \\ |   |                   |
         |                   |   |      X = \sigma_x |   |       \end{array} |
         |      \end{array}` |   |      \end{array}` |   |        \\ \\      |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \begin{array}{l} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  X \ket{b} = \ket |
         |                   |   |                   |   | {\overline{b}} \\ |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \ \ \ \scri |
         |                   |   |                   |   | ptstyle{b = 0,1 \ |
         |                   |   |                   |   |  ; \ \overline{b} |
         |                   |   |                   |   |  = \text{NOT}(b)} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |        \\ \\      |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \begin{array}{l} |
         |                   |   |                   |   |         X \ke     |
         |                   |   |                   |   | t{+} = \ket{+} \\ |
         |                   |   |                   |   |         X \       |
         |                   |   |                   |   | ket{-} = -\ket{-} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      \end{array}` |
         +-------------------+---+-------------------+---+-------------------+

      .. rubric:: Porta Y de Pauli
         :name: porta-y-de-pauli
         :class: unnumbered

      A porta :math:`Y` de Pauli é a operação unitária de 1 qubit que,
      na base computacional, é representada pela matriz de Pauli
      :math:`Y = \sigma_y`. Algumas informações dessa porta estão
      dispostas abaixo.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :math:            |   | :math:            |
         | `\begin{array}{c} |   | `\begin{array}{c} |   | `\begin{array}{l} |
         |        \Qcircuit  |   |      Y = \mqt     |   |                   |
         | @C=1em @R=.7em {& |   | y[ 0 & -i \\ i &  |   |  \begin{array}{l} |
         |  \gate{Y} & \qw } |   | \phantom{-}0 ] \\ |   |         Y \ket{   |
         |                   |   |      \\ \\        |   | 0} = i \ket{1} \\ |
         |      \end{array}` |   |                   |   |         Y \ke     |
         |                   |   |    \text{Notação  |   | t{1} = -i \ket{0} |
         |                   |   | alternativa}\\ \\ |   |                   |
         |                   |   |      Y = \sigma_y |   |       \end{array} |
         |                   |   |      \end{array}` |   |         \\ \\     |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \begin{array}{l} |
         |                   |   |                   |   |         Y \ket{   |
         |                   |   |                   |   | b} = (-1)^b i\ket |
         |                   |   |                   |   | {\overline{b}} \\ |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \ \ \ \scri |
         |                   |   |                   |   | ptstyle{b = 0,1 \ |
         |                   |   |                   |   |  ; \ \overline{b} |
         |                   |   |                   |   |  = \text{NOT}(b)} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |        \\ \\      |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \begin{array}{l} |
         |                   |   |                   |   |         Y \ket{+  |
         |                   |   |                   |   | } = -i \ket{-} \\ |
         |                   |   |                   |   |         Y \k      |
         |                   |   |                   |   | et{-} = i \ket{+} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      \end{array}` |
         +-------------------+---+-------------------+---+-------------------+

      .. rubric:: Porta Z de Pauli
         :name: porta-z-de-pauli
         :class: unnumbered

      A porta :math:`Z` de Pauli é a operação unitária de 1 qubit que é
      representada na base computacional pela matriz de Pauli
      :math:`Z = \sigma_z`. Essa porta introduz uma fase relativa de
      :math:`\pi`, o que corresponde a multiplicar o :math:`\ket{1}` por
      :math:`-1 = e^{i\pi}`, como se pode observar no quadro a seguir.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :math:            |   | :math:            |
         | `\begin{array}{c} |   | `\begin{array}{c} |   | `\begin{array}{l} |
         |        \Qcircuit  |   |      Z = \m       |   |                   |
         | @C=1em @R=.7em {& |   | qty[ 1 & \phantom |   |  \begin{array}{l} |
         |  \gate{Z} & \qw } |   | {-}0 \\ 0 & -1 ]  |   |         Z \ke     |
         |                   |   |      \\           |   | t{0} = \ket{0} \\ |
         |      \end{array}` |   |      \\           |   |         Z \k      |
         |                   |   |      \\           |   | et{1} = -\ket{1}  |
         |                   |   |      \text{Not    |   |                   |
         |                   |   | ação alternativa} |   |       \end{array} |
         |                   |   |      \\ \\        |   |        \\ \\      |
         |                   |   |      Z = \sigma_z |   |                   |
         |                   |   |      \end{array}` |   |  \begin{array}{l} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      Z \ket{b} =  |
         |                   |   |                   |   | (-1)^b \ket{b} \\ |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \ \ \ \scri |
         |                   |   |                   |   | ptstyle{b = 0,1 } |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |        \\ \\      |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \begin{array}{l} |
         |                   |   |                   |   |         Z \ke     |
         |                   |   |                   |   | t{+} = \ket{-} \\ |
         |                   |   |                   |   |         Z         |
         |                   |   |                   |   | \ket{-} = \ket{+} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      \end{array}` |
         +-------------------+---+-------------------+---+-------------------+

      .. rubric:: Porta Hadamard
         :name: porta-hadamard
         :class: unnumbered

      A porta de Hadamard é uma operação unitária de 1 qubit
      representada na base computacional pela matriz de Hadamard
      :math:`H`. Essa matriz, definida abaixo, também realiza mudança de
      base de
      :math:`\mathcal{I} = \big\lbrace \ket{0}, \ket{1}\big\rbrace` para
      :math:`\mathcal{X} = \big\lbrace \ket{+}, \ket{-}\big\rbrace` e
      vice-versa, como visto no exemplo
      `[cap2:ex_matrix_mudança_de_base] <#cap2:ex_matrix_mudança_de_base>`__.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :math:`H =\fra    |   | :math:            |
         | `\begin{array}{c} |   | c{1}{\sqrt{2}} \m |   | `\begin{array}{l} |
         |        \Qcircuit  |   | qty[ 1 & \phantom |   |                   |
         | @C=1em @R=.7em {& |   | {-}1 \\ 1 & -1 ]` |   |  \begin{array}{l} |
         |  \gate{H} & \qw } |   |                   |   |         H \ke     |
         |                   |   |                   |   | t{0} = \ket{+} \\ |
         |      \end{array}` |   |                   |   |         H         |
         |                   |   |                   |   | \ket{1} = \ket{-} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |        \\ \\      |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \begin{array}{l} |
         |                   |   |                   |   |         H \ke     |
         |                   |   |                   |   | t{+} = \ket{0} \\ |
         |                   |   |                   |   |         H         |
         |                   |   |                   |   | \ket{-} = \ket{1} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      \end{array}` |
         +-------------------+---+-------------------+---+-------------------+

      .. rubric:: Porta de Fase ou Porta S
         :name: porta-de-fase-ou-porta-s
         :class: unnumbered

      A porta :math:`S` introduz uma fase relativa de
      :math:`\frac{\pi}{2}` no qubit em que atua, levando um estado
      :math:`a \ket{0} + b \ket{1}` em um estado
      :math:`a \ket{0} + i b \ket{1}`, já que
      :math:`i = e^{i\frac{\pi}{2}}`. Os detalhes pertinentes a essa
      porta lógica estão dispostos abaixo.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :                 |   | :math:            |
         | `\begin{array}{c} |   | math:`S = \mqty[  |   | `\begin{array}{l} |
         |        \Qcircuit  |   | 1 & 0 \\ 0 & i ]` |   |                   |
         | @C=1em @R=.7em {& |   |                   |   |  \begin{array}{l} |
         |  \gate{S} & \qw } |   |                   |   |         S \ke     |
         |                   |   |                   |   | t{0} = \ket{0} \\ |
         |      \end{array}` |   |                   |   |         S \       |
         |                   |   |                   |   | ket{1} = i\ket{1} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      \end{array}` |
         +-------------------+---+-------------------+---+-------------------+

      .. rubric:: Porta T ou Porta :math:`\frac{\pi}{8}`
         :name: porta-t-ou-porta-fracpi8
         :class: unnumbered

      A porta :math:`T`, também conhecida como porta
      :math:`\frac{\pi}{8}`, é uma porta lógica que introduz uma fase
      relativa de :math:`\frac{\pi}{4}`, levando um estado
      :math:`a\ket{0} + b \ket{1}` em
      :math:`a\ket{0} + e^{i\frac{\pi}{4}} b \ket{1}`.

      O nome :math:`\frac{\pi}{8}` dessa porta se deve ao fato de poder
      ser escrita na forma

      .. math:: T = e^{i\frac{\pi}{8}} \mqty[ e^{-i\frac{\pi}{8}} & 0 \\ 0 & e^{i\frac{\pi}{8}} ] \ .

      Isso significa que, a menos de uma fase global, essa operação
      realiza uma mudança de fase de :math:`+\frac{\pi}{8}` no estado
      :math:`\ket{0}` e de :math:`-\frac{\pi}{8}` no estado
      :math:`\ket{1}`.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :m                |   | :math:            |
         | `\begin{array}{c} |   | ath:`T = \mqty[ 1 |   | `\begin{array}{l} |
         |        \Qcircuit  |   |  & 0 \\ 0 & e^{i  |   |                   |
         | @C=1em @R=.7em {& |   | \frac{\pi}{4}} ]` |   |  \begin{array}{l} |
         |  \gate{T} & \qw } |   |                   |   |         T \ke     |
         |                   |   |                   |   | t{0} = \ket{0} \\ |
         |      \end{array}` |   |                   |   |         T \k      |
         |                   |   |                   |   | et{1} = e^{i \fra |
         |                   |   |                   |   | c{\pi}{4}}\ket{1} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      \end{array}` |
         +-------------------+---+-------------------+---+-------------------+

      .. rubric:: Porta de Fase :math:`\theta`
         :name: porta-de-fase-theta
         :class: unnumbered

      A porta de fase pode ser generalizada para uma fase arbitrária
      :math:`\theta`. Nesse caso, a aplicação dessa porta, denotada por
      :math:`S(\theta)` leva um estado :math:`a\ket{0} + b\ket{1}` em
      :math:`a\ket{0} + e^{i\theta} b\ket{1}`. A matriz que realiza isso
      é mostrada a seguir.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :ma               |   | :math:            |
         | `\begin{array}{c} |   | th:`S(\theta) = \ |   | `\begin{array}{l} |
         |        \Qcircuit  |   | mqty[ 1 & 0 \\ 0  |   |                   |
         | @C=1em @R=.7em {& |   | & e^{i\theta}  ]` |   |  \begin{array}{l} |
         |  \gate{S} & \qw } |   |                   |   |         S \ke     |
         |                   |   |                   |   | t{0} = \ket{0} \\ |
         |      \end{array}` |   |                   |   |                   |
         |                   |   |                   |   |    S \ket{1} = e^ |
         |                   |   |                   |   | {i\theta} \ket{1} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      \end{array}` |
         +-------------------+---+-------------------+---+-------------------+

      Como casos particulares, tem-se

      .. math:: Z = S(\pi) \ , \ \ S = S(\tfrac{\pi}{2}) \ \text{e} \ \ T = S(\tfrac{\pi}{4}) \ .

   .. container:: subsection

      Portas Lógicas de 2 Qubits

      As portas lógicas de 2 qubits são realizadas por matrizes
      unitárias :math:`4\times 4`. As principais operações são CNOT (NOT
      controlada), :math:`Z` controlada e SWAP, descritas abaixo.

      .. rubric:: Porta CNOT
         :name: porta-cnot
         :class: unnumbered

      A porta CNOT, ou NOT controlada, é uma porta de 2 qubits em que um
      deles exerce a função de controle e o outro, a de alvo. Em geral,
      quando não especificado, o primeiro qubit é o controle e o
      segundo, o alvo. Se o qubit de controle for :math:`\ket{0}`, nada
      acontece com o qubit alvo. Se o controle for :math:`\ket{1}`, a
      porta NOT quântica (porta :math:`X` de Pauli) é aplicada ao alvo:

      .. math:: \text{CNOT} \ket{0}_1\ket{1}_2 = \ket{0}_1\ket{1}_2 \ , \ \ \text{CNOT}\ket{1}_1\ket{0}_2 = X_2 \ket{1}_1 \ket{0}_2 = \ket{1}_1 \ket{1}_2 \ .

      Esse comportamento é análogo à entrada “enable” em circuitos
      digitais clássicos, que permite a ação do circuito se está
      habilitada em 1, ou nada acontece, se o enable é 0. A novidade na
      Computação Quântica é que a entrada de controle é um qubit e pode,
      portanto, se encontrar em uma superposição de estados, como
      :math:`\frac{\ket{0}+\ket{1}}{\sqrt{2}}`. A aplicação da porta
      CNOT, em casos como esse, ficaria

      .. math::

         \begin{split}
               \text{CNOT}\frac{\ket{0}_1+\ket{1}_1}{\sqrt{2}}\ket{1}_2 
               &= \frac{1}{\sqrt{2}} \big( \text{CNOT}\ket{0}_1 \ket{1}_2 + \text{CNOT}\ket{1}_1 \ket{1}_2 \big) \\
               &= \frac{1}{\sqrt{2}} \big( \ket{0}_1 \ket{1}_2 + \ket{1}_1 \ket{0}_2 \big) \ .
              \end{split}

      A porta CNOT tem suas informações resumidas no quadro abaixo.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :math:`\          |   | :math:            |
         | `\begin{array}{c} |   | text{CNOT} = \mqt |   | `\begin{array}{l} |
         |        \Qcircuit  |   | y[ 1 & \textcolor |   |                   |
         | @C=1em @R=1em {&  |   | {gray}{0}  & \tex |   |  \begin{array}{l} |
         | \ctrl{1} & \qw \\ |   | tcolor{gray}{0} & |   |                   |
         |                   |   |  \textcolor{gray} |   | \text{CNOT} \ket{ |
         |                   |   | {0}  \\\textcolor |   | 00} = \ket{00} \\ |
         |   & \targ & \qw } |   | {gray}{0}  & 1 &  |   |                   |
         |        \\ \\      |   | \textcolor{gray}{ |   | \text{CNOT} \ket{ |
         |        \Qcircuit  |   | 0}  & \textcolor{ |   | 01} = \ket{01} \\ |
         | @C=1em @R=1em {&  |   | gray}{0} \\\textc |   |                   |
         | \ctrl{1} & \qw \\ |   | olor{gray}{0}  &\ |   | \text{CNOT} \ket{ |
         |                   |   | textcolor{gray}{0 |   | 10} = \ket{11} \\ |
         |                 & |   | }  & \textcolor{g |   |                   |
         |  \gate{X} & \qw } |   | ray}{0} & 1 \\\te |   |   \text{CNOT} \ke |
         |                   |   | xtcolor{gray}{0}  |   | t{11} = \ket{10}  |
         |      \end{array}` |   |  &\textcolor{gray |   |                   |
         |                   |   | }{0}  & 1 &\textc |   |       \end{array} |
         |                   |   | olor{gray}{0}  ]` |   |        \\ \\      |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \begin{array}{l} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   | \text{CNOT} \ket{ |
         |                   |   |                   |   | 0b} = \ket{0b} \\ |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \text{CNOT} |
         |                   |   |                   |   |  \ket{1b} = \ket{ |
         |                   |   |                   |   | 1\overline{b}} \\ |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \ \ \ \scri |
         |                   |   |                   |   | ptstyle{b = 0,1 \ |
         |                   |   |                   |   |  ; \ \overline{b} |
         |                   |   |                   |   |  = \text{NOT}(b)} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |        \\ \\      |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \begin{array}{l} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |        \text{CNOT |
         |                   |   |                   |   | } \ket{a,b} = \ke |
         |                   |   |                   |   | t{a,a\oplus b} \\ |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \ \ \ \scriptsty |
         |                   |   |                   |   | le{a,b = 0,1 } \\ |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \ \ \ \scri |
         |                   |   |                   |   | ptstyle{ \oplus = |
         |                   |   |                   |   |  \text{XOR} = \te |
         |                   |   |                   |   | xt{adição mod 2}} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      \end{array}` |
         +-------------------+---+-------------------+---+-------------------+

      A porta CNOT também pode aparecer com o controle no segundo qubit
      e alvo no primeiro qubit. Nesse caso, podem ser usados índices no
      símbolo CNOT para especificar o controle e o alvo em situações
      mais específicas. Por exemplo:

      .. math::

         \begin{array}{lll}
          \begin{array}{l}  \\ \\ \text{CNOT}_{1,2} \\ (= \text{\footnotesize{CNOT}}) \end{array}
          & & 
         \Qcircuit @C=10pt @R=10pt {
          & \ctrl{1} & \qw \\
          & \targ    & \qw }
         \\ \\
         \begin{array}{l}  \\ \text{CNOT}_{2,1} \end{array}
          & & 
         \Qcircuit @C=10pt @R=10pt {
         & \targ     & \qw \\
         & \ctrl{-1} & \qw }
             \end{array}

      Uma notação semelhante pode ser usada em outras portas controladas
      para especificar o qubit de controle e o de alvo.

      .. rubric:: Porta Z Controlada
         :name: porta-z-controlada
         :class: unnumbered

      A porta :math:`Z` controlada também atua em 2 qubits, um deles com
      função de controle e o outro, de alvo. Se o controle (o primeiro
      qubit) for :math:`\ket{0}`, o alvo (segundo qubit) não se
      modifica, e se o controle for :math:`\ket{1}`, aplica-se uma porta
      :math:`Z` de Pauli ao alvo, como se pode ver no quadro a seguir.
      Se os qubits estiverem em superposição, basta usar a linearidade
      do operador CZ e atuar em cada estado da base computacional
      isoladamente:

      .. math::

         \begin{split}
              \text{CZ} \frac{\ket{0}_1+\ket{1}_1}{\sqrt{2}}\ket{1}_2 
              &= \frac{1}{\sqrt{2}} \big( \text{CZ}\ket{0}_1 \ket{1}_2 + \text{CZ}\ket{1}_1 \ket{1}_2 \big) \\
              &= \frac{1}{\sqrt{2}} \big( \ket{0}_1 \ket{1}_2 - \ket{1}_1 \ket{1}_2 \big) \ .
             \end{split}

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :math:`           |   | :math:            |
         | `\begin{array}{c} |   | \text{CZ} = \mqty |   | `\begin{array}{l} |
         |        \Qcircuit  |   | [ 1 &\textcolor{g |   |                   |
         | @C=1em @R=1em {&  |   | ray}{0}  &\textco |   |  \begin{array}{l} |
         | \ctrl{1} & \qw \\ |   | lor{gray}{0}  &\t |   |                   |
         |                   |   | extcolor{gray}{0} |   |   \text{CZ} \ket{ |
         |                 & |   |    \\\textcolor{g |   | 00} = \ket{00} \\ |
         |  \gate{Z} & \qw } |   | ray}{0}  & 1 &\te |   |                   |
         |                   |   | xtcolor{gray}{0}  |   |   \text{CZ} \ket{ |
         |      \end{array}` |   |   &\textcolor{gra |   | 01} = \ket{01} \\ |
         |                   |   | y}{0}  \\\textcol |   |                   |
         |                   |   | or{gray}{0}  &\te |   |   \text{CZ} \ket{ |
         |                   |   | xtcolor{gray}{0}  |   | 10} = \ket{10} \\ |
         |                   |   |  & 1 &\textcolor{ |   |                   |
         |                   |   | gray}{0}   \\\tex |   |    \text{CZ} \ket |
         |                   |   | tcolor{gray}{0}   |   | {11} = -\ket{11}  |
         |                   |   | &\textcolor{gray} |   |                   |
         |                   |   | {0}  &\textcolor{ |   |       \end{array} |
         |                   |   | gray}{0}  & -1 ]` |   |        \\ \\      |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \begin{array}{l} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |   \text{CZ} \ket{ |
         |                   |   |                   |   | 0b} = \ket{0b} \\ |
         |                   |   |                   |   |         \text     |
         |                   |   |                   |   | {CZ} \ket{1b} = Z |
         |                   |   |                   |   | _2 \ket{1b} = \ke |
         |                   |   |                   |   | t{1}(Z\ket{b}) \\ |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |        \ \ \ \scr |
         |                   |   |                   |   | iptstyle{b = 0,1} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      \end{array}` |
         +-------------------+---+-------------------+---+-------------------+

      .. rubric:: Porta SWAP
         :name: porta-swap
         :class: unnumbered

      A porta SWAP troca o estado de dois qubits, levando
      :math:`\ket{\phi}\ket{\psi}` em :math:`\ket{\psi}\ket{\phi}`.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :math:`\te        |   | :math:            |
         | `\begin{array}{c} |   | xt{SWAP} = \mqty[ |   | `\begin{array}{l} |
         |        \Qcircuit  |   |  1 &\textcolor{gr |   |                   |
         | @C=1em @R=1.5em { |   | ay}{0}   &\textco |   |  \begin{array}{l} |
         | & \qswap & \qw \\ |   | lor{gray}{0}  &\t |   |                   |
         |                   |   | extcolor{gray}{0} |   | \text{SWAP} \ket{ |
         |                   |   |    \\\textcolor{g |   | 00} = \ket{00} \\ |
         |              & \q |   | ray}{0}  &\textco |   |                   |
         | swap \qwx & \qw } |   | lor{gray}{0}   &  |   | \text{SWAP} \ket{ |
         |                   |   | 1 &\textcolor{gra |   | 01} = \ket{10} \\ |
         |      \end{array}` |   | y}{0}  \\\textcol |   |                   |
         |                   |   | or{gray}{0}  & 1  |   | \text{SWAP} \ket{ |
         |                   |   |  &\textcolor{gray |   | 10} = \ket{01} \\ |
         |                   |   | }{0}  &\textcolor |   |                   |
         |                   |   | {gray}{0}  \\\tex |   |   \text{SWAP} \ke |
         |                   |   | tcolor{gray}{0}   |   | t{11} = \ket{11}  |
         |                   |   | &\textcolor{gray} |   |                   |
         |                   |   | {0}  &\textcolor{ |   |       \end{array} |
         |                   |   | gray}{0}   & 1 ]` |   |        \\ \\      |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |  \begin{array}{l} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   | \text{SWAP} \ket{ |
         |                   |   |                   |   | ab} = \ket{ba} \\ |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |    \ \ \ \scripts |
         |                   |   |                   |   | tyle{a,b = 0,1 }  |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |       \end{array} |
         |                   |   |                   |   |                   |
         |                   |   |                   |   |      \end{array}` |
         +-------------------+---+-------------------+---+-------------------+

   .. container:: subsection

      Portas Lógicas de 3 Qubits

      As principais operações em 3 qubits são as portas Toffoli, também
      conhecida por CCNOT (CNOT controlada), e Fredkin, ou CSWAP (SWAP
      controlada).

      .. rubric:: Porta Toffoli ou CCNOT
         :name: porta-toffoli-ou-ccnot
         :class: unnumbered

      A porta Toffoli é uma operação linear que envolve 3 qubits. Dois
      deles funcionam como controle e um, como alvo. O alvo só é
      modificado (pela aplicação da porta :math:`X`) se o estado dos
      dois controles for :math:`\ket{1}\ket{1}`.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :ma               |   | :math:            |
         | `\begin{array}{c} |   | th:`\bmqty{ 1 &\t |   | `\begin{array}{l} |
         |        \Qcircuit  |   | extcolor{gray}{0} |   |                   |
         | @C=1em @R=1em {&  |   |   &\textcolor{gra |   |  \begin{array}{l} |
         | \ctrl{1} & \qw \\ |   | y}{0}  &\textcolo |   |         \te       |
         |                   |   | r{gray}{0}  &\tex |   | xt{CCNOT} \ket{00 |
         |                   |   | tcolor{gray}{0}   |   | c} = \ket{00c} \\ |
         |                &  |   | &\textcolor{gray} |   |         \te       |
         | \ctrl{1} & \qw \\ |   | {0}  &\textcolor{ |   | xt{CCNOT} \ket{01 |
         |                   |   | gray}{0}  &\textc |   | c} = \ket{01c} \\ |
         |                   |   | olor{gray}{0}  \\ |   |         \te       |
         |   & \targ & \qw } |   |                   |   | xt{CCNOT} \ket{10 |
         |                   |   |             \text |   | c} = \ket{10c} \\ |
         |      \end{array}` |   | color{gray}{0}  & |   |                   |
         |                   |   | 1 &\textcolor{gra |   |    \text{CCNOT} \ |
         |                   |   | y}{0}  &\textcolo |   | ket{11c} = \ket{1 |
         |                   |   | r{gray}{0}  &\tex |   | 1\overline{c}} \\ |
         |                   |   | tcolor{gray}{0}   |   |                   |
         |                   |   | &\textcolor{gray} |   |       \ \ \ \scri |
         |                   |   | {0}  &\textcolor{ |   | ptstyle{c = 0,1 \ |
         |                   |   | gray}{0}  &\textc |   |  ; \ \overline{c} |
         |                   |   | olor{gray}{0}  \\ |   |  = \text{NOT}(c)} |
         |                   |   |                   |   |                   |
         |                   |   |             \text |   |       \end{array} |
         |                   |   | color{gray}{0}  & |   |        \\ \\      |
         |                   |   | \textcolor{gray}{ |   |                   |
         |                   |   | 0}  &1 &\textcolo |   |  \begin{array}{l} |
         |                   |   | r{gray}{0}  &\tex |   |                   |
         |                   |   | tcolor{gray}{0}   |   |        \text{CCNO |
         |                   |   | &\textcolor{gray} |   | T} \ket{a,b,c} \\ |
         |                   |   | {0}  &\textcolor{ |   |         \ \ \ \ \ |
         |                   |   | gray}{0}  &\textc |   |  = \ket{a,b,(a\cd |
         |                   |   | olor{gray}{0}  \\ |   | ot b)\oplus c} \\ |
         |                   |   |                   |   |          \        |
         |                   |   |             \text |   |  \ \ \scriptstyle |
         |                   |   | color{gray}{0}  & |   | {a,b,c = 0,1 } \\ |
         |                   |   | \textcolor{gray}{ |   |                   |
         |                   |   | 0}  &\textcolor{g |   |       \ \ \ \scri |
         |                   |   | ray}{0}  &1 &\tex |   | ptstyle{ \cdot \  |
         |                   |   | tcolor{gray}{0}   |   | = \text{AND} } \\ |
         |                   |   | &\textcolor{gray} |   |                   |
         |                   |   | {0}  &\textcolor{ |   |       \ \ \ \scri |
         |                   |   | gray}{0}  &\textc |   | ptstyle{ \oplus = |
         |                   |   | olor{gray}{0}  \\ |   |  \text{XOR} = \te |
         |                   |   |                   |   | xt{adição mod 2}} |
         |                   |   |             \text |   |                   |
         |                   |   | color{gray}{0}  & |   |                   |
         |                   |   | \textcolor{gray}{ |   |       \end{array} |
         |                   |   | 0}  &\textcolor{g |   |                   |
         |                   |   | ray}{0}  &\textco |   |      \end{array}` |
         |                   |   | lor{gray}{0}  &1  |   |                   |
         |                   |   | &\textcolor{gray} |   |                   |
         |                   |   | {0}  &\textcolor{ |   |                   |
         |                   |   | gray}{0}  &\textc |   |                   |
         |                   |   | olor{gray}{0}  \\ |   |                   |
         |                   |   |                   |   |                   |
         |                   |   |             \text |   |                   |
         |                   |   | color{gray}{0}  & |   |                   |
         |                   |   | \textcolor{gray}{ |   |                   |
         |                   |   | 0}  &\textcolor{g |   |                   |
         |                   |   | ray}{0}  &\textco |   |                   |
         |                   |   | lor{gray}{0}  &\t |   |                   |
         |                   |   | extcolor{gray}{0} |   |                   |
         |                   |   |   &1 &\textcolor{ |   |                   |
         |                   |   | gray}{0}  &\textc |   |                   |
         |                   |   | olor{gray}{0}  \\ |   |                   |
         |                   |   |                   |   |                   |
         |                   |   |             \text |   |                   |
         |                   |   | color{gray}{0}  & |   |                   |
         |                   |   | \textcolor{gray}{ |   |                   |
         |                   |   | 0}  &\textcolor{g |   |                   |
         |                   |   | ray}{0}  &\textco |   |                   |
         |                   |   | lor{gray}{0}  &\t |   |                   |
         |                   |   | extcolor{gray}{0} |   |                   |
         |                   |   |   &\textcolor{gra |   |                   |
         |                   |   | y}{0}  &\textcolo |   |                   |
         |                   |   | r{gray}{0}  &1 \\ |   |                   |
         |                   |   |                   |   |                   |
         |                   |   |              \tex |   |                   |
         |                   |   | tcolor{gray}{0}   |   |                   |
         |                   |   | &\textcolor{gray} |   |                   |
         |                   |   | {0}  &\textcolor{ |   |                   |
         |                   |   | gray}{0}  &\textc |   |                   |
         |                   |   | olor{gray}{0}  &\ |   |                   |
         |                   |   | textcolor{gray}{0 |   |                   |
         |                   |   | }  &\textcolor{gr |   |                   |
         |                   |   | ay}{0}  &1 &\text |   |                   |
         |                   |   | color{gray}{0} }` |   |                   |
         +-------------------+---+-------------------+---+-------------------+

      Novamente, quando não houver outra indicação, os qubits de
      controle são o primeiro e o segundo, e o alvo é o terceiro qubit.
      Pode-se especificar os qubits de controle e de alvo por meio de
      índices, como por exemplo:

      .. math::

         \begin{array}{lll}
          \begin{array}{l}  \\ \\ \\ \text{CCNOT}_{1,2,3} \\ (= \text{\footnotesize{CCNOT}}) \end{array}
          & & 
         \Qcircuit @C=10pt @R=10pt {
          & \ctrl{1} & \qw \\
          & \ctrl{1} & \qw \\
          & \targ    & \qw }
         \\ \\
         \begin{array}{l}  \\ \\ \text{CCNOT}_{1,3,2} \end{array}
          & & 
         \Qcircuit @C=10pt @R=10pt {
         & \ctrl{1}  & \qw \\
         & \targ     & \qw \\
         & \ctrl{-1} & \qw }
             \end{array}

      .. rubric:: Porta Fredkin ou CSWAP
         :name: porta-fredkin-ou-cswap
         :class: unnumbered

      A porta de Fredkin possui um qubit de controle e dois alvos. Se o
      controle for :math:`\ket{1}`, uma porta SWAP atua nos alvos.

      .. container:: float

         +-------------------+---+-------------------+---+-------------------+
         | Símbolo           |   | Matriz            |   | Comportamento     |
         +-------------------+---+-------------------+---+-------------------+
         |                   |   |                   |   |                   |
         +-------------------+---+-------------------+---+-------------------+
         | :math:            |   | :ma               |   | :math:            |
         | `\begin{array}{c} |   | th:`\bmqty{ 1 &\t |   | `\begin{array}{l} |
         |                   |   | extcolor{gray}{0} |   |         \te       |
         |     \Qcircuit @C= |   |   &\textcolor{gra |   | xt{CSWAP} \ket{0b |
         | 1em @R=1.5em { &  |   | y}{0}  &\textcolo |   | c} = \ket{0bc} \\ |
         | \ctrl{1} & \qw \\ |   | r{gray}{0}  &\tex |   |         \te       |
         |                   |   | tcolor{gray}{0}   |   | xt{CSWAP} \ket{1b |
         |                   |   | &\textcolor{gray} |   | c} = \ket{1cb} \\ |
         |              & \q |   | {0}  &\textcolor{ |   |                   |
         | swap & \qw     \\ |   | gray}{0}  &\textc |   |    \ \ \ \scripts |
         |                   |   | olor{gray}{0}  \\ |   | tyle{b,c = 0,1 }  |
         |                   |   |                   |   |                   |
         |              & \q |   |             \text |   |      \end{array}` |
         | swap \qwx & \qw } |   | color{gray}{0}  & |   |                   |
         |                   |   | 1 &\textcolor{gra |   |                   |
         |      \end{array}` |   | y}{0}  &\textcolo |   |                   |
         |                   |   | r{gray}{0}  &\tex |   |                   |
         |                   |   | tcolor{gray}{0}   |   |                   |
         |                   |   | &\textcolor{gray} |   |                   |
         |                   |   | {0}  &\textcolor{ |   |                   |
         |                   |   | gray}{0}  &\textc |   |                   |
         |                   |   | olor{gray}{0}  \\ |   |                   |
         |                   |   |                   |   |                   |
         |                   |   |             \text |   |                   |
         |                   |   | color{gray}{0}  & |   |                   |
         |                   |   | \textcolor{gray}{ |   |                   |
         |                   |   | 0}  &1 &\textcolo |   |                   |
         |                   |   | r{gray}{0}  &\tex |   |                   |
         |                   |   | tcolor{gray}{0}   |   |                   |
         |                   |   | &\textcolor{gray} |   |                   |
         |                   |   | {0}  &\textcolor{ |   |                   |
         |                   |   | gray}{0}  &\textc |   |                   |
         |                   |   | olor{gray}{0}  \\ |   |                   |
         |                   |   |                   |   |                   |
         |                   |   |             \text |   |                   |
         |                   |   | color{gray}{0}  & |   |                   |
         |                   |   | \textcolor{gray}{ |   |                   |
         |                   |   | 0}  &\textcolor{g |   |                   |
         |                   |   | ray}{0}  &1 &\tex |   |                   |
         |                   |   | tcolor{gray}{0}   |   |                   |
         |                   |   | &\textcolor{gray} |   |                   |
         |                   |   | {0}  &\textcolor{ |   |                   |
         |                   |   | gray}{0}  &\textc |   |                   |
         |                   |   | olor{gray}{0}  \\ |   |                   |
         |                   |   |                   |   |                   |
         |                   |   |             \text |   |                   |
         |                   |   | color{gray}{0}  & |   |                   |
         |                   |   | \textcolor{gray}{ |   |                   |
         |                   |   | 0}  &\textcolor{g |   |                   |
         |                   |   | ray}{0}  &\textco |   |                   |
         |                   |   | lor{gray}{0}  &1  |   |                   |
         |                   |   | &\textcolor{gray} |   |                   |
         |                   |   | {0}  &\textcolor{ |   |                   |
         |                   |   | gray}{0}  &\textc |   |                   |
         |                   |   | olor{gray}{0}  \\ |   |                   |
         |                   |   |                   |   |                   |
         |                   |   |             \text |   |                   |
         |                   |   | color{gray}{0}  & |   |                   |
         |                   |   | \textcolor{gray}{ |   |                   |
         |                   |   | 0}  &\textcolor{g |   |                   |
         |                   |   | ray}{0}  &\textco |   |                   |
         |                   |   | lor{gray}{0}  &\t |   |                   |
         |                   |   | extcolor{gray}{0} |   |                   |
         |                   |   |   &\textcolor{gra |   |                   |
         |                   |   | y}{0}  &1 &\textc |   |                   |
         |                   |   | olor{gray}{0}  \\ |   |                   |
         |                   |   |                   |   |                   |
         |                   |   |             \text |   |                   |
         |                   |   | color{gray}{0}  & |   |                   |
         |                   |   | \textcolor{gray}{ |   |                   |
         |                   |   | 0}  &\textcolor{g |   |                   |
         |                   |   | ray}{0}  &\textco |   |                   |
         |                   |   | lor{gray}{0}  &\t |   |                   |
         |                   |   | extcolor{gray}{0} |   |                   |
         |                   |   |   &1 &\textcolor{ |   |                   |
         |                   |   | gray}{0}  &\textc |   |                   |
         |                   |   | olor{gray}{0}  \\ |   |                   |
         |                   |   |                   |   |                   |
         |                   |   |             \text |   |                   |
         |                   |   | color{gray}{0}  & |   |                   |
         |                   |   | \textcolor{gray}{ |   |                   |
         |                   |   | 0}  &\textcolor{g |   |                   |
         |                   |   | ray}{0}  &\textco |   |                   |
         |                   |   | lor{gray}{0}  &\t |   |                   |
         |                   |   | extcolor{gray}{0} |   |                   |
         |                   |   |   &\textcolor{gra |   |                   |
         |                   |   | y}{0}  &\textcolo |   |                   |
         |                   |   | r{gray}{0}  &1 }` |   |                   |
         +-------------------+---+-------------------+---+-------------------+

.. container:: section

   Identidades de Circuitos

   Esta seção reúne algumas propriedades das portas lógicas quânticas
   vistas anteriormente. Também são apresentadas identidades úteis para
   se manipular circuitos quânticos.

   .. container:: proposition

      Valem as seguintes relações para as matrizes de Pauli :math:`X`,
      :math:`Y` e :math:`Z`.

      .. math::

         \begin{split}
                 X^2 &= I \\
                 Y^2 &= I \\
                 Z^2 &= I
                \end{split}
                \quad \quad \quad
                \begin{split}
                 XY &= iZ \\
                 YZ &= iX \\
                 ZX &= iY
                \end{split}

   .. container:: proof

      *Proof.* A prova se dá por cálculo direto.

      .. math::

         \begin{split}
               XX &=  \ \sbmqty{ 0 & 1 \\ 1 & 0} \ \sbmqty{ 0 & 1 \\ 1 & 0} \ \ \ = \sbmqty{ 1 & 0 \\ 0 & 1} = I    \\
               YY &= \sbmqty{ 0 & -i \\ i & 0} \sbmqty{ 0 & -i \\ i & 0} = \sbmqty{ 1 & 0 \\ 0 & 1} = I  \\
               ZZ &= \sbmqty{ 1 & 0 \\ 0 & -1} \sbmqty{ 1 & 0 \\ 0 & -1} = \sbmqty{ 1 & 0 \\ 0 & 1} = I 
               \end{split}
               \quad \quad \quad
               \begin{split}
                XY &= \sbmqty{ 0 & 1 \\ 1 & 0} \sbmqty{ 0 & -i \\ i & 0} = \sbmqty{ i &  0 \\ 0 & -i} = i Z \\
                YZ &= \sbmqty{ 0 & -i \\ i & 0} \sbmqty{ 1 & 0 \\ 0 & -1} = \sbmqty{ 0 & i \\ i & 0} = iX   \\
                ZX &= \sbmqty{ 1 & 0 \\ 0 & -1} \sbmqty{ 0 & 1 \\ 1 & 0} = \sbmqty{ 0 & 1 \\ -1 & 0} = iY
               \end{split}

       ◻

   .. container:: proposition

      Fazendo-se a mudança de base de :math:`\ket{0},\ket{1}` para
      :math:`\ket{+},\ket{-}` por meio da matriz :math:`H`, obtém-se
      para as matrizes de Pauli:

      .. math::

         \begin{split}
            HXH &= Z \\
            HYH &= -Y \\
            HZH &= X
           \end{split}

      Portanto, na nova base :math:`\ket{+},\ket{-}`, a matriz do
      operador :math:`X` é :math:`Z`, a matriz de :math:`Y` é :math:`-Y`
      e a matriz de :math:`Z` é :math:`X`.

   .. container:: proof

      *Proof.*

      .. math::

         \begin{split}
              HXH &= \tfrac{1}{\sqrt{2}}\sbmqty{1 & 1 \\ 1 & -1}\sbmqty{ 0 & 1 \\ 1 & 0}\tfrac{1}{\sqrt{2}}\sbmqty{ 1 & 1 \\ 1 & -1} 
                   = \tfrac{1}{2} \sbmqty{1 & 1 \\ 1 & -1} \sbmqty{1 & -1 \\ 1 & 1} \\
                   &= \tfrac{1}{2} \sbmqty{2 & 0 \\ 0 & -2} = \sbmqty{1 & 0 \\ 0 & -1} = Z \\
              HYH &= \tfrac{1}{\sqrt{2}}\sbmqty{1 & 1 \\ 1 & -1}\sbmqty{ 0 & -i \\ i & 0}\tfrac{1}{\sqrt{2}}\sbmqty{ 1 & 1 \\ 1 & -1} 
                   = \tfrac{1}{2} \sbmqty{1 & 1 \\ 1 & -1} \sbmqty{-i & i \\ i & i} \\
                  &= \tfrac{1}{2} \sbmqty{0 & 2i \\ -2i & 0} = -\sbmqty{0 & -i \\ i & 0} = -Y \\
              HZH &= \tfrac{1}{\sqrt{2}}\sbmqty{1 & 1 \\ 1 & -1}\sbmqty{ 1 & 0 \\ 0 & -1}\tfrac{1}{\sqrt{2}}\sbmqty{ 1 & 1 \\ 1 & -1} 
                   = \tfrac{1}{2} \sbmqty{1 & 1 \\ 1 & -1} \sbmqty{1 & 1 \\ -1 & 1} \\
                  &= \tfrac{1}{2} \sbmqty{0 & 2 \\ 2 & 0} = \sbmqty{0 & 1 \\ 1 & 0} = X 
             \end{split}

       ◻

   .. container:: proposition

      A porta de Hadamard é sua própria inversa.

      .. math:: H^2 = I

   .. container:: proof

      *Proof.* Pode-se verificar fazendo a conta com matrizes
      diretamente.

      .. math::

         \begin{split}
              HH &=   \tfrac{1}{\sqrt{2}}\sbmqty{1 & 1 \\ 1 & -1} \tfrac{1}{\sqrt{2}}\sbmqty{1 & 1 \\ 1 & -1} 
                  =  \tfrac{1}{2}\sbmqty{2 & 0 \\ 0 & 2} = I \\
             \end{split}

      Outra maneira de se verificar isso é perceber que :math:`H` é
      hermitiana (:math:`H^\dagger = H`) e unitária
      (:math:`H^{-1} = H^\dagger`), de forma que
      :math:`H^{-1} = H^\dagger = H`. ◻

   .. container:: proposition

      Vale que:

      .. math:: T^2 = S

   .. container:: proof

      *Proof.*
      :math:`T^2 =  \sbmqty{ 1 & 0 \\ 1 & e^{i\frac{\pi}{4}}} \sbmqty{ 1 & 0 \\ 1 & e^{i\frac{\pi}{4}}} 
              = \sbmqty{ 1 & 0 \\ 1 & e^{i\frac{\pi}{2}}} = 
              = \sbmqty{ 1 & 0 \\ 1 & i} = S` ◻

   .. container:: proposition

      As portas CNOT e SWAP são suas próprias inversas.

      .. math:: \text{\normalfont CNOT}^2 = I \quad \quad \quad \text{\normalfont SWAP}^2 = I

   .. container:: proof

      *Proof.* Ambas operações são hermitianas (em particular, são
      matrizes de coeficientes reais e simétricas) e unitárias (as
      colunas são vetores ortonormais), portanto vale o mesmo argumento
      dado para :math:`H`:

      .. math::

         \begin{split}
            &\text{CNOT}^{\dagger} = \text{CNOT} \ , \ \text{CNOT}^{-1} = \text{CNOT}^{\dagger} \\
            &\text{SWAP}^{\dagger} = \text{SWAP} \ , \ \text{SWAP}^{-1} = \text{SWAP}^{\dagger}
           \end{split}
           \implies
           \begin{split}
            \text{CNOT}^{-1} &= \text{CNOT}^{\dagger} = \text{CNOT} \\
            \text{SWAP}^{-1} &= \text{SWAP}^{\dagger} = \text{SWAP}
           \end{split}

       ◻

   .. container:: proposition

      É possível intercambiar alvo e controle na porta :math:`Z`
      controlada.

      .. math::

         \Qcircuit @C=10pt @R=10pt { & \ctrl{2} & \qw      & &   & & & \gate{Z}    & \qw \\
                                     &          &          & & = & & &             &     \\
                                     & \gate{Z} & \qw      & &   & & & \ctrl{-2}   & \qw }

   .. container:: proof

      *Proof.* Para mostrar que dois operadores são iguais, basta
      verificar que fornecem mesmo resultado nos vetores da base
      computacional.

      .. math::

         \begin{array}{lll}
              \ket{00} & \xrightarrow{CZ_{1,2}} & \ket{00} \\
              \ket{01} & \xrightarrow{CZ_{1,2}} & \ket{01} \\
              \ket{10} & \xrightarrow{CZ_{1,2}} & Z_2\ket{10} = \ket{10} \\
              \ket{11} & \xrightarrow{CZ_{1,2}} & Z_2\ket{11} = - \ket{11} \ ,
             \end{array}

      .. math::

         \begin{array}{lll}
              \ket{00} & \xrightarrow{CZ_{2,1}} & \ket{00} \\
              \ket{01} & \xrightarrow{CZ_{2,1}} & Z_1 \ket{01} = \ket{01} \\
              \ket{10} & \xrightarrow{CZ_{2,1}} & \ket{10} \\
              \ket{11} & \xrightarrow{CZ_{2,1}} & Z_1\ket{11} = -\ket{11} \ . 
             \end{array}

      Isso implica igualdade entre os operadores :math:`CZ_{1,2}` e
      :math:`CZ_{2,1}`. ◻

   .. container:: proposition

      A porta CNOT pode ser obtida usando-se uma porta :math:`Z`
      controlada:

      .. math::

         \Qcircuit @C=10pt @R=10pt {&  \qw      & \ctrl{2} & \qw      & \qw & &   & & & \ctrl{2}    & \qw \\
                                    &           &          &          &     & & = & & &             &   \\
                                    &  \gate{H} & \gate{Z} & \gate{H} & \qw & &   & & & \targ       & \qw }

   .. container:: proof

      *Proof.* Os dois circuitos representam operadores lineares em 2
      qubits. É suficiente, pois, verificar que o comportamento dos dois
      circuitos é o mesmo na base computacional, pois as transformações
      lineares são especificadas de forma única por seu resultado em uma
      base qualquer (ver seção
      `[cap2:sec_def_tl_nos_elementos_da_base] <#cap2:sec_def_tl_nos_elementos_da_base>`__).
      Nesta demonstração, também serão usadas as propriedades
      :math:`HH=I` e :math:`HZH = X`, verificadas nas proposições
      `[cap4:prop_matr_pauli_e_hadamard] <#cap4:prop_matr_pauli_e_hadamard>`__
      e `[cap4:prop_inversa_hadamard] <#cap4:prop_inversa_hadamard>`__.

      De fato, analisando o efeito do circuito para os vetores da base
      computacional, obtém-se o seguinte. Seja :math:`b = 0,1`. Se o
      primeiro qubit for :math:`\ket{0}`, a porta :math:`Z` controlada
      não surtirá efeito no segundo qubit, de forma que

      .. math:: \ket{\psi_f}_{1,2} = H_2 H_2 \ket{0b} = I_2\ket{0b} = \ket{0b} \ .

      Se o primeiro qubit for :math:`\ket{1}`, a porta :math:`Z`
      controlada atuará, resultando em

      .. math:: \ket{\psi_f}_{1,2} = H_2 Z_2 H_2 \ket{1b} = X_2 \ket{1b} = \ket{1\overline{b}} \ .

      O efeito final para esses vetores da base é o mesmo que o de uma
      porta CNOT, dessa forma, os dois circuitos do enunciado são
      equivalentes. ◻

   .. container:: proposition

      A porta SWAP pode ser construída por 3 portas CNOT:

      .. math::

         \Qcircuit @C=10pt @R=10pt {& \ctrl{2} & \targ     & \ctrl{2} & \qw & &   & & & \qswap      & \qw  & &   & & & \targ    & \ctrl{2}  & \targ    & \qw \\
                                    &           &          &          &     & & = & & &   \qwx      &      & & = & & &          &           &          &     \\
                                    & \targ    & \ctrl{-2} & \targ    & \qw & &   & & & \qswap \qwx & \qw  & &   & & & \ctrl{-2}& \targ     & \ctrl{-2}& \qw  }

   .. container:: proof

      *Proof.* Verificando a primeira igualdade nos vetores da base
      computacional:

      .. math::

         \begin{array}{ccccccc}
               \ket{00} & \xrightarrow{\text{CNOT}_{1,2}} & \ket{00} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{00} 
                        & \xrightarrow{\text{CNOT}_{1,2}}  & \ket{00} \\
               \ket{01} & \xrightarrow{\text{CNOT}_{1,2}} & \ket{01} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{11} 
                        & \xrightarrow{\text{CNOT}_{1,2}}  & \ket{10} \\
               \ket{10} & \xrightarrow{\text{CNOT}_{1,2}} & \ket{11} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{01} 
                        & \xrightarrow{\text{CNOT}_{1,2}}  & \ket{01} \\
               \ket{11} & \xrightarrow{\text{CNOT}_{1,2}} & \ket{10} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{10} 
                        & \xrightarrow{\text{CNOT}_{1,2}}  & \ket{11} \\
              \end{array}

      Verificando a segunda igualdade nos vetores da base computacional:

      .. math::

         \begin{array}{ccccccc}
               \ket{00} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{00} & \xrightarrow{\text{CNOT}_{1,2}} & \ket{00} 
                        & \xrightarrow{\text{CNOT}_{2,1}}  & \ket{00} \\
               \ket{01} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{11} & \xrightarrow{\text{CNOT}_{1,2}} & \ket{10} 
                        & \xrightarrow{\text{CNOT}_{2,1}}  & \ket{10} \\
               \ket{10} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{10} & \xrightarrow{\text{CNOT}_{1,2}} & \ket{11} 
                        & \xrightarrow{\text{CNOT}_{2,1}}  & \ket{01} \\
               \ket{11} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{01} & \xrightarrow{\text{CNOT}_{1,2}} & \ket{01} 
                        & \xrightarrow{\text{CNOT}_{2,1}}  & \ket{11} \\
              \end{array}

      Percebe-se, então, que o efeito dessas sequências de portas CNOT é
      o mesmo de uma porta SWAP. ◻

   .. container:: proposition

      Dependendo da base considerada, os papeis de controle e alvo da
      porta CNOT se invertem. Mais precisamente, vale a seguinte
      igualdade de circuitos:

      .. math::

         \Qcircuit @C=10pt @R=10pt {& \gate{H}  & \targ     & \gate{H}  & \qw & &   & & & \ctrl{2} & \qw \\
                                    &           &           &           &     & & = & & &          &   \\
                                    & \gate{H}  & \ctrl{-2} & \gate{H}  & \qw & &   & & & \targ    & \qw }

   .. container:: proof

      *Proof.* Primeiramente, perceba que o efeito da porta Hadamard é
      de mudança de base saindo de
      :math:`\mathcal{I} = \big\{ \ket{0},\ket{1}\big\}` para
      :math:`\mathcal{X} = \big\{ \ket{+},\ket{-} \big\}` e vice versa,
      portanto para 2 qubits vale:

      .. math::

         \tag{$\ast$}\label{cap4:eq_mudanca_de_base_HH}
              \begin{array}{ccccl}
               \ket{00} & \xrightarrow{H_1 H_2} & \ket{++} & \xrightarrow{H_1 H_2} & \ket{00} \\
               \ket{01} & \xrightarrow{H_1 H_2} & \ket{+-} & \xrightarrow{H_1 H_2} & \ket{01} \\
               \ket{10} & \xrightarrow{H_1 H_2} & \ket{-+} & \xrightarrow{H_1 H_2} & \ket{10} \\
               \ket{11} & \xrightarrow{H_1 H_2} & \ket{--} & \xrightarrow{H_1 H_2} & \ket{11} \ . \\
              \end{array}

      Aplicando-se :math:`\text{CNOT}_{2,1}` à base
      :math:`\mathcal{X}\otimes \mathcal{X} = \big\{ \ket{++},\ket{+-},\ket{-+},\ket{--}\big\}`,
      obtém-se que:

      .. math::

         \begin{split}
               \text{CNOT}_{2,1}\ket{++} 
               &= \text{CNOT}_{2,1} \frac{1}{2} \big( \ket{00} + \ket{01} + \ket{10} + \ket{11} \big) \\
               &=  \frac{1}{2} \big( \text{CNOT}_{2,1}\ket{00} + \text{CNOT}_{2,1}\ket{01} + \text{CNOT}_{2,1}\ket{10} + \text{CNOT}_{2,1}\ket{11} \big) \\
               &= \frac{1}{2} \big(\ket{00} + \ket{11} + \ket{10} + \ket{01} \big) \\
               &= \frac{1}{2} \big(\ket{0}(\ket{0}+\ket{1}) + \ket{1}(\ket{0} + \ket{1} \big) \\
               &= \frac{1}{2} (\ket{0} + \ket{1})(\ket{0}+\ket{1}) = \ket{++} \ ,
              \end{split}

      .. math::

         \begin{split}
               \text{CNOT}_{2,1}\ket{+-} 
               &= \text{CNOT}_{2,1} \frac{1}{2} \big( \ket{00} - \ket{01} + \ket{10} - \ket{11} \big) \\
               &=  \frac{1}{2} \big( \text{CNOT}_{2,1}\ket{00} - \text{CNOT}_{2,1}\ket{01} + \text{CNOT}_{2,1}\ket{10} - \text{CNOT}_{2,1}\ket{11} \big) \\
               &= \frac{1}{2} \big(\ket{00} - \ket{11} + \ket{10} - \ket{01} \big) \\
               &= \frac{1}{2} \big(\ket{0}(\ket{0}-\ket{1}) + \ket{1}(\ket{0} - \ket{1} \big) \\
               &= \frac{1}{2} (\ket{0} + \ket{1})(\ket{0}-\ket{1}) = \ket{+-} \ ,
              \end{split}

      .. math::

         \begin{split}
               \text{CNOT}_{2,1}\ket{-+} 
               &= \text{CNOT}_{2,1} \frac{1}{2} \big( \ket{00} + \ket{01} - \ket{10} - \ket{11} \big) \\
               &=  \frac{1}{2} \big( \text{CNOT}_{2,1}\ket{00} + \text{CNOT}_{2,1}\ket{01} - \text{CNOT}_{2,1}\ket{10} - \text{CNOT}_{2,1}\ket{11} \big) \\
               &= \frac{1}{2} \big(\ket{00} + \ket{11} - \ket{10} - \ket{01} \big) \\
               &= \frac{1}{2} \big(\ket{0}(\ket{0}-\ket{1}) - \ket{1}(\ket{0} - \ket{1} \big) \\
               &= \frac{1}{2} (\ket{0} - \ket{1})(\ket{0}-\ket{1}) = \ket{--} \ ,
              \end{split}

      .. math::

         \begin{split}
               \text{CNOT}_{2,1}\ket{--} 
               &= \text{CNOT}_{2,1} \frac{1}{2} \big( \ket{00} - \ket{01} - \ket{10} + \ket{11} \big) \\
               &=  \frac{1}{2} \big( \text{CNOT}_{2,1}\ket{00} - \text{CNOT}_{2,1}\ket{01} - \text{CNOT}_{2,1}\ket{10} + \text{CNOT}_{2,1}\ket{11} \big) \\
               &= \frac{1}{2} \big(\ket{00} - \ket{11} - \ket{10} + \ket{01} \big) \\
               &= \frac{1}{2} \big(\ket{0}(\ket{0}+\ket{1}) - \ket{1}(\ket{0} + \ket{1} \big) \\
               &= \frac{1}{2} (\ket{0} - \ket{1})(\ket{0}+\ket{1}) = \ket{-+} \ .
              \end{split}

      Pode-se resumir o resultado dessas contas na seguinte expressão:

      .. math::

         \tag{$\ast \ast$}\label{cap4:eq_cnot_in_basis_xx}
             \begin{array}{ccc}
               \ket{+b} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{+b}            \\
               \ket{-b} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{+\overline{b}} 
              \end{array}
              \quad \quad
              \scriptstyle{
              \begin{array}{l}
               b \in \{ +,- \} \\
               \overline{+} := -  \\
               \overline{-} := +
              \end{array} }

      Isto é, a porta :math:`\text{CNOT}_{2,1}` atuando na base
      :math:`\mathcal{X} \otimes \mathcal{X}` tem o mesmo efeito da
      porta :math:`\text{CNOT}_{1,2}` atuando na base
      :math:`\mathcal{I}\otimes \mathcal{I}`, substituindo-se :math:`0`
      por :math:`+` e :math:`1` por :math:`-`.

      Portanto, usando-se
      `[cap4:eq_mudanca_de_base_HH] <#cap4:eq_mudanca_de_base_HH>`__ e
      `[cap4:eq_cnot_in_basis_xx] <#cap4:eq_cnot_in_basis_xx>`__, o
      circuito da esquerda do enunciado da proposição fornece

      .. math::

         \begin{array}{ccccccl}
               \ket{00} & \xrightarrow{H_1 H_2} & \ket{++} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{++}  & \xrightarrow{H_1 H_2} & \ket{00} \\
               \ket{01} & \xrightarrow{H_1 H_2} & \ket{+-} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{+-}  & \xrightarrow{H_1 H_2} & \ket{01} \\
               \ket{10} & \xrightarrow{H_1 H_2} & \ket{-+} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{--}  & \xrightarrow{H_1 H_2} & \ket{11} \\
               \ket{11} & \xrightarrow{H_1 H_2} & \ket{--} & \xrightarrow{\text{CNOT}_{2,1}} & \ket{-+}  & \xrightarrow{H_1 H_2} & \ket{10} \ , \\
              \end{array}

      resultando no mesmo efeito de uma :math:`\text{CNOT}_{1,2}`. ◻

   .. container:: proposition

      É possível implementar uma CNOT com alvo e controle distantes com
      CNOTs entre qubits adjacentes.

      .. math::

         \Qcircuit @C=10pt @R=10pt {
            & \qw      & \ctrl{1} & \qw      & \ctrl{1} & \qw  & &   & & & \ctrl{2} & \qw \\
            & \ctrl{1} & \targ    & \ctrl{1} & \targ    & \qw  & & = & & &  \qw     & \qw \\
            & \targ    & \qw      & \targ    & \qw      & \qw  & &   & & & \targ    & \qw }

   .. container:: proof

      *Proof.* Sejam :math:`a,b,c = 0,1`. Aplicando a sequência de CNOTs
      adjacentes mostradas no enunciado da proposição, obtém-se:

      .. math::

         \begin{split}
               \ket{a,b,c}
               &\xrightarrow{\text{CNOT}_{2,3}} \ket{a,b,b\oplus c} \\
               &\xrightarrow{\text{CNOT}_{1,2}} \ket{a,a\oplus b,b\oplus c} \\
               &\xrightarrow{\text{CNOT}_{2,3}} \ket{a,a\oplus b,a\oplus b\oplus b\oplus c} \\
               &\phantom{\xrightarrow{\text{CNOT}_{2,3}}}\ = \ \ket{a,a\oplus b,a\oplus c} \\
               &\xrightarrow{\text{CNOT}_{1,2}} \ket{a,a\oplus a\oplus b,a\oplus c} \\
               & \phantom{\xrightarrow{\text{CNOT}_{1,2}} } \ = \ \ket{a,a\oplus a\oplus b,a\oplus c}  = \text{CNOT}_{1,3} \ket{a,b,c} \ .
              \end{split}

      Isso prova a igualdade de circuitos apresentada no enunciado. ◻

   .. container:: proposition

      Considere a porta CNOT com controle no qubit 1 e alvo no qubit 2.
      Valem as seguintes identidades de circuitos:

      .. math::

         \begin{split}
             \text{\normalfont CNOT\,\,} X_1 \, \text{\normalfont CNOT\,} &= X_1 X_2 \\
             \text{\normalfont CNOT\,\,} Y_1 \, \text{\normalfont CNOT\,} &= Y_1 X_2 \\
             \text{\normalfont CNOT\,\,} Z_1 \, \text{\normalfont CNOT\,} &= Z_1 \\
             \text{\normalfont CNOT\,\,} X_2 \, \text{\normalfont CNOT\,} &= X_2 \\
             \text{\normalfont CNOT\,\,} Y_2 \, \text{\normalfont CNOT\,} &= Z_1 Y_2 \\
             \text{\normalfont CNOT\,\,} Z_2 \, \text{\normalfont CNOT\,} &= Z_1 Z_2  \ . 
            \end{split}

      Graficamente, as igualdades ficam:

      .. math::

         \begin{array}{c}
         \Qcircuit @C=5pt @R=3pt 
         {& \ctrl{2}  & \gate{X} & \ctrl{2}  & \qw & &   & & & \gate{X} & \qw & & & & & & & \ctrl{2}  & \qw      & \ctrl{2}  & \qw & &   & & & \qw      & \qw \\
          &           &          &           &     & & = & & &          &     & & & & & & &           &          &           &     & & = & & &          &   \\
          & \targ     & \qw      & \targ     & \qw & &   & & & \gate{X} & \qw & & & & & & & \targ     & \gate{X} & \targ     & \qw & &   & & & \gate{X} & \qw } 
          \\ \\
         \Qcircuit @C=5pt @R=3pt 
         {& \ctrl{2}  & \gate{Y} & \ctrl{2}  & \qw & &   & & & \gate{Y} & \qw & & & & & & & \ctrl{2}  & \qw      & \ctrl{2}  & \qw & &   & & & \gate{Z} & \qw \\
          &           &          &           &     & & = & & &          &     & & & & & & &           &          &           &     & & = & & &          &   \\
          & \targ     & \qw      & \targ     & \qw & &   & & & \gate{X} & \qw & & & & & & & \targ     & \gate{Y} & \targ     & \qw & &   & & & \gate{Y} & \qw } 
          \\ \\
         \Qcircuit @C=5pt @R=3pt 
         {& \ctrl{2}  & \gate{Z} & \ctrl{2}  & \qw & &   & & & \gate{Z} & \qw & & & & & & & \ctrl{2}  & \qw      & \ctrl{2}  & \qw & &   & & & \gate{Z} & \qw \\
          &           &          &           &     & & = & & &          &     & & & & & & &           &          &           &     & & = & & &          &   \\
          & \targ     & \qw      & \targ     & \qw & &   & & & \qw      & \qw & & & & & & & \targ     & \gate{Z} & \targ     & \qw & &   & & & \gate{Z} & \qw } 
             \end{array}

   .. container:: proof

      | *Proof.* A prova dessas igualdades é essencialmente verificar
        que os circuitos têm o mesmo efeito na base computacional.
      | :math:`\text{\normalfont CNOT\,\,} X_1 \, \text{\normalfont CNOT\,} = X_1 X_2`
      | 

        .. math::

           \begin{array}{ccccccc}
                 \ket{0b} & \xrightarrow{\text{CNOT}} & \ket{0b} & \xrightarrow{X_1} & \ket{1b} 
                          & \xrightarrow{\text{CNOT}}  & \ket{1\overline{b}} = X_1 X_2 \ket{0b} \\
                 \ket{1b} & \xrightarrow{\text{CNOT}} & \ket{1\overline{b}} & \xrightarrow{X_1} & \ket{0\overline{b}} 
                          & \xrightarrow{\text{CNOT}}  & \ket{0\overline{b}} = X_1 X_2 \ket{1b} \\
                \end{array}

      | :math:`\text{\normalfont CNOT\,\,} X_2 \, \text{\normalfont CNOT\,} = X_2`
      | 

        .. math::

           \begin{array}{ccccccc}
                 \ket{0b} & \xrightarrow{\text{CNOT}} & \ket{0b} & \xrightarrow{X_2} & \ket{0\overline{b}} 
                          & \xrightarrow{\text{CNOT}}  & \ket{0\overline{b}} =  X_2 \ket{0b} \\
                 \ket{1b} & \xrightarrow{\text{CNOT}} & \ket{1\overline{b}} & \xrightarrow{X_2} & \ket{1b} 
                          & \xrightarrow{\text{CNOT}}  & \ket{1\overline{b}} = X_2 \ket{1b} \\
                \end{array}

      | :math:`\text{\normalfont CNOT\,\,} Y_1 \, \text{\normalfont CNOT\,} = Y_1 X_2`
      | 

        .. math::

           \begin{array}{ccccccc}
                 \ket{0b} & \xrightarrow{\text{CNOT}} & \ket{0b} & \xrightarrow{Y_1} & i \ket{1b} 
                          & \xrightarrow{\text{CNOT}}  & i \ket{1\overline{b}} = Y_1 X_2 \ket{0b} \\
                 \ket{1b} & \xrightarrow{\text{CNOT}} & \ket{1\overline{b}} & \xrightarrow{Y_1} & -i\ket{0\overline{b}} 
                          & \xrightarrow{\text{CNOT}}  & -i \ket{0\overline{b}} = Y_1 X_2 \ket{1b} \\
                \end{array}

      | :math:`\text{\normalfont CNOT\,\,} Y_2 \, \text{\normalfont CNOT\,} = Z_1 Y_2`
      | 

        .. math::

           \begin{array}{ccccccc}
                 \ket{0b} & \xrightarrow{\text{CNOT}} & \ket{0b} & \xrightarrow{Y_2} & (-1)^b i \ket{0\overline{b}} 
                          & \xrightarrow{\text{CNOT}}  & (-1)^b i \ket{0\overline{b}} = Z_1 Y_2 \ket{0b} \\
                 \ket{1b} & \xrightarrow{\text{CNOT}} & \ket{1\overline{b}} & \xrightarrow{Y_2} & -(-1)^{b} i\ket{1b} 
                          & \xrightarrow{\text{CNOT}}  & -(-1)^{b} i\ket{1\overline{b}} = Z_1 Y_2 \ket{1b} \\
                \end{array}

        Observação: pode-se escrever
        :math:`Y\ket{b} = (-1)^b i \ket{\overline{b}}` e
        :math:`Y\ket{\overline{b}} = -(-1)^b i \ket{b}`.

      | :math:`\text{\normalfont CNOT\,\,} Z_1 \, \text{\normalfont CNOT\,} = Z_1`
      | 

        .. math::

           \begin{array}{ccccccc}
                 \ket{0b} & \xrightarrow{\text{CNOT}} & \ket{0b} & \xrightarrow{Z_1} & \ket{0b}
                          & \xrightarrow{\text{CNOT}}  & \ket{0b} = Z_1 \ket{0b} \\
                 \ket{1b} & \xrightarrow{\text{CNOT}} & \ket{1\overline{b}} & \xrightarrow{Z_1} & -\ket{1\overline{b}} 
                          & \xrightarrow{\text{CNOT}}  & -\ket{1b} = Z_1 \ket{1b} \\
                \end{array}

      | :math:`\text{\normalfont CNOT\,\,} Z_2 \, \text{\normalfont CNOT\,} = Z_1 Z_2`
      | 

        .. math::

           \begin{array}{ccccccc}
                 \ket{0b} & \xrightarrow{\text{CNOT}} & \ket{0b} & \xrightarrow{Z_2} & (-1)^b \ket{0b} 
                          & \xrightarrow{\text{CNOT}}  & (-1)^b \ket{0b} =  Z_1 Z_2 \ket{0b} \\
                 \ket{1b} & \xrightarrow{\text{CNOT}} & \ket{1\overline{b}} & \xrightarrow{Z_2} & -(-1)^b \ket{1\overline{b}} 
                          & \xrightarrow{\text{CNOT}}  & -(-1)^b \ket{1b}  = Z_1 Z_2\ket{1b} \\
                \end{array}

        Observação: pode-se escrever :math:`Z\ket{b} = (-1)^b \ket{b}` e
        :math:`Z\ket{\overline{b}} = -(-1)^b  \ket{\overline{b}}`.

      Dessa forma, os circuitos do enunciado têm o mesmo resultado na
      base computacional, e, portanto, são equivalentes. ◻

   .. container:: proposition

      As portas Toffoli e Fredkin são suas próprias inversas.

      .. math:: \text{\normalfont CCNOT }^2 = I \quad \quad \quad \text{\normalfont CSWAP}^2 = I

   .. container:: proof

      *Proof.* Análoga à prova da proposição
      `[cap4:prop_inversas_cnot_swap] <#cap4:prop_inversas_cnot_swap>`__. ◻

   .. container:: proposition

      A porta Fredkin pode ser obtida com 3 portas Toffoli:

      .. math::

         \Qcircuit @C=20pt @R=10pt {& \ctrl{2}  &  \ctrl{2} & \ctrl{2}  & \qw & &   & & & \ctrl{2}    & \qw \\
                                    &           &           &           &     & &   & & &             &     \\
                                    & \ctrl{2}  & \targ     & \ctrl{2}  & \qw & & = & & & \qswap      & \qw \\
                                    &           &           &           &     & &   & & &   \qwx      &     \\
                                    & \targ     & \ctrl{-2} & \targ     & \qw & &   & & & \qswap \qwx & \qw  }

   .. container:: proof

      *Proof.* Basta verificar a igualdade para os vetores da base
      computacional. Sejam :math:`b,c = 0,1`.

      Para os vetores da forma :math:`\ket{0bc}`, a sequência de portas
      Toffoli não afeta o vetor. O resultado continua sendo
      :math:`\ket{0bc}` pois só há modificação quando o AND dos
      controles for 1 (o que não ocorre em nenhuma das portas Toffoli
      nesse caso).

      Já para os vetores da forma :math:`\ket{1bc}`, as portas Toffoli
      se ativam dependendo do valor de :math:`b` e :math:`c`. O
      comportamento das portas Toffoli fica semelhante ao das portas
      CNOT da proposição
      `[cap4:prop_SWAP_CNOT] <#cap4:prop_SWAP_CNOT>`__ nesse caso em que
      o primeiro qubit se encontra no estado :math:`\ket{1}`. E esse
      comportamento, pela referida proposição, é o mesmo de uma porta
      SWAP.

      É possível perceber, portanto, que o conjunto de portas Toffoli no
      enunciado funcionam como um SWAP controlado (isto é, uma porta
      Fredkin): quando o primeiro qubit é :math:`\ket{0}`, nada
      acontece, e quando o primeiro qubit é :math:`\ket{1}`, os dois
      últimos sofrem um efeito igual à aplicação de uma porta SWAP.
      Logo, verifica-se a equivalência dos circuitos no enunciado da
      proposição. ◻

   .. container:: proposition

      A porta Toffoli pode ser obtida pela seguinte combinação de portas
      lógicas de 1 e 2 qubits:

      .. math::

         \Qcircuit @C=6pt @R=8pt 
         {& \ctrl{2}  &\qw & &   & & & \qw      & \qw      & \qw           & \ctrl{4} & \qw      & \qw      & \qw           & \ctrl{4} & \qw           & \ctrl{2} & \qw           & \ctrl{2} & \gate{T}& \qw   \\
          &           &    & &   & & &          &          &               &          &          &          &               &          &               &          &               &          &         &       \\
          & \ctrl{2}  &\qw & & = & & & \qw      & \ctrl{2} & \qw           &  \qw     & \qw      & \ctrl{2} & \qw           & \qw      & \gate{T^\dagger} & \targ    & \gate{T^\dagger} & \targ    & \gate{S}& \qw   \\
          &           &    & &   & & &          &          &               &          &          &          &               &          &               &          &               &          &         &       \\
          & \targ     &\qw & &   & & & \gate{H} & \targ    & \gate{T^\dagger} &   \targ  & \gate{T} & \targ    & \gate{T^\dagger} & \targ    & \gate{T}      & \gate{H} & \qw           & \qw      & \qw     & \qw   }

   .. container:: proof

      *Proof.* Pode-se verificar essa construção avaliando o efeito nos
      elementos da base computacional. Sejam :math:`b,c = 0,1`.

      Caso o primeiro qubit seja :math:`\ket{0}`, o circuito
      simplifica-se para:

      .. math::

         \Qcircuit @C=3pt @R=2pt                                                                                                                                                                                                                                                                                            
         {\lstick{\ket{0}} & \qw      & \qw      & \qw            & \qw                                     & \qw      & \qw            & \qw            & \qw           & \gate{T}\gategroup{1}{10}{1}{10}{4pt}{--} & \qw &  &  &   &  &  &        & \qw    & \qw    & \qw                                    & \qw         & \qw                                       & \qw                                       & \qw &  &  &   &  &  &  & \qw  & \qw & \qw & \rstick{\ket{0}}  \\
                           &          &          &                &                                         &          &                &                &               &                                           &     &  &  &   &  &  &        &        &        &                                        &             &                                           &                                           &     &  &  &   &  &  &  &      &     &     &                   \\
          \lstick{\ket{b}} & \qw      & \ctrl{2} & \qw            & \qw                                     & \ctrl{2} & \qw            & \gate{T^\dagger}  & \gate{T^\dagger} & \gate{S}                                  & \qw &  &  & = &  &  &        & \qw    &\ctrl{2}&\ctrl{2}                                &\gate{T^\dagger}&\gate{T^\dagger}                              &\gate{S} \gategroup{3}{21}{3}{23}{4pt}{--} & \qw &  &  & = &  &  &  & \qw  & \qw & \qw & \rstick{\ket{b}}  \\
                           &          &          &                &                                         &          &                &                &               &                                           &     &  &  &   &  &  &        &        &        &                                        &             &                                           &                                           &     &  &  &   &  &  &  &      &     &     &                   \\
          \lstick{\ket{c}} & \gate{H} & \targ    & \gate{T^\dagger}  & \gate{T}\gategroup{5}{4}{5}{5}{4pt}{--} & \targ    & \gate{T^\dagger}  & \gate{T}       & \gate{H}      & \qw                                       & \qw &  &  &   &  &  &        &\gate{H}&\targ   &\targ \gategroup{3}{19}{5}{20}{4pt}{--} &\gate{T^\dagger}&\gate{T} \gategroup{5}{21}{5}{22}{4pt}{--} &\gate{H}                                   & \qw &  &  &   &  &  &  & \qw  & \qw & \qw & \rstick{\ket{c}}  }
                   %   1   &    2     &    3     &   4            &                    5                    &     6    &    7           &      8         &      9        &   10                                      & 11  &12&13&14 %15&16&   17   &  18    &    19  & 20                                     &  21         &  22                                       &  23                                       &  24 &  &  &   &  &  &  &

      Algumas simplificações estão destacadas nas figuras e correspondem
      a:

      .. math:: T\ket{0} = \ket{0} \ , \ \ \text{CNOT}^2 = I \ , \ \ T T^\dagger = I \ , \ \ T^\dagger T^\dagger S = \big( T^\dagger \big)^2 T^2 = I \ , \ \ HH = I \ .

      Agora será checado o caso em que os dois primeiros qubits são
      :math:`\ket{10}`. Nesse caso, o circuito original reduz-se a:

      .. math::

         \Qcircuit @C=3pt @R=2pt                                                                                                                                                                                                                                                                                            
         {\lstick{\ket{1}} & \qw      & \qw          & \qw      & \qw      & \qw           & \qw       & \qw            & \qw                                       & \gate{T} \gategroup{1}{10}{1}{10}{4pt}{--} & \qw &  &  &   &  &  &  &     \qw  & \gate{T}  & \qw  &  &  &   &  &  &  & \qw  & \qw  & \qw  & \rstick{e^{i\frac{\pi}{4}}\ket{1}}  \\
                           &          &              &          &          &               &           &                &                                           &                                            &     &  &  &   &  &  &  &          &                             &      &  &  &   &  &  &  &      &      &      &  \\
          \lstick{\ket{0}} & \qw      & \qw          & \qw      & \qw      & \gate{T^\dagger} & \gate{X}  & \gate{T^\dagger}  & \gate{X} \gategroup{3}{6}{3}{10}{4pt}{--} & \gate{S}                                   & \qw &  &  & = &  &  &  &     \qw  & \gate{e^{-i\frac{\pi}{4}}S} & \qw  &  &  & = &  &  &  & \qw  & \qw  & \qw  & \rstick{e^{-i\frac{\pi}{4}}\ket{0}} \\
                           &          &              &          &          &               &           &                &                                           &                                            &     &  &  &   &  &  &  &          &                             &      &  &  &   &  &  &  &      &      &      &  \\
          \lstick{\ket{c}} & \gate{H} & \gate{T^\dagger}& \gate{X} & \gate{T} & \gate{T^\dagger} & \gate{X}  & \gate{T}       & \gate{H} \gategroup{5}{2}{5}{9}{4pt}{--}  & \qw                                        & \qw &  &  &   &  &  &  &     \qw  & \qw                         & \qw  &  &  &   &  &  &  & \qw  & \qw  & \qw  & \rstick{\ket{c}}   }
                   %   1   &    2     &    3         &   4      &    5     &     6         &    7      &      8         &      9                                    &   10                                       & 11  &12&13&14 %15&16&   17   &  18    &    19  & 20                                     &  21         &  22                                       &  23                                       &  24 &  &  &   &  &  &  &

      As simplificações utilizadas foram:

      .. math::

         \begin{split}
          T\ket{1} 
              &= e^{i\frac{\pi}{4}}\ket{1}  \\
          S X T^\dagger X T^\dagger 
              &= \mqty[ 1 & 0 \\ 0 & i ] \mqty[ 0 & 1 \\ 1 & 0 ] \mqty[ 1 & 0 \\ 0 & e^{-i\frac{\pi}{4}} ] \mqty[ 0 & 1 \\ 1 & 0 ] \mqty[ 1 & 0 \\ 0 & e^{-i\frac{\pi}{4}} ] \\
              &= \mqty[ 1 & 0 \\ 0 & i ] \mqty[ 0 & e^{-i\frac{\pi}{4}} \\ 1 & 0 ] \mqty[ 0 & e^{-i\frac{\pi}{4}} \\ 1 & 0 ] \\
              &= \mqty[ 1 & 0 \\ 0 & i ] \mqty[ e^{-i\frac{\pi}{4}} & 0 \\ 0 & e^{-i\frac{\pi}{4}} ] 
               = e^{-i\frac{\pi}{4}} S \\
          H T X T^\dagger T X T^\dagger H 
              &= H T X  X T^\dagger H = H T T^\dagger H = H H = I \ .
             \end{split}

      O estado ao final do circuito é

      .. math:: \big(e^{i\frac{\pi}{4}}\ket{1}\big) \otimes \big( e^{-i\frac{\pi}{4}}\ket{0} \big) \otimes \ket{c} = \ket{10c} \ .

      Resta verificar o caso em que os dois primeiros qubits são
      :math:`\ket{11}`. Nesse caso, o circuito se reduz a:

      .. math::

         \Qcircuit @C=3pt @R=2pt                                                                                                                                                                                                                                                                                            
         {\lstick{\ket{1}} & \qw      & \qw      & \qw          & \qw      & \qw      & \qw           & \qw           & \qw            & \qw                                       & \gate{T} \gategroup{1}{11}{1}{11}{4pt}{--} & \qw &  &  &   &  &  &  &     \qw  & \gate{T}  & \qw  &  &  &   &  &  &  & \qw  & \qw  & \qw  & \rstick{e^{i\frac{\pi}{4}}\ket{1}}  \\
                           &          &          &              &          &          &               &               &                &                                           &                                            &     &  &  &   &  &  &  &          &                             &      &  &  &   &  &  &  &      &      &      &  \\
          \lstick{\ket{1}} & \qw      & \qw      & \qw          & \qw      & \qw      & \gate{T^\dagger} & \gate{X}      & \gate{T^\dagger}  & \gate{X} \gategroup{3}{7}{3}{11}{4pt}{--} & \gate{S}                                   & \qw &  &  & = &  &  &  &     \qw  & \gate{e^{-i\frac{\pi}{4}}S} & \qw  &  &  & = &  &  &  & \qw  & \qw  & \qw  & \rstick{ie^{-i\frac{\pi}{4}}\ket{1}} \\
                           &          &          &              &          &          &               &               &                &                                           &                                            &     &  &  &   &  &  &  &          &                             &      &  &  &   &  &  &  &      &      &      &  \\
          \lstick{\ket{c}} & \gate{H} & \gate{X} & \gate{T^\dagger}& \gate{X} & \gate{T} & \gate{X}      & \gate{T^\dagger} & \gate{X}       & \gate{T}                                  & \gate{H} \gategroup{5}{2}{5}{11}{4pt}{--}  & \qw &  &  &   &  &  &  &     \qw  & \gate{-iX}                    & \qw  &  &  &   &  &  &  & \qw  & \qw  & \qw  & \rstick{-i\ket{\overline{c}}}   }
                   %   1   &    2     &    3     &   4          &    5     &     6    &    7          &      8        &      9         &   10                                      & 11                                         & 12                                         &13&14 %15&16&   17   &  18    &    19  & 20                                     &  21         &  22                                       &  23                                       &  24 &  &  &   &  &  &  &

      Novamente, as simplificações realizadas são:

      .. math::

         \begin{split}
          T\ket{1} 
              &= e^{i\frac{\pi}{4}}\ket{1}  \\
          S X T^\dagger X T^\dagger \ket{1} 
              &=  e^{-i\frac{\pi}{4}} S \ket{1} = i e^{-i\frac{\pi}{4}} \ket{1} \\
          T X T^\dagger X
              &= \mqty[ 1 & 0 \\ 0 & e^{i\frac{\pi}{4}} ] \mqty[ 0 & 1 \\ 1 & 0 ] \mqty[ 1 & 0 \\ 0 & e^{-i\frac{\pi}{4}} ] \mqty[ 0 & 1 \\ 1 & 0 ]  \\
              &= \mqty[ e^{-i\frac{\pi}{4}} &  0 \\ 0  & e^{i\frac{\pi}{4}} ]  \\
          H \big(T X T^\dagger  X \big) \big( T X T^\dagger  X\big) H 
              &= \frac{1}{2} \mqty[ 1 & 1 \\ 1 & -1 ] \mqty[ e^{-i\frac{\pi}{4}} &  0 \\ 0  & e^{i\frac{\pi}{4}} ] 
                 \mqty[ e^{-i\frac{\pi}{4}} &  0 \\ 0  & e^{i\frac{\pi}{4}} ] \mqty[ 1 & 1 \\ 1 & -1 ] \\
              &=  \frac{1}{2} \mqty[ 1 & 1 \\ 1 & -1 ] \mqty[ -i &  0 \\ 0  & i ] 
                   \mqty[ 1 & 1 \\ 1 & -1 ] \\
              &= H (-iZ) H = -i H Z H = -i X \ ,
              \end{split}

      e o estado final é

      .. math::

         \big( e^{i\frac{\pi}{4}}\ket{1} \big) \otimes \big( ie^{-i\frac{\pi}{4}}\ket{1} \big) \otimes \big( -i\ket{\overline{c}} \big) 
              = \ket{11\overline{c}} \ .

      Termina, portanto, a verificação de que o circuito do enunciado
      realiza uma porta Toffoli. ◻

.. container:: section

   Universalidade das Portas Lógicas
   Quânticas

   É útil saber quais conjuntos de portas lógicas quânticas permitem
   reproduzir qualquer operação unitária :math:`U` em :math:`n` qubits.
   Essa questão é conhecida como *universalidade* de um conjunto de
   portas lógicas quânticas. A universalidade pode ter um sentido
   estrito – isto é, uma operação :math:`U` qualquer pode ser
   implementada exatamente com um número finito portas lógicas – ou um
   sentido amplo – isto é, a operação :math:`U` pode ser aproximada,
   permitindo-se um erro :math:`\varepsilon` arbitrado, por uma
   sequência finita de portas lógicas (cujo número é função do erro
   :math:`\varepsilon` máximo estipulado).

   .. container:: subsection

      Universalidade de Portas Lógicas na
      Computação Clássica Reversível

      A universalidade na Computação Clássica Não-Reversível está
      essencialmente decidida nos teoremas
      `[cap1:teorema_universalidade_or_and_not] <#cap1:teorema_universalidade_or_and_not>`__
      e
      `[cap1:teorema_universalidade_nand] <#cap1:teorema_universalidade_nand>`__
      da seção
      `[cap1:universalidade_portas_logicas_classicas] <#cap1:universalidade_portas_logicas_classicas>`__.
      Esses teoremas dizem que qualquer função booleana
      :math:`f \colon \{0,1\}^m \to \{0,1\}^n` pode ser realizada por
      portas NOT e AND, por portas NOT e OR ou apenas por portas NAND (e
      em todos os casos, acrescenta-se implicitamente as portas SWAP e
      FANOUT/COPY).

      Na Computação Clássica Reversível, as funções booleanas têm o
      mesmo número de bits na entrada e saída, e as portas lógicas são
      reversíveis (isto é, conhecendo-se a saída, é possível determinar
      a entrada que a originou). As portas lógicas NOT, CNOT, SWAP,
      Toffoli e Fredkin, quando trabalhando apenas com bits e não
      qubits, são exemplos de portas lógicas clássicas reversíveis.

      .. container:: theorem

         A porta Toffoli é universal para a Computação Clássica.

      .. container:: proof

         *Proof.* É possível realizar as portas NOT e AND utilizando a
         porta Toffoli e acrescentando alguns bits de trabalho (bits com
         valor fixado em 0 ou 1 dependendo da necessidade).

         .. math::

            \begin{array}{ccccccc}
               \text{AND} \phantom{x\cdot y} \quad \quad \quad
               & & 
               \text{NOT} \phantom{x} \quad \quad \quad
               & & 
               \text{COPY/FANOUT}
               & & 
               \text{SWAP}
               \\ & & & & & & \\
                       \Qcircuit @C=4pt @R=6pt @!R {
              \lstick{x} & \ctrl{1} & \qw & \\
              \lstick{y} & \ctrl{1} & \qw & \\
              \lstick{0} & \targ    & \qw &\rstick{x \cdot y} }
              \phantom{x\cdot y} \quad \quad \quad
              & &
                  \Qcircuit @C=4pt @R=6pt @!R {
              \lstick{x} & \ctrl{1} & \qw & \rstick{x}\\
              \lstick{1} & \ctrl{1} & \qw & \\
              \lstick{1} & \targ    & \qw &\rstick{\overline{x}} }
               \phantom{x} \quad \quad 
              & &
               \Qcircuit @C=4pt @R=6pt @!R {
              \lstick{x} & \ctrl{1} & \qw & \rstick{x}\\
              \lstick{1} & \ctrl{1} & \qw & \\
              \lstick{0} & \targ    & \qw &\rstick{x} }
              & & 
              \Qcircuit @C=4pt @R=6pt @!R {
              \lstick{1} & \ctrl{1} & \ctrl{1} & \ctrl{1} & \qw & &   & & & \ctrl{1}    & \qw & \\
              \lstick{x} & \ctrl{1} & \targ    & \ctrl{1} & \qw & & = & & & \qswap      & \qw & \rstick{y}\\
              \lstick{y} & \targ    & \ctrl{-1}& \targ    & \qw & &   & & & \qswap \qwx & \qw & \rstick{x} }
               \end{array}

         Com isso, um circuito booleano qualquer pode ser realizado
         apenas com portas Toffoli ignorando-se bits extra (“lixo”). ◻

      .. container:: theorem

         A porta Fredkin é universal para a Computação Clássica.

      .. container:: proof

         *Proof.* Da mesma forma como para a porta Toffoli, é possível
         realizar as portas NOT e AND utilizando a porta Fredkin e
         acrescentando alquns bits de trabalho.

         .. math::

            \begin{array}{ccccccc}
               \text{AND} \phantom{x\cdot y} \quad \quad \quad
               & & 
               \text{NOT} \phantom{x} \quad \quad \quad
               & & 
               \text{COPY/FANOUT}
               & & 
               \text{SWAP}
               \\ & & & & & & \\
                       \Qcircuit @C=4pt @R=10pt @!R {
              \lstick{x} & \ctrl{1} & \qw & \\
              \lstick{y} & \qswap   & \qw & \\
              \lstick{0} &\qswap\qwx& \qw &\rstick{x \cdot y} }
              \phantom{x\cdot y} \quad \quad \quad
              & &
                  \Qcircuit @C=4pt @R=10pt @!R {
              \lstick{x} & \ctrl{1} & \qw & \\
              \lstick{1} & \qswap   & \qw & \rstick{\overline{x}} \\
              \lstick{0} &\qswap\qwx& \qw &  }
               \phantom{x} \quad \quad 
              & &
             \Qcircuit @C=4pt @R=10pt @!R {
              \lstick{x} & \ctrl{1} & \qw & \rstick{x}\\
              \lstick{1} & \qswap   & \qw &  \\
              \lstick{0} &\qswap\qwx& \qw & \rstick{x} }
              & & 
            \Qcircuit @C=4pt @R=10pt @!R {
              \lstick{1} & \ctrl{1} & \qw & \\
              \lstick{x} & \qswap   & \qw & \rstick{y} \\
              \lstick{y} &\qswap\qwx& \qw & \rstick{x} }
               \end{array}

         Com isso, um circuito booleano qualquer pode ser realizado
         apenas com portas Fredkin ignorando-se bits extra (“lixo”). ◻

      .. container:: remark

         O preço a se pagar, na Computação Clássica, para que a
         computação seja reversível é o uso de bits de trabalho
         (*ancilla bits*) e produção de bits “lixo” (*garbage bits*). Em
         :raw-latex:`\cite{book:lecture_notes_preskill}` (capítulo [1]_
         6, p.12-15) verifica-se a universalidade do conjunto de portas
         reversíveis Toffoli e NOT para a Computação Clássica Reversível
         usando-se apenas 1 bit de trabalho.

      .. container:: remark

         Na referência :raw-latex:`\cite{book:lecture_notes_preskill}`
         (capítulo\ `1 <#cap4:footnote1>`__ 6, p.11-12) encontra-se uma
         demonstração de que as portas clássicas reversíveis de 1 e 2
         bits não podem formar um conjunto universal.

   .. container:: subsection

      Universalidade de Portas Lógicas na
      Computação Quântica

      Como as portas Toffoli e Fredkin da Computação Clássica têm
      análogos quânticos, e tendo em vista a universalidade dessas
      portas na Computação Clássica, tem-se que a Computação Quântica
      engloba a Computação Clássica quando lança-se mão de qubits de
      trabalho e ignoram-se os qubits “lixo”
      (:raw-latex:`\cite{book:pqci_benenti}`, p.122).

      É possível verificar que, para a Computação Quântica, todas as
      portas lógicas de 1 qubit e a porta CNOT formam um conjunto
      universal no sentido estrito (ou seja, capaz de produzir
      exatamente, em princípio, qualquer transformação unitária)
      (:raw-latex:`\cite{book:pqci_benenti}`, p.119-124), o que já não
      ocorre com a Computação Clássica Reversível (conforme final da
      seção
      `[cap4:sec_univ_portas_log_na_comp_rev] <#cap4:sec_univ_portas_log_na_comp_rev>`__,
      observação
      `[cap4:obs_portas_class_1_2_qubit_nao_universal] <#cap4:obs_portas_class_1_2_qubit_nao_universal>`__).

      O conjunto finito de operações :math:`X`, :math:`Y`, :math:`Z`,
      :math:`H`, :math:`T`, :math:`S`, e CNOT também é universal, agora
      no sentido amplo (isto é, capaz apenas de aproximar uma operação
      unitária geral dentro de uma faixa de erro pré-determinada). Esse
      conjunto não é mínimo, pois algumas dessas portas de 1 qubit podem
      ser obtidas das demais. Por exemplo: :math:`S = T^2`,
      :math:`Z=HXH = S^2`, :math:`Y = -iZX`.

      As portas lógicas quânticas Toffoli e Hadamard formam um conjunto
      universal capaz de aproximar qualquer matriz unitária com
      coeficientes reais. Com o uso de um qubit extra, de trabalho, é
      possível realizar operações unitárias gerais em função de matrizes
      unitárias com coeficientes reais (conforme artigo
      :raw-latex:`\cite{article:toff_had_quantum_universal_dorit}`,
      p.2-3).

      O assunto de universalidade de portas lógicas quânticas está em
      desenvolvimento. Não se conhece um algoritmo ou método eficiente
      capaz de decompor uma matriz unitária :math:`U` em fatores de um
      dado conjunto universal. A maioria dos conjuntos finitos que foram
      provados universais têm suas demonstrações baseadas em argumentos
      de existência: mostra-se que existe uma sequência de portas que
      aproxima dentro de um erro dado a porta :math:`U`, mas não se sabe
      como encontrar essa aproximação. Para a universalidade de todas as
      portas de 1 qubit mais a CNOT, a prova é construtiva
      (:raw-latex:`\cite{book:pqci_benenti}`, p.119-124), mas recorre à
      disponibilidade de qualquer porta de 1 qubit, além de não ser
      eficiente em geral (isto é, requerer um número de portas lógicas
      exponencial no número de qubits :math:`n`).

.. container:: section

   Teorema da Não-Clonagem

   Um bit clássico pode ser copiado para servir como entrada em diversas
   partes de um circuito digital clássico.

   .. figure:: cap4/figuras/full_adder_gates.png
      :alt: 

   Pode-se pensar nesse comportamento em termos da porta lógica clássica
   COPY que devolve à saída duas (ou mais) cópias do bit de entrada.

   .. container:: float

      .. math::

         \Qcircuit @C=10pt @R=10pt @!R {
                     &     & \multigatenoqw{2}{\text{COPY}} & \qw &  \rstick{b} \qw \\
          \lstick{b} & \qw &  \ghost{\text{COPY}}            & \qw &  \rstick{b} \qw \\
                     &     &  \ghostnoqw{\text{COPY}}        & \qw &  \rstick{b} \qw  }

   Como na Computação Quântica, o análogo das portas lógicas seriam as
   operações unitárias sobre qubits, poderia-se cogitar a existência de
   uma porta lógica quântica de 2 qubits que tivesse como entrada um
   qubit num estado :math:`\ket{\psi}` qualquer, a ser copiado, (e outra
   entrada :math:`\ket{0}` para completar 2 entradas) e devolvesse 2
   qubits no estado :math:`\ket{\psi}`, como ilustrado na figura a
   seguir.

   .. container:: float

      .. math::

         \Qcircuit @C=10pt @R=10pt @!R {
          \lstick{\ket{\psi}} & \qw &  \multigate{1}{\text{COPY}} & \qw & \rstick{\ket{\psi}} \qw \\
          \lstick{\ket{0}}    & \qw &  \ghost{\text{COPY}}        & \qw & \rstick{\ket{\psi}} \qw }

   No entanto, o chamado *Teorema da Não Clonagem* informa que não
   existe uma operação unitária capaz de efetuar essa operação para
   qualquer estado :math:`\ket{\psi}` de entrada. Desse modo, a cópia de
   bits não possui análogo na Computação Quântica.

   .. container:: theorem

      Não existe uma operação unitária que permita copiar o estado de 1
      qubit em 2 (ou mais) qubits. Isto é, não existe operação unitária
      :math:`U` que satisfaça, para todo estado :math:`\ket{\psi}` de 1
      qubit, o seguinte:

      .. math::

         \Qcircuit @C=10pt @R=10pt @!R {
          \lstick{\ket{\psi}} & \multigate{1}{U} &  \rstick{\ket{\psi}} \qw\\
          \lstick{\ket{0}}    & \ghost{U}        &  \rstick{\ket{\psi}} \qw}

   .. container:: proof

      *Proof.* A prova se dá por redução ao absurdo. Seja :math:`U` uma
      tal operação unitária, satisfazendo
      :math:`U\ket{\psi}\ket{0} = \ket{\psi}\ket{\psi}` para todo estado
      :math:`\ket{\psi}` com :math:`||\ket{\psi||} = 1`.

      Considere os estados de 1 qubit :math:`\ket{1}` e :math:`\ket{+}`.
      Tem-se que

      .. math::

         \tag{$\ast$}
           \begin{split}
            \braket{10}{+0}_{1,2} 
            &= \braket{1}{+}_1 \braket{0}{0}_2 \\
            &= \braket{1}{+} \cdot 1 \\
            &= \bra{1} \left(\frac{\ket{0} + \ket{1}}{\sqrt{2}} \right) \\
            &= \frac{1}{\sqrt{2}} \ .
           \end{split}

      Por outro lado, como :math:`U` é unitária, também vale que

      .. math::

         \tag{$\ast \ast$}
            \begin{split}
             \braket{10}{+0}_{1,2} 
             &= \bra{10}_{1,2} U^\dagger U \ket{+0}_{1,2} \\
             &= \bra{11}_{1,2} \ket{++}_{1,2} \\
             &= \braket{1}{+} \braket{1}{+} \\ 
             &=  \frac{1}{\sqrt{2}} \cdot  \frac{1}{\sqrt{2}} \ .
            \end{split}

      Comparando (:math:`\ast`) e (:math:`\ast \ast`), tem-se que
      :math:`\frac{1}{\sqrt{2}} =  \frac{1}{\sqrt{2}} \cdot  \frac{1}{\sqrt{2}}`,
      o que não pode ocorrer. Portanto a hipótese de que existe
      :math:`U` como descrito acima é falsa. ◻

   É possível generalizar essa demonstração para mostrar que não existe
   operação unitária :math:`U` tal que
   :math:`U\ket{\psi}\ket{s} = \ket{\psi}\ket{\psi}` para todo estado
   :math:`\ket{\psi}`, e com :math:`\ket{s}` um estado fixo qualquer
   (:raw-latex:`\cite{book:qcqi_nc}`, p.532).

   Apesar de não ser possível clonar um estado arbitrário, é possível
   copiar estados na base computacional. De fato, uma porta CNOT é
   suficiente para realizar isso.

   .. container:: proposition

      O circuito abaixo realiza a clonagem de estados da base
      computacional. Se :math:`a = 0,1`, tem-se:

      .. math::

         \Qcircuit @C=10pt @R=10pt @!R {
          \lstick{\ket{a}} & \ctrl{1} &  \rstick{\ket{a}} \qw \\
          \lstick{\ket{0}} & \targ    &  \rstick{\ket{a}} \qw }

   .. container:: proof

      *Proof.* Segue do comportamento da porta CNOT nos vetores da base
      computacional:

      .. math:: \ket{a}\ket{0} \ \  \xrightarrow{\text{CNOT}} \ \  \ket{a}\ket{0\oplus a} = \ket{a} \ket{a} \ . \qedhere

       ◻

.. [1]
   | Disponível no link:
   | ``http://www.theory.caltech.edu/people/preskill/ph229/notes/chap6.pdf``
