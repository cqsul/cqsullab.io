Sobre o Projeto
===============

Motivação e Problemática
------------------------

A computação quântica (CQ) apresenta um novo modelo de computação, o
qual incorpora novas maneiras de pensar, projetar e estruturar algoritmos.
Conceitos chave deste modelo são o `princípio da superposição, emaranhamento
quântico, não-clonagem e reversibilidade`. Atualmente, a CQ tem sido investigada e
tem se mostrado um paradigma promissor considerando diferentes áreas na ciência
e tecnologia, desde algoritmos quânticos, inteligência artificial quântica, linguagens
de programação quântica e hardware quântico.

No Brasil, a CQ tem sido trabalhada por pesquisadores principalmente da
área da física e alguns da ciência da computação e engenharias, mas há ainda uma
grande barreira a ser transposta para que as tecnologias quânticas possam ser
aplicadas em larga escala na solução de problemas reais. As dificuldades
enfrentadas indicam a necessidade de mais avanços no desenvolvimento de
hardwares quânticos para o controle da decoerência e implementação de códigos
de correção de erros, uniformidade na produção de qubits físicos, refrigeração dos
processadores quânticos, escalabilidade do número de qubits, desenvolvimento de
memórias quânticas, entre outros. Na área de software, faz-se necessário o
desenvolvimento de algoritmos eficientes para preparação e leitura de estados
quânticos, algoritmos quânticos eficientes para resolver os mais variados problemas
da indústria, linguagens quânticas de programação de alto nível e integração entre
HPC e computadores quânticos. Todavia, para vencer todas as dificuldades
apresentadas acima, é necessário a formação de recursos humanos altamente
qualificados.

Nesse sentido, nosso foco é o de unir esforços de pesquisa e consolidar um
grupo forte para atuação na pesquisa básica e aplicada em computação quântica no
sul do Brasil, visando o desenvolvimento de algoritmos quânticos aplicados a
problemas reais das indústrias do país, avançando também na questão de formação
de profissionais tanto para fomentar as pesquisas quanto para atuar diretamente na
implementação de soluções no setor industrial.

Através deste projeto pretendemos contribuir para responder algumas
questões em aberto no campo da computação quântica. Separamos algumas
destas perguntas em:

* **Pesquisa básica**
    #. É possível preparar estados quânticos de maneira eficiente usando ML?
    #. Como mitigar erros em processadores quânticos utilizando técnicas de controle quântico?
    #. Quais os limites para a implementação do algoritmo de fatoração de Shor via computação quântica adiabática?
    #. Quais os recursos (correlações, energia, referencial, etc) necessários para a implementação de computação quântica?
    #. Que classe de algoritmos podem ser implementados em sistemas ópticos de maneira eficiente utilizando os graus de liberdade espacial da luz?
* **Pesquisa aplicada**:
    #. Como desenvolver uma linguagem quântica de programação de alto nível?
    #. Qual a melhor forma de integrar HPC com computação quântica?
    #. Como obter vantagem no processamento utilizando algoritmos quânticos para resolver problemas da indústria na era NISQ?

Além das questões anteriores, abordaremos outras questões práticas, tais como:

#. Como fazer a indústria brasileira perceber a importância de se investir em tecnologias quânticas nesse momento?
#. Qual a formação ideal para os profissionais dos diferentes setores da atividade econômica?

Objetivo
--------

Criar um núcleo colaborativo no sul do Brasil, envolvendo grupos de pesquisa
da UFSC, UFSM, UFPR, UDESC e ISI-SE, para impulsionar a pesquisa básica, o
ensino e o desenvolvimento de aplicações de computação quântica. Através do
estímulo à interação entre os grupos de pesquisa existentes e do fornecimento das
ferramentas necessárias, pretendemos juntar as habilidades individuais de cada
grupo para acelerar esse processo. Em particular, visamos adaptar algoritmos
quânticos conhecidos e desenvolver novos algoritmos para decifrar esquemas
criptográficos, melhorar técnicas de machine learning, resolver problemas práticos
de interesse da indústria brasileira, testar protocolos e algoritmos nos simuladores
quânticos ópticos e nos computadores quânticos acessíveis na nuvem, conhecer os
limites últimos dos recursos quânticos para o processamento de informação,
produzir material didático e ministrar cursos de formação, alavancando assim as
pesquisas em computação quântica no Brasil.

Relevância
----------

A área de computação quântica vem crescendo em todo o mundo, sendo
vislumbrada como uma área estratégica para a segurança e desenvolvimento
tecnológico de qualquer nação do planeta nos anos vindouros. Recentemente vimos
o surgimento de congressos e eventos científicos voltados à apresentação e
discussão dos avanços de pesquisas na área. Tais eventos, antes restritos à
academia, hoje atraem bancos e demais empresas que atuam no mercado
financeiro, indústrias dos setores de logística, aeroespacial, automotiva, óleo e gás,
transporte, energia, comunicação, saúde, química, entre outras. Além de ser uma
tecnologia disruptiva, já que em alguns casos pode fornecer ganhos exponenciais
no tempo de processamento ou armazenar um certo volume de dados em uma
memória exponencialmente menor do que os computadores atuais (clássicos), a
computação quântica é vista como prioridade para a segurança nacional, devido às
implicações nas áreas de criptografia e segurança de dados. Após o advento do
algoritmo de fatoração de Shor, vários métodos clássicos que utilizam algoritmos de
chave pública tornaram-se inseguros. Um exemplo de tamanha ameaça, é a adição
de esquemas de criptografia pós-quântica em sistemas clássicos de comunicação,
como preconizado pelo NIST. Neste sentido, esta proposta é extremamente
relevante tanto no contexto do conhecimento científico como tecnológico, pois
aborda um tema sensível e promissor que provocará profundos impactos na
sociedade brasileira. Em específico, este projeto contribuirá em temas que afetarão
diretamente as indústrias brasileiras e a segurança nacional, permitindo que no
futuro tenhamos independência científica e tecnológica na área de computação
quântica.

Potencial de Inovação
---------------------

Por se tratar de uma tecnologia de ponta, avanços na área de computação
quântica, sejam em pesquisa básica ou aplicada, possuem caráter inovador. Em sua
grande maioria, os resultados deste projeto contribuirão para o desenvolvimento
técnico ou do arcabouço teórico da área. Para além disto, a aplicação desta
tecnologia visando a solução de problemas práticos também apresenta-se como
uma potencial fonte de inovação. Listam-se abaixo algumas das possíveis
inovações resultantes deste projeto de pesquisa:

* Novas linguagens quânticas de programação: tais desenvolvimentos
  permitem tratar problemas com maior nível de abstração (assim como
  ocorreu na área da computação clássica). O surgimento de novas
  linguagens impulsiona o desenvolvimento de outras ferramentas destinadas
  ao desenvolvimento de algoritmos quânticos, como compiladores, técnicas
  de otimização de código, etc.
* Bibliotecas para linguagens quânticas de programação: ao permitir maior
  abstração para descrição e modelagem de solução de problemas, surge
  também a necessidade de desenvolver bibliotecas de suporte às
  funcionalidades da linguagem quântica de programação, neste sentido é
  natural que tais funcionalidades sejam providas na forma de bibliotecas.
* Licenças de algoritmos quânticos: o desenvolvimento de algoritmos
  quânticos visando a solução de problemas da indústria apresenta grande
  potencial de inovação, seja pelo impacto direto do uso destas tecnologias
  seja pelas potencialidades vislumbradas em decorrência destes
  desenvolvimentos.
* Patentes de técnicas e metodologias para tratamento de decoerência e
  mitigação de erros: os atuais computadores quânticos estão sujeitos a
  elevadas taxas de erro. A aplicação de técnicas de controle aos processos
  de computação visando a identificação e a correção de tais erros permitirá
  que os atuais computadores possam ser usados em aplicações reais.
* Construção de material didático: existem muito poucos materiais didáticos
  em língua portuguesa para a formação de recursos humanos na área de
  computação quântica e que sejam adequados aos diferentes perfis de
  profissionais. Portanto, a interação com o ISI-SE (pessoal da indústria) e
  com as ICTs parceiras (pessoal da universidade), permitirá a confecção de
  materiais personalizados para esses diferentes grupos interessados em
  computação quântica.

Caráter Multi ou Interdisciplinar
---------------------------------

A Ciência da Informação Quântica é uma das áreas mais multidisciplinares
da ciência atual, pois envolve Física, Matemática, Ciência da Computação,
Engenharia, Teoria da Comunicação, entre outras. No seu início, em torno de 1980,
a Computação Quântica surgiu com a chamada Simulação Quântica. A motivação
para isso veio da Física, pois se queria descrever/simular sistemas quânticos para
se entender melhor o funcionamento de materiais tais como supercondutores de
altas temperaturas. Naturalmente, a modelagem física usa como ferramenta
essencial e onipresente a matemática. Em relação à Ciência da Computação,
também na década de 1980, David Deutsch estendeu o conceito fundamental de
máquina de Turing para o regime quântico, mostrando ainda que o paralelismo
quântico, que é baseado no princípio de superposição quântica, poderia ser utilizado
para resolver problemas matemáticos de forma mais rápida do que com máquinas
de Turing clássicas. A partir dos algoritmos de Shor e de Grover, percebeu-se que
computadores podem de fato serem usados para resolver muito mais rapidamente
problemas matemáticos de interesse geral da sociedade. Isso motivou muitos
laboratórios e empresas mundo afora a almejar computadores quânticos (em larga
escala e tolerante a falhas) utilizando diferentes plataformas físicas, desbravando o
campo da engenharia quântica. Pela natureza não intuitiva da Mecânica Quântica,
outro grande desafio a ser enfrentado é o desenvolvimento de algoritmos quânticos
para os mais diversos problemas matemáticos de interesse científico e tecnológico.
Neste sentido, é importante também descrever a complexidade computacional
desses algoritmos, uma tarefa típica de Ciência da Computação. Outra área
estreitamente ligada à Computação Quântica é a comunicação quântica, em
particular a Criptografia Quântica. A utilização e desenvolvimento da infraestrutura
óptica de comunicação atual, tanto com fibras ópticas quanto com satélites, é
essencial para distribuição de estados emaranhados, que por sua vez são
essenciais para implementação de protocolos de comunicação quântica e para
implementação de computação quântica distribuída e/ou na nuvem.

Considerando ainda as possíveis aplicações dos algoritmos quânticos, a
interação deste projeto com outras áreas do conhecimento aumenta
significativamente, vejamos: Química quântica (simulação de moléculas); Transporte
e logística (otimização de rotas, organização de estoques e agendas, controle
portuário); Física de materiais (descoberta de novos materiais, materiais
supercondutores a altas temperaturas); Machine learning (veículos autônomos,
visão computacional, determinação de padrões); Medicina (descoberta de novos
fármacos, medicina personalizada); Finanças (detecção de fraudes, otimização de
portfólio); Matemática (solução de equações diferenciais, solução de problemas de
álgebra linear); Segurança cibernética (quebra de códigos criptográficos).

Metodologia
-----------

A natureza deste trabalho é em grande parte pesquisa básica. Serão
utilizadas, neste trabalho, avaliações de desempenho e análise de complexidade
dos algoritmos desenvolvidos. Experimentação através do desenvolvimento e
avaliação de protótipos são também objetivos neste projeto. As atividades a serem
desenvolvidas neste projeto levam em conta que o desenvolvimento de algoritmos e
experimentos em questão estão diretamente ligados às atividades de pesquisa
realizadas pelos grupos envolvidos, através de seus pesquisadores, professores e
estudantes.

A gestão e o acompanhamento do projeto se darão através da realização de
três workshops presenciais (início, meio e fim do projeto) e de reuniões periódicas (a
distância) com os membros da equipe. No primeiro workshop, com a presença de
todos os membros da equipe, apresentaremos as nossas ideias para o
desenvolvimento dos trabalhos propostos dentro do projeto. Esta será uma
oportunidade para sondar os interesses em comum e definir pequenos grupos de
trabalho para resolver problemas específicos. A presença de um especialista em
computação quântica de fora da equipe ajudará na discussão e posterior
determinação dos rumos referentes a cada trabalho. Apurada as necessidades das
equipes e das indústrias através do ISI- SE, buscaremos desenvolver soluções para
esses problemas. Um curso de computação quântica introdutório será ministrado
aos pesquisadores do ISI-SE, de modo que possam compreender o funcionamento
da computação quântica e sejam capazes de identificar problemas que necessitem
desse novo tipo de tecnologia. Os workshops posteriores, que seguirão a estrutura
do primeiro workshop, servirão para avaliar o andamento do projeto nas etapas
intermediária e final. Além dessas reuniões, estão previstos seminários com a
participação de estudantes e demais pesquisadores participantes das diversas
instituições envolvidas.

A difusão dos conhecimentos ligados a esse projeto se dará da seguinte
forma:

* Publicação de artigos em congressos e revistas nacionais e internacionais de reconhecida importância na área de Computação Quântica e Informação Quântica;
* Participação em feiras e outros eventos similares;
* Divulgação dos conhecimentos adquiridos e/ou gerados através de seminários nos cursos de graduação e de pós-graduação das instituições envolvidas no projeto;
* Divulgação dos resultados da pesquisa em mídias sociais;
* Produção de relatório final contendo os resultados alcançados no projeto.

Produtos e Resultados Esperados
-------------------------------

São produtos esperados neste projeto:
* Formação de uma rede de colaboração em computação quântica na região sul do Brasil.
* Provas de conceito de algoritmos quânticos adaptados para aplicações da indústria, tanto em computadores quânticos reais como em simuladores clássicos de computadores quânticos.
* Uma maneira eficiente de se extrair o resultado clássico de algoritmos quânticos.
* Uma técnica eficiente para a preparação de estados usando machine learning.
* Artigos científicos publicados em revistas científicas internacionais com qualidade reconhecida pela CAPES.
* Material didático, escrito em português, para divulgação da computação quântica desde o nível básico até o nível avançado.
* Formação de pesquisadores para atuarem na área de computação quântica e demais tecnologias quânticas, podendo ser graduados, mestres ou doutores.
* Bibliotecas para linguagens de programação quântica.
* Relatório dos avanços obtidos no projeto. 

Impactos Esperados
------------------

Este projeto propiciará a interação entre professores(as), pesquisadores(as)
e alunos(as) das diferentes ICTs, criando novos laços de colaboração científica
entre os membros da equipe. Como isso, espera-se o início ou a consolidação da
linha de pesquisa computação quântica nas ICTs parceiras. Espera-se também que
esse núcleo formado pelas ICTs do projeto se torne referência para outras
instituições no país e para os demais setores da sociedade sobre o tema
computação quântica.

Com a realização deste projeto, almejamos demonstrar via simulação a
potencialidade da computação quântica para resolver problemas práticos de
interesse da indústria brasileira. Também demonstraremos a conveniência e
potencialidade da utilização de computadores como ferramenta universal para
verificação de aspectos fundamentais da mecânica quântica via simulação quântica.
Além disso, esperamos acelerar a consciência popular geral sobre essa área de
pesquisa e educar pessoas para que possam ser membros atuantes na utilização e
desenvolvimento de tecnologias quânticas.

Plano de Divulgação Científica
------------------------------

Física Quântica e a Óptica são duas áreas fascinantes, que cativam
crianças, jovens e adultos. Queremos estimular a busca pelo conhecimento
científico e tecnológico pelos cidadãos e cidadãs brasileiras, tornando-os acessíveis
de forma gratuita a qualquer pessoa. A implementação das nossas ações se darão
de forma presencial e a distância. Este trabalho alcançará o público não
especializado através de uma homepage construída para o projeto, através da
divulgação de vídeos no `Canal do Grupo de Computação Quântica (GCQ) da UFSC
no YouTube <https://www.youtube.com/c/GCQUFSC>`_ e também através dos perfis
criados nas redes sociais para divulgação do projeto, incluindo mídias como
Instagram, LinkedIn e Facebook. Nestes canais serão publicizados os resultados e
avanços obtidos durante a realização do projeto, bem como a realização de
palestras e defesas de TCC, mestrado e doutorado sobre o tema do projeto.
Aproveitaremos dois momentos para impulsionar a divulgação da computação
quântica no Brasil, o dia `Mundial Quântico <https://worldquantumday.org>`_, que se
comemora em 14 de abril, e a realização do `Workshop de Computação Quântica
da UFSC <https://workshop-cq.ufsc.br>`_, que ocorre anualmente com a finalidades de
divulgar a computação quântica no Brasil, trazendo os avanços recentes e
promovendo a interação entre indústria e universidades. Também serão realizadas
diversas palestras de divulgação científica nas universidades participantes do
projeto e apresentações nas semanas acadêmicas e de amostra científica abertas
ao público externo às universidades. Os materiais produzidos e as gravações dos
cursos ministrados também poderão ser divulgados de forma aberta, seguindo uma
licença Creative Commons. 

Infraestrutura Institucional para Execução do Projeto
-----------------------------------------------------

A instituição executora, através do grupo de Computação Quântica da UFSC
(dgp.cnpq.br/dgp/espelhogrupo/3337802746855041), vinculado ao Laboratório de
Pesquisa em Inteligência Artificial e Algoritmos (https://pnipe.mctic.gov.br/search?term=LIAA) localizado no Departamento de Informática e Estatística da
Universidade Federal de Santa Catarina, possui uma área total de :math:`26,6\text{m}^2` equipada
com computadores, impressoras e demais equipamentos. O grupo conta ainda com
um simulador de Computador Quântico (código pnipe - Cod1BV62P/22) para
implementação e testes de algoritmos quânticos circuitais

O Laboratório de Óptica Quântica do departamento de Física da UFSC
dispõe atualmente de duas salas escuras para experimentos de produção e
contagem de fótons em coincidências e de ótica não-linear com conversão
descendente de frequência. Cada sala contém uma mesa óptica, vários tipos de
lasers nos comprimentos de onda violeta, visível e infravermelho próximo; quatro
módulos de contagem de fótons únicos, três moduladores espaciais de luz com
resolução HD; uma câmera CCD para visível e infravermelho; dois sistemas de
FPGA para processamento de sinais eletrônicos; um osciloscópio de 200 MHz;
diversos componentes óticos e opto-mecânicos.

O cluster computacional do Laboratório de Computação Científica (LabCCUFSC/Joinville) também será usado pelo projeto. Esse cluster conta com 23 nós,
sendo 21 para processamento exclusivo CPUs e 2 nós com GPUs específicas para
uso em computação científica (4 x Tesla K40m por nó). O cluster conta no total com
448 CPU cores. Nós CPU: 2 x Intel Xeon CPU E5-2640 v4 2.40GHz com 10 núcleos
e 128 GB de memória RAM e 1 TB de memória em disco. Nós CPU/GPU: 2 x Intel
Xeon CPU E5-2660 v4 2.00GHz com 14 núcleos, 256 GB de memória RAM e 1 TB
de memória em disco e 4 GPUs Nvidia Tesla K40m. Em armazenamento o cluster
possui 10 HD de 2TB, com 15 TB de armazenamento RAID 6. Cadastro no PNIPE
https://pnipe.mctic.gov.br/equipment/15819.

A SeTIC - UFSC conta com clusters de computadores de alto desempenho e
oferece serviços de acesso e processamento através de máquinas virtuais
configuráveis de acordo com a demanda do projeto. 

Os integrantes do Grupo de Informação Quântica e Fenômenos Emergentes
da Universidade Federal de Santa Maria têm disponibilidade de computadores
pessoais (um pouco antigos e ultrapassados, mas funcionais) para fazer os
trabalhos do dia a dia. Os recursos para compra desses computadores foram
obtidos através de projeto do pesquisador Jonas Maziero em um Edital Universal do
CNPq no ano de 2013.

Além disso, a UFSM junto da pesquisadora Juliana K. Vizzotto do
Departamento de Linguagens e Sistemas de Computação (DLSC), tem disponível
para execução do projeto a infraestrutura do laboratório do Grupo CNPq de
Linguagens de Programação e Banco de Dados (GLPBD) localizado na sala 387,
anexo B no Centro de Tecnologia (CT). O laboratório do GLPB contém 6
computadores e um servidor MAC Pro (2013), impressora, datashow e TV 42''.

Colaborações e Parcerias Nacionais
----------------------------------

O Grupo de Computação Quântica da UFSC possui parceria de pesquisa
com o SENAI - CIMATEC (Bahia), com as startups de computação quântica
Quantuloop e Quanby, ambas localizadas em Florianópolis-SC e fundadas pelos
membros do grupo Evandro Chagas Ribeiro da Rosa e Eduardo Inacio Duzzioni,
respectivamente. O prof. Duzzioni possui colaboração com o grupo de pesquisa
liderado pelo prof. Celso Jorge Villas-Bôas da UFSCar. A profa. Jerusa e o prof.
Duzzioni têm atuado junto aos grupos de discussão sobre tecnologias quânticas
conduzidos pelas Forças Armadas do Brasil e pelo MCTI. 

O prof. Paulo Henrique Souto Ribeiro tem colaborações com os grupos de
pesquisa coordenados pelos profs. Antonio Zelaquett Khoury do Instituto de Física
da UFF e Willamys Cristiano Soares Silva da UFAL, Campus Arapiraca. 

Os professores Eduardo Inacio Duzzioni, Paulo Henrique Souto Ribeiro e
Renné Luiz Câmara Medeiros de Araujo participam do INCT de Informação
Quântica.

A parceria com o ISI-SE foi estabelecida especialmente para esta chamada,
tendo um grande interesse de colaboração por ambas as partes. 

O pesquisador Jonas Maziero desenvolve trabalhos sobre mecânica quântica
e computação quântica em colaboração com Marcos Leopoldo Ways Basso,
doutorando em Física na Universidade Federal do ABC, e também em colaboração
com Lucas Chibebe Céleri, professor na Universidade Federal de Goiás.

O pesquisador Renato Moreira Angelo desenvolve trabalhos sobre recursos
quânticos e aspectos fundamentais de mecânica quântica em colaboração com
Roberto M. Serra, professor na Universidade Federal do ABC.

Colaborações e Parcerias Internacionais
---------------------------------------

O prof. Paulo Henrique Souto Ribeiro possui colaboração com os grupos de
pesquisa coordenados pelos professores Xiasong Ma da Universidade de Nanjing,
na China, e Sergei Kulik da Universidade de Moscow, na Rússia, através do projeto
CNPq(402823/2019-2). Também possui colaboração com o prof. Philip Walther, da
Universidade de Viena, na Áustria, e com o prof. Laurent Vernac da Universidade
Paris 13, na França. Em todas as colaborações os temas de pesquisa são
informação quântica, óptica clássica e quântica e termodinâmica quântica.

A pesquisadora Juliana K. Vizzotto participa desde 2015 de um projeto de
cooperação internacional STIC-AmSud/CAPES, incluindo renomados pesquisadores
da área, chamado Foundations of Quantum Computation: Syntax and Semantics,
com as seguintes instituições: Inria, Universidad Nacional de Quilmes/Argentina,
CNRS, Université Aix-Marseille e CentraleSupélec -LRI/França.

O pesquisador Renato Moreira Angelo desenvolve trabalhos sobre aspectos
fundamentais de mecânica quântica em colaboração com os pesquisadores Ismael
L. Paiva (pós-doc) e Eliahu Cohen (professor), vinculados a Faculty of Engineering
and the Institute of Nanotechnology and Advanced Materials, Bar-Ilan University,
Israel, e com Pedro R. Dieguez, pós-doc no International Centre for Theory of
Quantum Technologies (ICTQT), University of Gdánsk, Poland.

A pesquisadora Ana Cristina Sprotte Costa vem desenvolvendo trabalhos em
colaboração com a pesquisadora Sabine Wollmann, que trabalha em grupos
experimentais na Universidade de Bristol na Inglaterra e na Universidade HeriotWatt na Escócia, e com o Dr. Roope Uola, da Universidade de Genebra na Suíça. O
foco dessa colaboração é a verificação experimental de recursos quânticos em
sistemas de dois qubits e, mais recentemente, em sistemas de dimensões mais
altas. 
