Projeto Computação Quântica na Região Sul do Brasil
===================================================

A computação quântica é um campo da ciência que revolucionou a maneira de
processar, armazenar e transmitir informação, podendo impactar todos os setores
produtivos de uma sociedade. Este projeto visa colocar a região sul do Brasil como
protagonista no desenvolvimento dessa tecnologia, não apenas no Brasil e na
América Latina, mas no cenário mundial. Contudo, para que isto ocorra, precisamos
capacitar os nossos estudantes e pesquisadores para atuar no setor, bem como
preparar a nossa indústria para poder tirar proveito imediatamente após os
processadores quânticos estarem funcionando sem erros, ou com algoritmos
robustos de controle de erros, e em larga escala. Temos como proposta a formação
de um forte grupo de pesquisadores envolvendo as universidades UFSC, UFSM,
UDESC, UFPR e o Instituto SENAI de Inovação em Sistemas Embarcados (ISE-SE)
para que de forma colaborativa possa avançar na pesquisa básica, formar mão de
obra qualificada e através do ISI-SE levar essa tecnologia para a indústria
catarinense e para o Brasil. Nosso time conta com vasta experiência na área de
informação quântica, tanto na parte experimental quanto teórica, envolvendo
pesquisadores(as) com formação em áreas como física, matemática, engenharia,
ciência da computação e biologia. A instituição executora (UFSC) e a universidade
parceira UFSM já possuem maturidade no campo específico desta chamada, a
computação quântica, e contribuirão para a consolidação dos grupos de pesquisa
nas demais instituições parceiras. Esse caráter multidisciplinar da equipe permitirá
desenvolver uma ampla gama de projetos em computação quântica, desde testes
dos fundamentos da teoria quântica através dos simuladores quânticos, podendo
ser experimentos em óptica ou computadores quânticos acessíveis online,
desenvolvimento de softwares para linguagens quânticas de programação e
algoritmos quânticos para resolver problemas comerciais de interesse da indústria,
para quebra de sistemas criptográficos e aprendizado de máquina.


Financiamento
-------------

Esse é um projeto financiado pelo CNPq, processo 409673/2022-6.

:**AUXÍLIO FINANCEIRO**:
* **Custeio**: R$ 220.400,00
* **Capital**: R$ 100.000,00

:**BOLSA DE LONGA DURAÇÃO** (R$ 99.600,00): 
* Desenvolvimento Tecnológico em TICs - DTC
   * Duração: 12 Meses
   * Quantidade: 3
* Iniciação Científica - IC
   * Duração: 12 Meses
   * Quantidade: 2

.. toctree::
   :maxdepth: 2
   :hidden:

   sobre
   livro/index

